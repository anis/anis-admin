import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { AppComponent } from './containers/app.component';
import { LoginComponent } from './containers/login.component';
import { NotFoundPageComponent } from './containers/not-found-page.component';
import { NavComponent } from './components/nav.component';
import { throwIfAlreadyLoaded } from './module-import-guard';

export const COMPONENTS = [
    AppComponent,
    LoginComponent,
    NotFoundPageComponent,
    NavComponent
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        CollapseModule.forRoot(),
        BsDropdownModule.forRoot()
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
}
