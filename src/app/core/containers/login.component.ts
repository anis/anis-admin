import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromAuth from '../../auth/auth.reducer';
import * as authActions from '../../auth/auth.action';
import * as authSelector from '../../auth/auth.selector';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
    year = (new Date()).getFullYear();
    public isAuthenticated: Observable<boolean>;

    constructor(private store: Store<{ auth: fromAuth.State }>, private router: Router) {
        this.isAuthenticated = store.select(authSelector.isAuthenticated);
    }

    ngOnInit() {
        this.isAuthenticated.subscribe(isAuthenticated => (isAuthenticated) ? this.router.navigateByUrl('/instance-list') : null );
    }

    login(): void {
        this.store.dispatch(new authActions.LoginAction());
    }
}
