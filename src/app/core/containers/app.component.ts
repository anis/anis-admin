import { Component, ViewEncapsulation } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import * as fromAuth from '../../auth/auth.reducer';
import * as authActions from '../../auth/auth.action';
import * as authSelector from '../../auth/auth.selector';
import { UserProfile } from '../../auth/user-profile.model';
import { VERSIONS } from '../../../settings/settings';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [ './app.component.css' ],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    anisAdminVersion: string = VERSIONS.anisAdmin;
    anisServerVersion: string = VERSIONS.anisServer;
    anisClientVersion: string = VERSIONS.anisClient;
    year = (new Date()).getFullYear();
    isAuthenticated: Observable<boolean>;
    userProfile: Observable<UserProfile>;
    userRoles: Observable<string[]>;

    constructor(private store: Store<{ auth: fromAuth.State }>) {
        this.isAuthenticated = store.select(authSelector.isAuthenticated);
        this.userProfile = store.select(authSelector.getUserProfile);
        this.userRoles = store.select(authSelector.getUserRoles);
    }

    authenticationEnabled(): boolean {
        return environment.authenticationEnabled;
    }

    logout(): void {
        this.store.dispatch(new authActions.LogoutAction());
    }

    openEditProfile(): void {
        this.store.dispatch(new authActions.OpenEditProfileAction());
    }

    isAnisAdmin() {
        return this.userRoles.pipe(
            map(roles => roles.includes('anis_admin'))
        );
    }
}
