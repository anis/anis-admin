import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UserProfile } from '../../auth/user-profile.model';
import { environment } from '../../../environments/environment'

@Component({
    selector: 'app-nav',
    templateUrl: 'nav.component.html',
    styleUrls: [ 'nav.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavComponent {
    @Input() userProfile: UserProfile;
    @Output() logout: EventEmitter<any> = new EventEmitter();
    @Output() openEditProfile: EventEmitter<any> = new EventEmitter();
    isCollapsed = true;
    baseHref: string = environment.baseHref;

    authenticationEnabled(): boolean {
        return environment.authenticationEnabled;
    }

    emitLogout() {
        this.logout.emit();
    }

    emitOpenEditProfile() {
        this.openEditProfile.emit();
    }
}
