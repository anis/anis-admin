import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NavComponent } from './nav.component';

describe('[Core] Component: NavComponent', () => {
    let component: NavComponent;
    let fixture: ComponentFixture<NavComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [NavComponent]
        });
        fixture = TestBed.createComponent(NavComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    // it('should display the Sign In button if no user logged in', () => {
    //     component.isAuthenticated = false;
    //     fixture.detectChanges();
    //     const template = fixture.nativeElement;
    //     expect(template.querySelector('#button-sign-in')).toBeTruthy();
    // });

    // it('should not display the dropdown menu if no user logged in', () => {
    //     component.isAuthenticated = false;
    //     fixture.detectChanges();
    //     const template = fixture.nativeElement;
    //     expect(template.querySelector('#dropdown-menu')).toBeFalsy();
    // });

    // it('should display the dropdown menu if user logged in', () => {
    //     component.isAuthenticated = true;
    //     fixture.detectChanges();
    //     const template = fixture.nativeElement;
    //     expect(template.querySelector('#dropdown-menu')).toBeTruthy();
    // });

    // it('should not display the Sign In button if user logged in', () => {
    //     component.isAuthenticated = true;
    //     fixture.detectChanges();
    //     const template = fixture.nativeElement;
    //     expect(template.querySelector('#button-sign-in')).toBeFalsy();
    // });

    // it('raises the logout event when clicked', () => {
    //     component.logout.subscribe((event) => expect(event).toBe(undefined));
    //     component.emitLogout();
    //   });
});
