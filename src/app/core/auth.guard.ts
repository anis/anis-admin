import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    Router,
    RouterStateSnapshot,
} from '@angular/router';

import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';

import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AuthGuard extends KeycloakAuthGuard {
    constructor(
        protected readonly router: Router,
        protected readonly keycloak: KeycloakService
    ) {
        super(router, keycloak);
    }

    public async isAccessAllowed(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ) {
        // If authorization is not enabled return true
        if (!environment.authenticationEnabled) {
            return true;
        }

        // Force the user to log in if currently unauthenticated.
        if (!this.authenticated) {
            this.router.navigateByUrl('/login');
            return false;
        }

        // If authenticated but not admin go to unauthorized page.
        if(!this.roles.includes('anis_admin')) {
            this.router.navigateByUrl('/unauthorized');
            return false;
        }

        // Else return true;
        return true;
    }
}
