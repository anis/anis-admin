import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './core/containers/login.component';
import { NotFoundPageComponent } from './core/containers/not-found-page.component';
import { UnauthorizedComponent } from './core/containers/unauthorized.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'unauthorized', component: UnauthorizedComponent },
    { path: '', redirectTo: 'instance-list', pathMatch: 'full' },
    { path: '**', component: NotFoundPageComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }

export const routedComponents = [
    NotFoundPageComponent
];
