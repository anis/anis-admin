import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule, RouterState } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { CustomRouterStateSerializer } from './shared/utils';
import { reducers, metaReducers } from './app.reducer';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { MetamodelModule} from './metamodel/metamodel.module';
import { SettingsModule } from './settings/settings.module';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './core/containers/app.component';
import { environment } from '../environments/environment';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        CoreModule,
        AuthModule,
        MetamodelModule,
        SettingsModule,
        StoreModule.forRoot(reducers, { 
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
                strictStateSerializability: true,
                strictActionSerializability: true,
                strictActionWithinNgZone: true
            }
        }),
        AppRoutingModule,
        StoreRouterConnectingModule.forRoot({
            serializer: CustomRouterStateSerializer,
            routerState: RouterState.Minimal,
        }),
        !environment.production ? StoreDevtoolsModule.instrument() : [],
        EffectsModule.forRoot([])
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
