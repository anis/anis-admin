import * as actions from './auth.action';

import { UserProfile } from './user-profile.model';

export interface State {
    isAuthenticated: boolean;
    userProfile: UserProfile;
    userRoles: string[];
}

export const initialState: State = {
    isAuthenticated: false,
    userProfile: null,
    userRoles: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.AUTH_SUCCESS:
            return {
                ...state,
                isAuthenticated: true
            };

        case actions.LOAD_USER_PROFILE_SUCCESS:
            const userProfile = action.payload;

            return {
                ...state,
                userProfile
            };

        case actions.LOAD_USER_ROLES_SUCCESS:
            const userRoles = action.payload;

            return {
                ...state,
                userRoles
            };

        default:
            return state;
    }
}

export const isAuthenticated = (state: State) => state.isAuthenticated;
export const getUserProfile = (state: State) => state.userProfile;
export const getUserRoles = (state: State) => state.userRoles;
