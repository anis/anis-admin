import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { KeycloakService } from 'keycloak-angular';

import * as authActions from './auth.action';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthEffects {
    constructor(
        private actions$: Actions,
        private keycloak: KeycloakService
    ) { }

    @Effect({ dispatch: false })
    loginAction$ = this.actions$.pipe(
        ofType(authActions.LOGIN),
        tap(_ => {
            let redirectUri = window.location.origin;
            if (environment.baseHref !== '/') {
                redirectUri += environment.baseHref;
            }
            redirectUri += environment.ssoLoginRedirectUri;
            this.keycloak.login({ redirectUri });
        })
    );

    @Effect({ dispatch: false })
    logoutAction$ = this.actions$.pipe(
        ofType(authActions.LOGOUT),
        tap(_ => {
            let redirectUri = window.location.origin;
            if (environment.baseHref !== '/') {
                redirectUri += environment.baseHref;
            }
            redirectUri += environment.ssoLogoutRedirectUri;
            this.keycloak.logout(redirectUri);
        })
    );

    @Effect()
    authSuccessAction$ = this.actions$.pipe(
        ofType(authActions.AUTH_SUCCESS),
        switchMap(_ =>
            from(this.keycloak.loadUserProfile()).pipe(
                switchMap(userProfile => [
                    new authActions.LoadUserProfileSuccessAction(userProfile),
                    new authActions.LoadUserRolesSuccessAction(this.keycloak.getUserRoles())
                ])
            )
        )
    );

    @Effect({ dispatch: false })
    OpenEditProfileAction$ = this.actions$.pipe(
        ofType(authActions.OPEN_EDIT_PROFILE),
        tap(_ => window.open(environment.ssoAuthUrl + '/realms/' + environment.ssoRealm + '/account', '_blank'))
    );
}
