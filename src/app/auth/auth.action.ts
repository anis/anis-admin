import { Action } from '@ngrx/store';

import { UserProfile } from './user-profile.model';

export const LOGIN = '[Auth] Login';
export const LOGOUT = '[Auth] Logout';
export const AUTH_SUCCESS = '[Auth] Auth Success';
export const LOAD_USER_PROFILE_SUCCESS = '[Auth] Load User Profile Success';
export const LOAD_USER_ROLES_SUCCESS = '[Auth] Load User Roles Success';
export const OPEN_EDIT_PROFILE = '[Auth] Edit Profile';

export class LoginAction implements Action {
    readonly type = LOGIN;

    constructor(public payload: {} = null) { }
}

export class LogoutAction implements Action {
    readonly type = LOGOUT;

    constructor(public payload: {} = null) { }
}

export class AuthSuccessAction implements Action {
    readonly type = AUTH_SUCCESS;

    constructor(public payload: {} = null) { }
}

export class LoadUserProfileSuccessAction implements Action {
    readonly type = LOAD_USER_PROFILE_SUCCESS;

    constructor(public payload: UserProfile) { }
}

export class LoadUserRolesSuccessAction implements Action {
    readonly type = LOAD_USER_ROLES_SUCCESS;

    constructor(public payload: string[]) { }
}

export class OpenEditProfileAction implements Action {
    readonly type = OPEN_EDIT_PROFILE;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoginAction
    | LogoutAction
    | AuthSuccessAction
    | LoadUserProfileSuccessAction
    | LoadUserRolesSuccessAction
    | OpenEditProfileAction;
