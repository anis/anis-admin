import { APP_INITIALIZER } from '@angular/core';
import { from } from 'rxjs';

import { KeycloakService, KeycloakEventType } from 'keycloak-angular';
import { Store } from '@ngrx/store';

import * as keycloakActions from './auth.action';
import * as fromKeycloak from './auth.reducer';
import { environment } from '../../environments/environment';

function initializeKeycloak(keycloak: KeycloakService, store: Store<{ keycloak: fromKeycloak.State }>) {
    return async () => {
        from(keycloak.keycloakEvents$).subscribe(event => {
            if (event.type === KeycloakEventType.OnAuthSuccess) {
                store.dispatch(new keycloakActions.AuthSuccessAction());
            }
        })

        let silentCheckSsoRedirectUri = window.location.origin;
        if (environment.baseHref != '/') {
            silentCheckSsoRedirectUri += environment.baseHref;
        }
        silentCheckSsoRedirectUri += '/assets/silent-check-sso.html';

        return keycloak.init({
            config: {
                url: environment.ssoAuthUrl,
                realm: environment.ssoRealm,
                clientId: environment.ssoClientId,
            },
            initOptions: {
                onLoad: 'check-sso',
                silentCheckSsoRedirectUri
            },
            loadUserProfileAtStartUp: true
        });
    }
}

export const initializeKeycloakAnis = {
    provide: APP_INITIALIZER,
    useFactory: initializeKeycloak,
    multi: true,
    deps: [ KeycloakService, Store ],
};
