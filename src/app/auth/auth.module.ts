import { NgModule } from '@angular/core';

import { KeycloakAngularModule } from 'keycloak-angular';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { initializeKeycloakAnis } from './init.keycloak';
import { reducer } from './auth.reducer';
import { AuthEffects } from './auth.effects';
import { environment } from 'src/environments/environment';

@NgModule({
    imports: [
        environment.authenticationEnabled ? KeycloakAngularModule : [],
        StoreModule.forFeature('auth', reducer),
        environment.authenticationEnabled ? EffectsModule.forFeature([ AuthEffects ]): []
    ],
    providers: [
        environment.authenticationEnabled ? initializeKeycloakAnis: []
    ]
})
export class AuthModule { }
