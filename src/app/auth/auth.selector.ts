import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as auth from './auth.reducer';

export const getAuthState = createFeatureSelector<auth.State>('auth');

export const isAuthenticated = createSelector(
    getAuthState,
    auth.isAuthenticated
);

export const getUserProfile = createSelector(
    getAuthState,
    auth.getUserProfile
);

export const getUserRoles = createSelector(
    getAuthState,
    auth.getUserRoles
);
