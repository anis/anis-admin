import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ToastrModule } from 'ngx-toastr';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { FormatFileSizePipe } from './format-file-size.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        AccordionModule.forRoot(),
        RouterModule
    ],
    declarations: [
        FormatFileSizePipe
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule,
        BsDropdownModule,
        AccordionModule,
        ModalModule,
        FormatFileSizePipe
    ]
})
export class SharedModule { }
