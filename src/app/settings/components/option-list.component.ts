import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { SettingsSelectOption } from '../store/model';

@Component({
    selector: 'app-option-list',
    templateUrl: 'option-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionListComponent {
    @Input() optionList: SettingsSelectOption[];
    @Output() editOption: EventEmitter<SettingsSelectOption> = new EventEmitter();
    @Output() deleteOption: EventEmitter<SettingsSelectOption> = new EventEmitter();

    modalRef: BsModalRef;
    optionSelected: SettingsSelectOption;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>, option: SettingsSelectOption) {
        this.optionSelected = option;
        this.modalRef = this.modalService.show(template);
    }

    confirmDel() {
        this.deleteOption.emit(this.optionSelected);
        this.modalRef.hide();
    }

    confirmEdit(option: SettingsSelectOption) {
        this.editOption.emit(option);
        this.modalRef.hide();
    }
}
