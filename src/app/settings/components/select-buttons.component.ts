import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { SettingsSelect, SettingsSelectOption } from '../store/model';

@Component({
    selector: 'app-select-buttons',
    templateUrl: 'select-buttons.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectButtonsComponent {
    @Input() select: SettingsSelect;
    @Output() deleteSelect: EventEmitter<SettingsSelect> = new EventEmitter();
    @Output() editSelect: EventEmitter<SettingsSelect> = new EventEmitter();
    @Output() addOption: EventEmitter<SettingsSelectOption> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    confirmDel() {
        this.deleteSelect.emit(this.select);
        this.modalRef.hide();
    }

    confirmEdit(select: SettingsSelect) {
        this.editSelect.emit(select);
        this.modalRef.hide();
    }

    addNewOption(option: SettingsSelectOption) {
        this.addOption.emit({...option, select_name: this.select.name});
        this.modalRef.hide();
    }
}
