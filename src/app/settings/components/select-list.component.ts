import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { SettingsSelect } from '../store/model';

@Component({
    selector: 'app-select-list',
    templateUrl: 'select-list.component.html',
    styleUrls: ['select-list.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectListComponent {
    @Input() selectList: SettingsSelect[];
    @Output() addSelect: EventEmitter<SettingsSelect> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    confirmAdd(select: SettingsSelect) {
        this.addSelect.emit(select);
        this.modalRef.hide();
    }
}
