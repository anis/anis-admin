import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { SettingsSelectOption } from '../store/model';

@Component({
    selector: 'app-form-option',
    templateUrl: 'form-option.component.html'
})
export class FormOptionComponent {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: SettingsSelectOption = new SettingsSelectOption();
    @Output() submitted: EventEmitter<SettingsSelectOption> = new EventEmitter();

    emit(option: SettingsSelectOption) {
        this.submitted.emit({id: this.model.id, ...option});
    }
}
