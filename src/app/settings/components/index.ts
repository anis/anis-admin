import { SelectListComponent } from './select-list.component';
import { FormSelectComponent } from './form-select.component';
import { SelectButtonsComponent } from './select-buttons.component';
import { OptionListComponent } from './option-list.component';
import { FormOptionComponent } from './form-option.component';

export const dummiesComponents = [
    SelectListComponent,
    FormSelectComponent,
    SelectButtonsComponent,
    OptionListComponent,
    FormOptionComponent
];
