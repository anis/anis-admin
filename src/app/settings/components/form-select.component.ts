import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { SettingsSelect } from '../store/model';

@Component({
    selector: 'app-form-select',
    templateUrl: 'form-select.component.html'
})
export class FormSelectComponent {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: SettingsSelect = new SettingsSelect();
    @Output() submitted: EventEmitter<SettingsSelect> = new EventEmitter();

    emit(select: SettingsSelect) {
        let selectEmitted: SettingsSelect;
        (this.model.name) ? selectEmitted = {name: this.model.name, ...select} : selectEmitted = select;
        this.submitted.emit(selectEmitted);
    }
}
