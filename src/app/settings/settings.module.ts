import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '../shared/shared.module';
import { SettingsRoutingModule, routedComponents } from './settings.routing';
import { dummiesComponents } from './components';
import { reducer } from './store/reducer';
import { settingsEffects } from './store/effects';
import { settingsServices } from './store/service';

@NgModule({
    imports: [
        SharedModule,
        SettingsRoutingModule,
        StoreModule.forFeature('settings', reducer),
        EffectsModule.forFeature(settingsEffects)
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ],
    providers: settingsServices
})
export class SettingsModule { }
