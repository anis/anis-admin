import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from './containers/settings.component';
import { AuthGuard } from '../core/auth.guard';
import { environment } from 'src/environments/environment';

const routes: Routes = [
    { path: 'settings', component: SettingsComponent },
    { path: 'settings/:select', component: SettingsComponent }
];

const routesGuarded = routes.map(r=> {
    if (environment.authenticationEnabled) {
        return {
            ...r,
            canActivate: [AuthGuard]
        };
    } else {
        return r;
    }
});

@NgModule({
    imports: [ RouterModule.forChild(routesGuarded) ],
    exports: [ RouterModule ]
})
export class SettingsRoutingModule { }

export const routedComponents = [
    SettingsComponent
];
