import * as actions from '../action/select.action';

import { SettingsSelect } from '../model';

export interface State {
    selectListIsLoading: boolean;
    selectListIsLoaded: boolean;
    selectList: SettingsSelect[];
}

const initialState: State = {
    selectListIsLoading: false,
    selectListIsLoaded: false,
    selectList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_SETTINGS_SELECT_LIST_WIP:
            return {
                ...state,
                selectListIsLoading: true
            };

        case actions.LOAD_SETTINGS_SELECT_LIST_SUCCESS:
            const selectList = action.payload as SettingsSelect[];

            return {
                ...state,
                selectList,
                selectListIsLoading: false,
                selectListIsLoaded: true
            };

        case actions.LOAD_SETTINGS_SELECT_LIST_FAIL:
            return {
                ...state,
                selectListIsLoading: false
            };

        case actions.ADD_NEW_SETTINGS_SELECT_SUCCESS:
            const newSelect = action.payload as SettingsSelect;

            return {
                ...state,
                selectList: [...state.selectList, newSelect]
            };

        case actions.EDIT_SETTINGS_SELECT_SUCCESS:
            const editedSelect = action.payload as SettingsSelect;

            return {
                ...state,
                selectList: [...state.selectList.filter(select => select.name !== editedSelect.name), editedSelect]
            };    

        case actions.DELETE_SETTINGS_SELECT_SUCCESS:
            const deletedSelect = action.payload as SettingsSelect;

            return {
                ...state,
                selectList: state.selectList.filter(select => select.name !== deletedSelect.name)
            }

        default:
            return state;
    }
}

export const getSelectListIsLoading = (state: State) => state.selectListIsLoading;
export const getSelectListIsLoaded = (state: State) => state.selectListIsLoaded;
export const getSelectList = (state: State) => state.selectList;
