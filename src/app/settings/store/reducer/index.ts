import { combineReducers, createFeatureSelector } from '@ngrx/store';

import * as select from './select.reducer';
import * as option from './option.reducer';

export interface State {
    select: select.State;
    option: option.State;
}

const reducers = {
    select: select.reducer,
    option: option.reducer
}

const productionReducer = combineReducers(reducers);

export function reducer(state: any, action: any) {
    return productionReducer(state, action);
}

export const getSettingsState = createFeatureSelector<State>('settings');
