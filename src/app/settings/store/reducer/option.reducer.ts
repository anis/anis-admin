import * as actions from '../action/option.action';

import { sortDisplay } from '../../../shared/utils';
import { SettingsSelectOption } from '../model';

export interface State {
    optionListIsLoading: boolean;
    optionListIsLoaded: boolean;
    optionList: SettingsSelectOption[];
}

const initialState: State = {
    optionListIsLoading: false,
    optionListIsLoaded: false,
    optionList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_SETTINGS_SELECT_OPTION_LIST_WIP:
            return {
                ...state,
                optionListIsLoading: true
            };

        case actions.LOAD_SETTINGS_SELECT_OPTION_LIST_SUCCESS:
            const optionList = action.payload as SettingsSelectOption[];

            return {
                ...state,
                optionList,
                optionListIsLoading: false,
                optionListIsLoaded: true
            };

        case actions.LOAD_SETTINGS_SELECT_OPTION_LIST_FAIL:
            return {
                ...state,
                optionListIsLoading: false
            };

        case actions.ADD_NEW_SETTINGS_SELECT_OPTION_SUCCESS:
            const newSelect = action.payload as SettingsSelectOption;

            return {
                ...state,
                optionList: [...state.optionList, newSelect].sort(sortDisplay)
            };

        case actions.EDIT_SETTINGS_SELECT_OPTION_SUCCESS:
            const editedSelect = action.payload as SettingsSelectOption;

            return {
                ...state,
                optionList: [
                    ...state.optionList.filter(option => option.id !== editedSelect.id),
                    editedSelect
                ].sort(sortDisplay)
            };    

        case actions.DELETE_SETTINGS_SELECT_OPTION_SUCCESS:
            const deletedSelect = action.payload as SettingsSelectOption;

            return {
                ...state,
                optionList: state.optionList.filter(option => option.id !== deletedSelect.id)
            }

        default:
            return state;
    }
}

export const getOptionListIsLoading = (state: State) => state.optionListIsLoading;
export const getOptionListIsLoaded = (state: State) => state.optionListIsLoaded;
export const getOptionList = (state: State) => state.optionList;
