export class SettingsSelectOption {
    id: number;
    label: string;
    value: string;
    display: number;
    select_name: string;
}
