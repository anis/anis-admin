import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SettingsSelect } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class SettingsSelectService {
    private SETTINGS_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveSettingsSelectList() {
        return this.http.get<SettingsSelect[]>(this.SETTINGS_PATH + 'select');
    }

    addSettingsSelect(settingsSelect: SettingsSelect) {
        return this.http.post<SettingsSelect>(this.SETTINGS_PATH + 'select', settingsSelect);
    }

    editSettingsSelect(settingsSelect: SettingsSelect) {
        return this.http.put<SettingsSelect>(this.SETTINGS_PATH + 'select/' + settingsSelect.name, settingsSelect);
    }

    deleteSettingsSelect(name: string) {
        return this.http.delete(this.SETTINGS_PATH + 'select/' + name);
    }
}
