import { SettingsSelectService } from './select.service';
import { SettingsSelectOptionService } from './option.service';

export const settingsServices = [
    SettingsSelectService,
    SettingsSelectOptionService
];
