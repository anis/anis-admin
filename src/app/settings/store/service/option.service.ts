import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { sortDisplay } from '../../../shared/utils';
import { SettingsSelectOption } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class SettingsSelectOptionService {
    private SETTINGS_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveSettingsSelectOptionList(): Observable<SettingsSelectOption[]> {
        return this.http.get<SettingsSelectOption[]>(this.SETTINGS_PATH + 'option').pipe(
            map(optionList => optionList.sort(sortDisplay))
        );;
    }

    addSettingsSelectOption(settingsSelectOption: SettingsSelectOption): Observable<SettingsSelectOption> {
        return this.http.post<SettingsSelectOption>(
            this.SETTINGS_PATH + 'option',
            settingsSelectOption
        );
    }

    editSettingsSelectOption(settingsSelectOption: SettingsSelectOption): Observable<SettingsSelectOption> {
        return this.http.put<SettingsSelectOption>(this.SETTINGS_PATH + 'option/' + settingsSelectOption.id, settingsSelectOption);
    }

    deleteSettingsSelectOption(id: number) {
        return this.http.delete(this.SETTINGS_PATH + 'option/' + id);
    }
}
