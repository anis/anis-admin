import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromRouter from '../../../shared/utils';
import * as fromSettings from '../reducer';
import { SettingsSelect } from '../model';
import * as selectActions from '../action/select.action';
import { SettingsSelectService } from '../service/select.service';

@Injectable()
export class SettingsSelectEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState, settings: fromSettings.State}>,
        private selectService: SettingsSelectService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadSettingsSelectListAction$ = this.actions$.pipe(
        ofType(selectActions.LOAD_SETTINGS_SELECT_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.settings.select.selectListIsLoaded) {
                return of({ type: '[No Action] [Settings] Settings Select List is already loaded' });
            } else {
                return of(new selectActions.LoadSettingsSelectListWipAction());
            }
        })
    );

    @Effect()
    loadSettingsSelectListWipAction$ = this.actions$.pipe(
        ofType(selectActions.LOAD_SETTINGS_SELECT_LIST_WIP),
        switchMap(_ =>
            this.selectService.retrieveSettingsSelectList().pipe(
                map((selectList: SettingsSelect[]) =>
                    new selectActions.LoadSettingsSelectListSuccessAction(selectList)),
                catchError(() => of(new selectActions.LoadSettingsSelectListFailAction()))
            )
        )
    );

    @Effect({ dispatch: false })
    loadSettingsSelectListSuccessAction$ = this.actions$.pipe(
        ofType(selectActions.LOAD_SETTINGS_SELECT_LIST_SUCCESS),
        withLatestFrom(this.store$),
        map(([action, state]) => {
            if (!state.router.state.params.select) {
                const loadSettingsSelectListSuccessAction = action as selectActions.LoadSettingsSelectListSuccessAction;
            }
        })
    );

    @Effect({ dispatch: false })
    loadSettingsSelectListFailAction$ = this.actions$.pipe(
        ofType(selectActions.LOAD_SETTINGS_SELECT_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Settings Select List loading failed'))
    );

    @Effect()
    addNewSettingsSelectAction$ = this.actions$.pipe(
        ofType(selectActions.ADD_NEW_SETTINGS_SELECT),
        switchMap((action: selectActions.AddNewSettingsSelectAction) => {
            return this.selectService.addSettingsSelect(action.payload).pipe(
                map((newSelect: SettingsSelect) => new selectActions.AddNewSettingsSelectSuccessAction(newSelect)),
                catchError(() => of(new selectActions.AddNewSettingsSelectFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewSettingsSelectSuccessAction$ = this.actions$.pipe(
        ofType(selectActions.ADD_NEW_SETTINGS_SELECT_SUCCESS),
        map((action: selectActions.AddNewSettingsSelectSuccessAction) => {
            this.router.navigate(['/settings/' + action.payload.name]);
            this.toastr.success('Add select success!', 'The new select has been created!');
        })
    );

    @Effect({dispatch: false})
    addNewSettingsSelectFailAction$ = this.actions$.pipe(
        ofType(selectActions.ADD_NEW_SETTINGS_SELECT_FAIL),
        map(_ => this.toastr.error('Add select failed!', 'The new select could not be created into the database'))
    );

    @Effect()
    editSettingsSelectAction$ = this.actions$.pipe(
        ofType(selectActions.EDIT_SETTINGS_SELECT),
        switchMap((action: selectActions.EditSettingsSelectAction) => {
            return this.selectService.editSettingsSelect(action.payload).pipe(
                map((select: SettingsSelect) => new selectActions.EditSettingsSelectSuccessAction(select)),
                catchError(() => of(new selectActions.EditSettingsSelectFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editSettingsSelectSuccessAction$ = this.actions$.pipe(
        ofType(selectActions.EDIT_SETTINGS_SELECT_SUCCESS),
        map(_ => {
            this.toastr.success('Edit select success!', 'The existing entity has been edited into the database')
        })
    );

    @Effect({dispatch: false})
    editSettingsSelectFailAction$ = this.actions$.pipe(
        ofType(selectActions.EDIT_SETTINGS_SELECT_FAIL),
        map(_ => this.toastr.error('Edit select failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteSettingsAction$ = this.actions$.pipe(
        ofType(selectActions.DELETE_SETTINGS_SELECT),
        switchMap((action: selectActions.DeleteSettingsSelectAction) => {
            return this.selectService.deleteSettingsSelect(action.payload.name).pipe(
                map(_ => new selectActions.DeleteSettingsSelectSuccessAction(action.payload)),
                catchError(() => of(new selectActions.DeleteSettingsSelectFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteSettingsSelectSuccessAction$ = this.actions$.pipe(
        ofType(selectActions.DELETE_SETTINGS_SELECT_SUCCESS),
        withLatestFrom(this.store$),
        map(([action, state]) => {
            this.router.navigate(['/settings/' + state.settings.select.selectList[0].name]);
            this.toastr.success('Delete select success!', 'The select has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteSettingsSelectFailAction$ = this.actions$.pipe(
        ofType(selectActions.DELETE_SETTINGS_SELECT_FAIL),
        map(_ => this.toastr.error('Delete select failed!', 'The select could not be deleted into the database'))
    );
}
