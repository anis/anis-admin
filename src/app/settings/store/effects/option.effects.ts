import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromSettings from '../reducer';
import { SettingsSelectOption } from '../model';
import * as optionActions from '../action/option.action';
import { SettingsSelectOptionService } from '../service/option.service';

@Injectable()
export class SettingsSelectOptionEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{settings: fromSettings.State}>,
        private optionService: SettingsSelectOptionService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadSettingsSelectOptionListAction$ = this.actions$.pipe(
        ofType(optionActions.LOAD_SETTINGS_SELECT_OPTION_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.settings.select.selectListIsLoaded) {
                return of({ type: '[No Action] [Settings] Settings Select Option List is already loaded' });
            } else {
                return of(new optionActions.LoadSettingsSelectOptionListWipAction());
            }
        })
    );

    @Effect()
    loadSettingsSelectOptionListWipAction$ = this.actions$.pipe(
        ofType(optionActions.LOAD_SETTINGS_SELECT_OPTION_LIST_WIP),
        switchMap(_ =>
            this.optionService.retrieveSettingsSelectOptionList().pipe(
                map((optionList: SettingsSelectOption[]) =>
                    new optionActions.LoadSettingsSelectOptionListSuccessAction(optionList)),
                catchError(() => of(new optionActions.LoadSettingsSelectOptionListFailAction()))
            )
        )
    );

    @Effect({ dispatch: false })
    loadSettingsSelectOptionListFailAction$ = this.actions$.pipe(
        ofType(optionActions.LOAD_SETTINGS_SELECT_OPTION_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Settings Select Option List loading failed'))
    );

    @Effect()
    addNewSettingsSelectOptionAction$ = this.actions$.pipe(
        ofType(optionActions.ADD_NEW_SETTINGS_SELECT_OPTION),
        switchMap((action: optionActions.AddNewSettingsSelectOptionAction) => {
            return this.optionService.addSettingsSelectOption(action.payload).pipe(
                map((newOption: SettingsSelectOption) => new optionActions.AddNewSettingsSelectOptionSuccessAction(newOption)),
                catchError(() => of(new optionActions.AddNewSettingsSelectOptionFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewSettingsSelectOptionSuccessAction$ = this.actions$.pipe(
        ofType(optionActions.ADD_NEW_SETTINGS_SELECT_OPTION_SUCCESS),
        map(_ => {
            this.toastr.success('Add select option success!', 'The new select option has been created!');
        })
    );

    @Effect({dispatch: false})
    addNewSettingsSelectOptionFailAction$ = this.actions$.pipe(
        ofType(optionActions.ADD_NEW_SETTINGS_SELECT_OPTION_FAIL),
        map(_ => this.toastr.error('Add select option failed!', 'The new select option could not be created into the database'))
    );

    @Effect()
    editSettingsSelectOptionAction$ = this.actions$.pipe(
        ofType(optionActions.EDIT_SETTINGS_SELECT_OPTION),
        switchMap((action: optionActions.EditSettingsSelectOptionAction) => {
            return this.optionService.editSettingsSelectOption(action.payload).pipe(
                map((option: SettingsSelectOption) => new optionActions.EditSettingsSelectOptionSuccessAction(option)),
                catchError(() => of(new optionActions.EditSettingsSelectOptionFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editSettingsSelectOptionSuccessAction$ = this.actions$.pipe(
        ofType(optionActions.EDIT_SETTINGS_SELECT_OPTION_SUCCESS),
        map(_ => {
            this.toastr.success('Edit select option success!', 'The existing entity has been edited into the database')
        })
    );

    @Effect({dispatch: false})
    editSettingsSelectOptionFailAction$ = this.actions$.pipe(
        ofType(optionActions.EDIT_SETTINGS_SELECT_OPTION_FAIL),
        map(_ => this.toastr.error('Edit select option failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteSettingsSelectOptionAction$ = this.actions$.pipe(
        ofType(optionActions.DELETE_SETTINGS_SELECT_OPTION),
        switchMap((action: optionActions.DeleteSettingsSelectOptionAction) => {
            return this.optionService.deleteSettingsSelectOption(action.payload.id).pipe(
                map(_ => new optionActions.DeleteSettingsSelectOptionSuccessAction(action.payload)),
                catchError(() => of(new optionActions.DeleteSettingsSelectOptionFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteSettingsSelectOptionSuccessAction$ = this.actions$.pipe(
        ofType(optionActions.DELETE_SETTINGS_SELECT_OPTION_SUCCESS),
        map(_ => {
            this.toastr.success('Delete select option success!', 'The select option has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteSettingsSelectOptionFailAction$ = this.actions$.pipe(
        ofType(optionActions.DELETE_SETTINGS_SELECT_OPTION_FAIL),
        map(_ => this.toastr.error('Delete select option failed!', 'The select option could not be deleted into the database'))
    );
}
