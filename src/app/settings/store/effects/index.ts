import { SettingsSelectEffects } from './select.effects';
import { SettingsSelectOptionEffects } from './option.effects';

export const settingsEffects = [
    SettingsSelectEffects,
    SettingsSelectOptionEffects
];
