import { Action } from '@ngrx/store';

import { SettingsSelectOption } from '../model';

export const LOAD_SETTINGS_SELECT_OPTION_LIST = '[Settings] Load Settings Select Option List';
export const LOAD_SETTINGS_SELECT_OPTION_LIST_WIP = '[Settings] Load Settings Select Option List WIP';
export const LOAD_SETTINGS_SELECT_OPTION_LIST_SUCCESS = '[Settings] Load Settings Select Option Sucess';
export const LOAD_SETTINGS_SELECT_OPTION_LIST_FAIL = '[Settings] Load Settings Select Option Fail';
export const ADD_NEW_SETTINGS_SELECT_OPTION = '[Settings] Add New Settings Select Option';
export const ADD_NEW_SETTINGS_SELECT_OPTION_SUCCESS = '[Settings] Add New Settings Select Option Sucess';
export const ADD_NEW_SETTINGS_SELECT_OPTION_FAIL = '[Settings] Add New Settings Select Option Fail';
export const EDIT_SETTINGS_SELECT_OPTION = '[Settings] Edit Settings Select Option';
export const EDIT_SETTINGS_SELECT_OPTION_SUCCESS = '[Settings] Edit Settings Select Option Success';
export const EDIT_SETTINGS_SELECT_OPTION_FAIL = '[Settings] Edit Settings Select Option Fail';
export const DELETE_SETTINGS_SELECT_OPTION = '[Settings] Delete Settings Select Option';
export const DELETE_SETTINGS_SELECT_OPTION_SUCCESS = '[Settings] Delete Settings Select Option Success';
export const DELETE_SETTINGS_SELECT_OPTION_FAIL = '[Settings] Delete Settings Select Option Fail';

export class LoadSettingsSelectOptionListAction implements Action {
    type = LOAD_SETTINGS_SELECT_OPTION_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadSettingsSelectOptionListWipAction implements Action {
    type = LOAD_SETTINGS_SELECT_OPTION_LIST_WIP;

    constructor(public payload: {} = null) { }
}

export class LoadSettingsSelectOptionListSuccessAction implements Action {
    type = LOAD_SETTINGS_SELECT_OPTION_LIST_SUCCESS;

    constructor(public payload: SettingsSelectOption[]) { }
}

export class LoadSettingsSelectOptionListFailAction implements Action {
    type = LOAD_SETTINGS_SELECT_OPTION_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewSettingsSelectOptionAction implements Action {
    type = ADD_NEW_SETTINGS_SELECT_OPTION;

    constructor(public payload: SettingsSelectOption) { }
}

export class AddNewSettingsSelectOptionSuccessAction implements Action {
    type = ADD_NEW_SETTINGS_SELECT_OPTION_SUCCESS;

    constructor(public payload: SettingsSelectOption) { }
}

export class AddNewSettingsSelectOptionFailAction implements Action {
    type = ADD_NEW_SETTINGS_SELECT_OPTION_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditSettingsSelectOptionAction implements Action {
    type = EDIT_SETTINGS_SELECT_OPTION;

    constructor(public payload: SettingsSelectOption) { }
}

export class EditSettingsSelectOptionSuccessAction implements Action {
    type = EDIT_SETTINGS_SELECT_OPTION_SUCCESS;

    constructor(public payload: SettingsSelectOption) { }
}

export class EditSettingsSelectOptionFailAction implements Action {
    type = EDIT_SETTINGS_SELECT_OPTION_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteSettingsSelectOptionAction implements Action {
    type = DELETE_SETTINGS_SELECT_OPTION;

    constructor(public payload: SettingsSelectOption) { }
}

export class DeleteSettingsSelectOptionSuccessAction implements Action {
    type = DELETE_SETTINGS_SELECT_OPTION_SUCCESS;

    constructor(public payload: SettingsSelectOption) { }
}

export class DeleteSettingsSelectOptionFailAction implements Action {
    type = DELETE_SETTINGS_SELECT_OPTION_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions 
    = LoadSettingsSelectOptionListAction
    | LoadSettingsSelectOptionListWipAction
    | LoadSettingsSelectOptionListSuccessAction
    | LoadSettingsSelectOptionListFailAction
    | AddNewSettingsSelectOptionAction
    | AddNewSettingsSelectOptionSuccessAction
    | AddNewSettingsSelectOptionFailAction
    | EditSettingsSelectOptionAction
    | EditSettingsSelectOptionSuccessAction
    | EditSettingsSelectOptionFailAction
    | DeleteSettingsSelectOptionAction
    | DeleteSettingsSelectOptionSuccessAction
    | DeleteSettingsSelectOptionFailAction;
