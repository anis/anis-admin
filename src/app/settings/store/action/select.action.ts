import { Action } from '@ngrx/store';

import { SettingsSelect } from '../model';

export const LOAD_SETTINGS_SELECT_LIST = '[Settings] Load Settings Select List';
export const LOAD_SETTINGS_SELECT_LIST_WIP = '[Settings] Load Settings Select List WIP';
export const LOAD_SETTINGS_SELECT_LIST_SUCCESS = '[Settings] Load Settings Select Sucess';
export const LOAD_SETTINGS_SELECT_LIST_FAIL = '[Settings] Load Settings Select Fail';
export const ADD_NEW_SETTINGS_SELECT = '[Settings] Add New Settings Select';
export const ADD_NEW_SETTINGS_SELECT_SUCCESS = '[Settings] Add New Settings Select Success';
export const ADD_NEW_SETTINGS_SELECT_FAIL = '[Settings] Add New Settings Select Fail';
export const EDIT_SETTINGS_SELECT = '[Settings] Edit Settings Select';
export const EDIT_SETTINGS_SELECT_SUCCESS = '[Settings] Edit Settings Select Success';
export const EDIT_SETTINGS_SELECT_FAIL = '[Settings] Edit Settings Select Fail';
export const DELETE_SETTINGS_SELECT = '[Settings] Delete Settings Select';
export const DELETE_SETTINGS_SELECT_SUCCESS = '[Settings] Delete Settings Select Success';
export const DELETE_SETTINGS_SELECT_FAIL = '[Settings] Delete Settings Select Fail';

export class LoadSettingsSelectListAction implements Action {
    type = LOAD_SETTINGS_SELECT_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadSettingsSelectListWipAction implements Action {
    type = LOAD_SETTINGS_SELECT_LIST_WIP;

    constructor(public payload: {} = null) { }
}

export class LoadSettingsSelectListSuccessAction implements Action {
    type = LOAD_SETTINGS_SELECT_LIST_SUCCESS;

    constructor(public payload: SettingsSelect[]) { }
}

export class LoadSettingsSelectListFailAction implements Action {
    type = LOAD_SETTINGS_SELECT_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewSettingsSelectAction implements Action {
    type = ADD_NEW_SETTINGS_SELECT;

    constructor(public payload: SettingsSelect) { }
}

export class AddNewSettingsSelectSuccessAction implements Action {
    type = ADD_NEW_SETTINGS_SELECT_SUCCESS;

    constructor(public payload: SettingsSelect) { }
}

export class AddNewSettingsSelectFailAction implements Action {
    type = ADD_NEW_SETTINGS_SELECT_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditSettingsSelectAction implements Action {
    type = EDIT_SETTINGS_SELECT;

    constructor(public payload: SettingsSelect) { }
}

export class EditSettingsSelectSuccessAction implements Action {
    type = EDIT_SETTINGS_SELECT_SUCCESS;

    constructor(public payload: SettingsSelect) { }
}

export class EditSettingsSelectFailAction implements Action {
    type = EDIT_SETTINGS_SELECT_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteSettingsSelectAction implements Action {
    type = DELETE_SETTINGS_SELECT;

    constructor(public payload: SettingsSelect) { }
}

export class DeleteSettingsSelectSuccessAction implements Action {
    type = DELETE_SETTINGS_SELECT_SUCCESS;

    constructor(public payload: SettingsSelect) { }
}

export class DeleteSettingsSelectFailAction implements Action {
    type = DELETE_SETTINGS_SELECT_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadSettingsSelectListAction
    | LoadSettingsSelectListWipAction
    | LoadSettingsSelectListSuccessAction
    | LoadSettingsSelectListFailAction
    | AddNewSettingsSelectAction
    | AddNewSettingsSelectSuccessAction
    | AddNewSettingsSelectFailAction
    | EditSettingsSelectAction
    | EditSettingsSelectSuccessAction
    | EditSettingsSelectFailAction
    | DeleteSettingsSelectAction
    | DeleteSettingsSelectSuccessAction
    | DeleteSettingsSelectFailAction;
