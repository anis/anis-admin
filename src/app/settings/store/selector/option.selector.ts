import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as option from '../reducer/option.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getOptionState = createSelector(
    reducer.getSettingsState,
    (state: reducer.State) => state.option
);

export const getOptionListIsLoading = createSelector(
    getOptionState,
    option.getOptionListIsLoading
);

export const getOptiontListIsLoaded = createSelector(
    getOptionState,
    option.getOptionListIsLoaded
);

export const getOptionList = createSelector(
    getOptionState,
    option.getOptionList
);

export const getOptionBySelectName = createSelector(
    getOptionList,
    selectRouterState,
    (optionList, router) => optionList.filter(option => option.select_name === router.state.params.select)
)

export const getOptionByRouteId = createSelector(
    getOptionList,
    selectRouterState,
    (optionList, router) => optionList.find(option => option.id === +router.state.params.oid)
);
