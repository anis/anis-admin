import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as select from '../reducer/select.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getSelectState = createSelector(
    reducer.getSettingsState,
    (state: reducer.State) => state.select
);

export const getSelectListIsLoading = createSelector(
    getSelectState,
    select.getSelectListIsLoading
);

export const getSelectListIsLoaded = createSelector(
    getSelectState,
    select.getSelectListIsLoaded
);

export const getSelectList = createSelector(
    getSelectState,
    select.getSelectList
);

export const getSelectByRouteName = createSelector(
    getSelectList,
    selectRouterState,
    (selectList, router) => selectList.find(select => select.name === router.state.params.select)
);
