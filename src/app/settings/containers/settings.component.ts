import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { SettingsSelect, SettingsSelectOption } from '../store/model';
import * as selectActions from '../store/action/select.action';
import * as optionActions from '../store/action/option.action';
import * as fromRoot from '../store/reducer';
import * as selectSelector from '../store/selector/select.selector';
import * as optionSelector from '../store/selector/option.selector';

@Component({
    selector: 'app-settings',
    templateUrl: 'settings.component.html'
})
export class SettingsComponent implements OnInit {
    public selectListIsLoading: Observable<boolean>;
    public selectListIsLoaded: Observable<boolean>;
    public selectList: Observable<SettingsSelect[]>;
    public optionListIsLoading: Observable<boolean>;
    public optionListIsLoaded: Observable<boolean>;
    public optionList: Observable<SettingsSelectOption[]>;
    public currentSelect: Observable<SettingsSelect>;

    constructor(private store: Store<fromRoot.State>) {
        this.selectListIsLoading = store.select(selectSelector.getSelectListIsLoading);
        this.selectListIsLoaded = store.select(selectSelector.getSelectListIsLoaded);
        this.selectList = store.select(selectSelector.getSelectList);
        this.optionListIsLoading = store.select(optionSelector.getOptionListIsLoading);
        this.optionListIsLoaded = store.select(optionSelector.getOptiontListIsLoaded);
        this.optionList = store.select(optionSelector.getOptionBySelectName);
        this.currentSelect = store.select(selectSelector.getSelectByRouteName);
    }

    ngOnInit() {
        this.store.dispatch(new selectActions.LoadSettingsSelectListAction());
        this.store.dispatch(new optionActions.LoadSettingsSelectOptionListAction());
    }

    addSelect(settingsSelect: SettingsSelect) {
        this.store.dispatch(new selectActions.AddNewSettingsSelectAction(settingsSelect));
    }

    editSelect(settingsSelect: SettingsSelect) {
        this.store.dispatch(new selectActions.EditSettingsSelectAction(settingsSelect));
    }

    deleteSelect(settingsSelect: SettingsSelect) {
        this.store.dispatch(new selectActions.DeleteSettingsSelectAction(settingsSelect));
    }

    addOption(settingsSelectOption: SettingsSelectOption) {
        this.store.dispatch(new optionActions.AddNewSettingsSelectOptionAction(settingsSelectOption));
    }

    editOption(settingsSelectOption: SettingsSelectOption) {
        this.store.dispatch(new optionActions.EditSettingsSelectOptionAction(settingsSelectOption));
    }

    deleteOption(settingsSelectOption: SettingsSelectOption) {
        this.store.dispatch(new optionActions.DeleteSettingsSelectOptionAction(settingsSelectOption));
    }
}
