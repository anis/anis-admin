import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Dataset, Attribute, Column, CriteriaFamily, OutputCategory, OutputFamily } from '../../store/model';
import { SettingsSelect, SettingsSelectOption } from '../../../settings/store/model';
import * as fromMetamodel from '../../store/reducer';
import * as fromSettings from '../../../settings/store/reducer';
import * as instanceSelector from '../../store/selector/instance.selector';
import * as datasetActions from '../../store/action/dataset.action';
import * as datasetSelector from '../../store/selector/dataset.selector';
import * as attributeActions from '../../store/action/attribute.action';
import * as attributeSelector from '../../store/selector/attribute.selector';
import * as criteriaFamilyActions from '../../store/action/criteria-family.action';
import * as criteriaFamilySelector from '../../store/selector/criteria-family.selector';
import * as outputFamilyActions from '../../store/action/output-family.action';
import * as outputFamilySelector from '../../store/selector/output-family.selector';
import * as outputCategoryActions from '../../store/action/output-category.action';
import * as outputCategorySelector from '../../store/selector/output-category.selector';
import * as selectActions from '../../../settings/store/action/select.action';
import * as selectSelector from '../../../settings/store/selector/select.selector';
import * as optionActions from '../../../settings/store/action/option.action';
import * as optionSelector from '../../../settings/store/selector/option.selector';
import * as databaseActions from '../../store/action/database.action';
import * as databaseSelector from '../../store/selector/database.selector';

@Component({
    selector: 'app-attribute',
    templateUrl: 'attribute.component.html',
    styleUrls: [ 'attribute.component.css' ]
})
export class AttributeComponent implements OnInit {
    public instanceSelected: Observable<string>;
    public datasetSelected: Observable<string>;
    public dataset: Observable<Dataset>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public tabSelected: Observable<string>;
    public attributeList: Observable<Attribute[]>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public columnList: Observable<Column[]>;
    public columnListIsLoading: Observable<boolean>;
    public columnListIsLoaded: Observable<boolean>;
    public optionListGenerated: Observable<string[]>;
    public criteriaFamilyList: Observable<CriteriaFamily[]>;
    public outputFamilyList: Observable<OutputFamily[]>;
    public outputCategoryList: Observable<OutputCategory[]>;
    public settingsSelectList: Observable<SettingsSelect[]>;
    public settingsSelectOptionList: Observable<SettingsSelectOption[]>;

    constructor(private store: Store<{settings: fromSettings.State, metamodel: fromMetamodel.State}>) {
        this.instanceSelected = store.select(instanceSelector.getInstanceSelected);
        this.datasetSelected = store.select(datasetSelector.getDatasetSelected);
        this.dataset = store.select(datasetSelector.getDatasetByRouteName);
        this.datasetListIsLoading = store.select(datasetSelector.getDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.getDatasetListIsLoaded);
        this.tabSelected = store.select(attributeSelector.getTabSelectedQueryParam);
        this.attributeList = store.select(attributeSelector.getAttributeList);
        this.attributeListIsLoading = store.select(attributeSelector.getAttributeListIsLoading);
        this.attributeListIsLoaded = store.select(attributeSelector.getAttributeListIsLoaded);
        this.columnList = store.select(databaseSelector.getColumnList);
        this.columnListIsLoading = store.select(databaseSelector.getColumnListIsLoading);
        this.columnListIsLoaded = store.select(databaseSelector.getColumnListIsLoaded);
        this.optionListGenerated = store.select(attributeSelector.getOptionListGenerated);
        this.criteriaFamilyList = store.select(criteriaFamilySelector.getCriteriaFamilyList);
        this.outputFamilyList = store.select(outputFamilySelector.getOutputFamilyList);
        this.outputCategoryList = store.select(outputCategorySelector.getOutputCategoryList);
        this.settingsSelectList = store.select(selectSelector.getSelectList);
        this.settingsSelectOptionList = store.select(optionSelector.getOptionList);
    }

    ngOnInit() {
        this.store.dispatch(new datasetActions.LoadDatasetListAction());
        this.store.dispatch(new attributeActions.LoadAttributeListAction());
        this.store.dispatch(new selectActions.LoadSettingsSelectListAction());
        this.store.dispatch(new optionActions.LoadSettingsSelectOptionListAction());
        this.store.dispatch(new criteriaFamilyActions.LoadCriteriaFamilyListAction());
        this.store.dispatch(new outputFamilyActions.LoadOutputFamilyListAction());
        this.store.dispatch(new outputCategoryActions.LoadOutputCategoryListAction());
        this.store.dispatch(new databaseActions.LoadColumnListAction());
    }

    addCriteriaFamily(criteriaFamily: CriteriaFamily) {
        this.store.dispatch(new criteriaFamilyActions.AddNewCriteriaFamilyAction(criteriaFamily));
    }

    editCriteriaFamily(criteriaFamily: CriteriaFamily) {
        this.store.dispatch(new criteriaFamilyActions.EditCriteriaFamilyAction(criteriaFamily));
    }

    deleteCriteriaFamily(criteriaFamily: CriteriaFamily) {
        this.store.dispatch(new criteriaFamilyActions.DeleteCriteriaFamilyAction(criteriaFamily));
    }

    addOutputFamily(outputFamily: OutputFamily) {
        this.store.dispatch(new outputFamilyActions.AddNewOutputFamilyAction(outputFamily));
    }

    editOutputFamily(outputFamily: OutputFamily) {
        this.store.dispatch(new outputFamilyActions.EditOutputFamilyAction(outputFamily));
    }

    deleteOutputFamily(outputFamily: OutputFamily) {
        this.store.dispatch(new outputFamilyActions.DeleteOutputFamilyAction(outputFamily));
    }

    addOutputCategory(outputCategory: OutputCategory) {
        this.store.dispatch(new outputCategoryActions.AddNewOutputCategoryAction(outputCategory));
    }

    editOutputCategory(outputCategory: OutputCategory) {
        this.store.dispatch(new outputCategoryActions.EditOutputCategoryAction(outputCategory));
    }

    deleteOutputCategory(outputCategory: OutputCategory) {
        this.store.dispatch(new outputCategoryActions.DeleteOutputCategoryAction(outputCategory));
    }

    addAttribute(attribute: Attribute) {
        this.store.dispatch(new attributeActions.AddNewAttributeAction(attribute));
    }

    editAttribute(attribute: Attribute) {
        this.store.dispatch(new attributeActions.EditAttributeAction(attribute));
    }

    deleteAttribute(attribute: Attribute) {
        this.store.dispatch(new attributeActions.DeleteAttributeAction(attribute));
    }

    generateAttributeOptionList(attribute: Attribute) {
        this.store.dispatch(new attributeActions.GenerateOptionListAction(attribute));
    }
}
