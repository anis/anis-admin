import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, DatasetFamily, Dataset } from '../../store/model';
import * as instanceActions from '../../store/action/instance.action';
import * as instanceSelector from '../../store/selector/instance.selector';
import * as datasetFamilyActions from '../../store/action/dataset-family.action';
import * as datasetFamilySelector from '../../store/selector/dataset-family.selector';
import * as datasetActions from '../../store/action/dataset.action';
import * as datasetSelector from '../../store/selector/dataset.selector';
import * as metamodelReducer from '../../store/reducer';

@Component({
    selector: 'app-configure-instance',
    templateUrl: 'configure-instance.component.html'
})
export class ConfigureInstanceComponent implements OnInit {
    public instance: Observable<Instance>;
    public instanceListIsLoading: Observable<boolean>;
    public instanceListIsLoaded: Observable<boolean>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetList: Observable<Dataset[]>;

    constructor(private store: Store<metamodelReducer.State>) {
        this.instance = store.select(instanceSelector.getInstanceByRouteName);
        this.instanceListIsLoading = store.select(instanceSelector.getInstanceListIsLoading);
        this.instanceListIsLoaded = store.select(instanceSelector.getInstanceListIsLoaded);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.getDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.getDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.getDatasetFamilyList);
        this.datasetListIsLoading = store.select(datasetSelector.getDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.getDatasetListIsLoaded);
        this.datasetList = store.select(datasetSelector.getDatasetList);
    }

    ngOnInit() {
        this.store.dispatch(new instanceActions.LoadInstanceListAction());
        this.store.dispatch(new datasetFamilyActions.LoadDatasetFamilyListAction());
        this.store.dispatch(new datasetActions.LoadDatasetListAction());
    }

    addDatasetFamily(datasetFamily: DatasetFamily) {
        this.store.dispatch(new datasetFamilyActions.AddNewDatasetFamilyAction(datasetFamily));
    }

    editDatasetFamily(datasetFamily: DatasetFamily) {
        this.store.dispatch(new datasetFamilyActions.EditDatasetFamilyAction(datasetFamily));
    }

    deleteDatasetFamily(datasetFamily: DatasetFamily) {
        this.store.dispatch(new datasetFamilyActions.DeleteDatasetFamilyAction(datasetFamily));
    }

    deleteDataset(dataset: Dataset) {
        this.store.dispatch(new datasetActions.DeleteDatasetAction(dataset));
    }
}
