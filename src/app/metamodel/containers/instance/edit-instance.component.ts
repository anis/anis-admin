import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Instance } from '../../store/model';
import * as instanceActions from '../../store/action/instance.action';
import * as instanceReducer from '../../store/reducer/instance.reducer';
import * as instanceSelector from '../../store/selector/instance.selector';

@Component({
    selector: 'app-edit-instance',
    templateUrl: 'edit-instance.component.html'
})
export class EditInstanceComponent implements OnInit {
    public instanceListIsLoading: Observable<boolean>;
    public instanceListIsLoaded: Observable<boolean>;
    public instance: Observable<Instance>;

    constructor(private store: Store<instanceReducer.State>) {
        this.instanceListIsLoading = store.select(instanceSelector.getInstanceListIsLoading);
        this.instanceListIsLoaded = store.select(instanceSelector.getInstanceListIsLoaded);
        this.instance = store.select(instanceSelector.getInstanceByRouteName);
    }

    ngOnInit() {
        this.store.dispatch(new instanceActions.LoadInstanceListAction());
    }

    editInstance(instance: Instance) {
        this.store.dispatch(new instanceActions.EditInstanceAction(instance));
    }
}
