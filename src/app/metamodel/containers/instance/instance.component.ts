import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance } from '../../store/model';
import * as instanceActions from '../../store/action/instance.action';
import * as instanceReducer from '../../store/reducer/instance.reducer';
import * as instanceSelector from '../../store/selector/instance.selector';

@Component({
    selector: 'app-instance',
    templateUrl: 'instance.component.html',
    styleUrls: [ 'instance.component.css' ]
})
export class InstanceComponent implements OnInit {
    public instanceListIsLoading: Observable<boolean>;
    public instanceListIsLoaded: Observable<boolean>;
    public instanceList: Observable<Instance[]>;

    constructor(private store: Store<instanceReducer.State>) {
        this.instanceListIsLoading = store.select(instanceSelector.getInstanceListIsLoading);
        this.instanceListIsLoaded = store.select(instanceSelector.getInstanceListIsLoaded);
        this.instanceList = store.select(instanceSelector.getInstanceList);
    }

    ngOnInit() {
        this.store.dispatch(new instanceActions.LoadInstanceListAction());
    }

    deleteInstance(instance: Instance) {
        this.store.dispatch(new instanceActions.DeleteInstanceAction(instance));
    }
}
