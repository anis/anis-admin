import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { Instance } from '../../store/model';
import * as instanceActions from '../../store/action/instance.action';
import * as instanceReducer from '../../store/reducer/instance.reducer';

@Component({
    selector: 'app-new-instance',
    templateUrl: 'new-instance.component.html'
})
export class NewInstanceComponent {
    constructor(private store: Store<instanceReducer.State>) { }

    addNewInstance(instance: Instance) {
        this.store.dispatch(new instanceActions.AddNewInstanceAction(instance));
    }
}
