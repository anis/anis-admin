import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Group, Dataset } from '../../store/model';
import * as groupActions from '../../store/action/group.action';
import * as groupReducer from '../../store/reducer/group.reducer';
import * as instanceSelector from '../../store/selector/instance.selector';
import * as datasetActions from '../../store/action/dataset.action';
import * as datasetSelector from '../../store/selector/dataset.selector';

@Component({
    selector: 'app-new-group',
    templateUrl: 'new-group.component.html'
})
export class NewGroupComponent implements OnInit {
    public instanceSelected: Observable<string>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetList: Observable<Dataset[]>;

    constructor(private store: Store<groupReducer.State>) {
        this.instanceSelected = store.select(instanceSelector.getInstanceSelected);
        this.datasetListIsLoading = store.select(datasetSelector.getDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.getDatasetListIsLoaded);
        this.datasetList = store.select(datasetSelector.getDatasetList);
    }

    ngOnInit() {
        this.store.dispatch(new datasetActions.LoadDatasetListAction());
    }

    addNewGroup(group: Group) {
        this.store.dispatch(new groupActions.AddNewGroupAction(group));
    }
}
