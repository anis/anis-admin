import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Group } from '../../store/model';
import * as groupActions from '../../store/action/group.action';
import * as groupReducer from '../../store/reducer/group.reducer';
import * as groupSelector from '../../store/selector/group.selector';
import * as instanceSelector from '../../store/selector/instance.selector';

@Component({
    selector: 'app-group',
    templateUrl: 'group.component.html'
})
export class GroupComponent implements OnInit {
    public instanceSelected: Observable<string>;
    public groupListIsLoading: Observable<boolean>;
    public groupListIsLoaded: Observable<boolean>;
    public groupList: Observable<Group[]>;

    constructor(private store: Store<groupReducer.State>) {
        this.instanceSelected = store.select(instanceSelector.getInstanceSelected);
        this.groupListIsLoading = store.select(groupSelector.getGroupListIsLoading);
        this.groupListIsLoaded = store.select(groupSelector.getGroupListIsLoaded);
        this.groupList = store.select(groupSelector.getGroupList);
    }

    ngOnInit() {
        this.store.dispatch(new groupActions.LoadGroupListAction());
    }

    deleteGroup(group: Group) {
        this.store.dispatch(new groupActions.DeleteGroupAction(group));
    }
}
