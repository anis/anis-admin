import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Survey, DatasetFamily, Dataset, FileInfo } from '../../store/model';
import * as surveyActions from '../../store/action/survey.action';
import * as surveySelector from '../../store/selector/survey.selector';
import * as databaseActions from '../../store/action/database.action';
import * as databaseSelector from '../../store/selector/database.selector';
import * as datasetFamilyActions from '../../store/action/dataset-family.action';
import * as datasetFamilySelector from '../../store/selector/dataset-family.selector';
import * as datasetActions from '../../store/action/dataset.action';
import * as fileExplorerAction from '../../store/action/file-explorer.action';
import * as fileExplorerSelector from '../../store/selector/file-explorer.selector';
import * as instanceSelector from '../../store/selector/instance.selector';
import * as metamodelReducer from '../../store/reducer';

@Component({
    selector: 'app-new-dataset',
    templateUrl: 'new-dataset.component.html'
})
export class NewDatasetComponent implements OnInit {
    public instanceSelected: Observable<string>;
    public surveyListIsLoading: Observable<boolean>;
    public surveyListIsLoaded: Observable<boolean>;
    public surveyList: Observable<Survey[]>;
    public tableListIsLoading: Observable<boolean>;
    public tableListIsLoaded: Observable<boolean>;
    public tableList: Observable<string[]>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public idDatasetFamilyQueryParam: Observable<number>;
    public rootDirectoryInfo: Observable<FileInfo[]>;
    public rootDirectoryInfoIsLoading: Observable<boolean>;
    public rootDirectoryInfoIsLoaded: Observable<boolean>;

    constructor(private store: Store<metamodelReducer.State>) {
        this.instanceSelected = store.select(instanceSelector.getInstanceSelected);
        this.surveyListIsLoading = store.select(surveySelector.getSurveyListIsLoading);
        this.surveyListIsLoaded = store.select(surveySelector.getSurveyListIsLoaded);
        this.surveyList = store.select(surveySelector.getSurveyList);
        this.tableListIsLoading = store.select(databaseSelector.getTableListIsLoading);
        this.tableListIsLoaded = store.select(databaseSelector.getTableListIsLoaded);
        this.tableList = store.select(databaseSelector.getTableList);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.getDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.getDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.getDatasetFamilyList);
        this.idDatasetFamilyQueryParam = store.select(datasetFamilySelector.getIdDatasetFamilyQueryParam);
        this.rootDirectoryInfo = store.select(fileExplorerSelector.getRootDirectoryInfo);
        this.rootDirectoryInfoIsLoading = store.select(fileExplorerSelector.getRootDirectoryInfoIsLoading);
        this.rootDirectoryInfoIsLoaded = store.select(fileExplorerSelector.getRootDirectoryInfoIsLoaded);
    }

    ngOnInit() {
        this.store.dispatch(new surveyActions.LoadSurveyListAction());
        this.store.dispatch(new datasetFamilyActions.LoadDatasetFamilyListAction());
    }

    loadTableList(idDatabase: number) {
        this.store.dispatch(new databaseActions.LoadTableListAction(idDatabase));
    }
    
    loadRootDirectoryInfo(path: string) {
        this.store.dispatch(new fileExplorerAction.LoadRootDirectoryInfoAction(path));
    }

    addNewDataset(dataset: Dataset) {
        this.store.dispatch(new datasetActions.AddNewDatasetAction(dataset));
    }
}
