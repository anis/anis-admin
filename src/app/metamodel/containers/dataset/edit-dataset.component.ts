import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { DatasetFamily, Dataset, Attribute, FileInfo, ImageLimit } from '../../store/model';
import * as datasetFamilyActions from '../../store/action/dataset-family.action';
import * as datasetFamilySelector from '../../store/selector/dataset-family.selector';
import * as datasetActions from '../../store/action/dataset.action';
import * as datasetSelector from '../../store/selector/dataset.selector';
import * as fileExplorerAction from '../../store/action/file-explorer.action';
import * as fileExplorerSelector from '../../store/selector/file-explorer.selector';
import * as attributeActions from '../../store/action/attribute.action';
import * as attributeSelector from '../../store/selector/attribute.selector';
import * as instanceSelector from '../../store/selector/instance.selector';
import * as metamodelReducer from '../../store/reducer';

@Component({
    selector: 'app-edit-dataset',
    templateUrl: 'edit-dataset.component.html'
})
export class EditDatasetComponent implements OnInit {
    public instanceSelected: Observable<string>;
    public datasetSelected: Observable<string>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public dataset: Observable<Dataset>;
    public rootDirectoryInfo: Observable<FileInfo[]>;
    public rootDirectoryInfoIsLoading: Observable<boolean>;
    public rootDirectoryInfoIsLoaded: Observable<boolean>;
    public datasetDirectoryInfo: Observable<FileInfo[]>;
    public datasetDirectoryInfoIsLoading: Observable<boolean>;
    public datasetDirectoryInfoIsLoaded: Observable<boolean>;
    public attributeList: Observable<Attribute[]>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public imageLimitIsLoading: Observable<boolean>;
    public imageLimitIsLoaded: Observable<boolean>;
    public imageLimit: Observable<ImageLimit>;

    constructor(private store: Store<metamodelReducer.State>) {
        this.instanceSelected = store.select(instanceSelector.getInstanceSelected);
        this.datasetSelected = store.select(datasetSelector.getDatasetSelected);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.getDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.getDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.getDatasetFamilyList);
        this.datasetListIsLoading = store.select(datasetSelector.getDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.getDatasetListIsLoaded);
        this.dataset = store.select(datasetSelector.getDatasetByRouteName);
        this.rootDirectoryInfo = store.select(fileExplorerSelector.getRootDirectoryInfo);
        this.rootDirectoryInfoIsLoading = store.select(fileExplorerSelector.getRootDirectoryInfoIsLoading);
        this.rootDirectoryInfoIsLoaded = store.select(fileExplorerSelector.getRootDirectoryInfoIsLoaded);
        this.datasetDirectoryInfo = store.select(fileExplorerSelector.getDatasetDirectoryInfo);
        this.datasetDirectoryInfoIsLoading = store.select(fileExplorerSelector.getDatasetDirectoryInfoIsLoading);
        this.datasetDirectoryInfoIsLoaded = store.select(fileExplorerSelector.getDatasetDirectoryInfoIsLoaded);
        this.attributeList = store.select(attributeSelector.getAttributeList);
        this.attributeListIsLoading = store.select(attributeSelector.getAttributeListIsLoading);
        this.attributeListIsLoaded = store.select(attributeSelector.getAttributeListIsLoaded);
        this.imageLimitIsLoading = store.select(fileExplorerSelector.getImageLimitIsLoading);
        this.imageLimitIsLoaded = store.select(fileExplorerSelector.getImageLimitIsLoaded);
        this.imageLimit = store.select(fileExplorerSelector.getImageLimit);
    }

    ngOnInit() {
        this.store.dispatch(new datasetFamilyActions.LoadDatasetFamilyListAction());
        this.store.dispatch(new datasetActions.LoadDatasetListAction());
        this.store.dispatch(new attributeActions.LoadAttributeListAction());
    }

    loadRootDirectoryInfo(path: string) {
        this.store.dispatch(new fileExplorerAction.LoadRootDirectoryInfoAction(path));
    }

    loadDatasetDirectoryInfo(path: string) {
        this.store.dispatch(new fileExplorerAction.LoadDatasetDirectoryInfoAction(path));
    }

    loadFitsImageLimits(filename: string) {
        this.store.dispatch(new fileExplorerAction.LoadImageLimit(filename));
    }

    editDataset(dataset: Dataset) {
        this.store.dispatch(new datasetActions.EditDatasetAction(dataset));
    }
}
