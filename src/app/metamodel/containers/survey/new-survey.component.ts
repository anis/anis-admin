import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Survey, Database } from '../../store/model';
import * as surveyActions from '../../store/action/survey.action';
import * as databaseActions from '../../store/action/database.action';
import * as reducer from '../../store/reducer';
import * as databaseSelector from '../../store/selector/database.selector';

@Component({
    selector: 'app-new-survey',
    templateUrl: 'new-survey.component.html'
})
export class NewSurveyComponent implements OnInit {
    public databaseList: Observable<Database[]>;

    constructor(private store: Store<reducer.State>) {
        this.databaseList = this.store.select(databaseSelector.getDatabaseList);
    }

    ngOnInit() {
        this.store.dispatch(new databaseActions.LoadDatabaseListAction());
    }

    addNewSurvey(survey: Survey) {
        this.store.dispatch(new surveyActions.AddNewSurveyAction(survey));
    }
}
