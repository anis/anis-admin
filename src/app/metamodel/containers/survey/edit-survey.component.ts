import { Component, OnInit} from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Survey, Database } from '../../store/model';
import * as surveyActions from '../../store/action/survey.action';
import * as databaseActions from '../../store/action/database.action';
import * as surveySelector from '../../store/selector/survey.selector';
import * as databaseSelector from '../../store/selector/database.selector';
import * as reducer from '../../store/reducer';

@Component({
    selector: 'app-edit-survey',
    templateUrl: 'edit-survey.component.html'
})
export class EditSurveyComponent implements OnInit {
    public surveyListIsLoading: Observable<boolean>;
    public surveyListIsLoaded: Observable<boolean>;
    public survey: Observable<Survey>;
    public databaseList: Observable<Database[]>;

    constructor(private store: Store<reducer.State>) {
        this.surveyListIsLoading = store.select(surveySelector.getSurveyListIsLoading);
        this.surveyListIsLoaded = store.select(surveySelector.getSurveyListIsLoaded);
        this.survey = store.select(surveySelector.getSurveyByRouteName);
        this.databaseList = this.store.select(databaseSelector.getDatabaseList);
    }

    ngOnInit() {
        this.store.dispatch(new surveyActions.LoadSurveyListAction());
        this.store.dispatch(new databaseActions.LoadDatabaseListAction());
    }

    editSurvey(survey: Survey) {
        this.store.dispatch(new surveyActions.EditSurveyAction(survey));
    }
}
