import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-survey-page',
    templateUrl: 'survey-page.component.html'
})
export class SurveyPageComponent {
    constructor(private router: Router) {}

    isSurveyList() {
        return this.router.url === '/survey/survey-list';
    }

    isDatabaseList() {
        return this.router.url === '/survey/database-list';
    }
}
