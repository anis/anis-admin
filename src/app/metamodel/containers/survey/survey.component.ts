import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Survey, Database } from '../../store/model';
import * as surveyActions from '../../store/action/survey.action';
import * as databaseActions from '../../store/action/database.action';
import * as reducer from '../../store/reducer';
import * as surveySelector from '../../store/selector/survey.selector';
import * as databaseSelector from '../../store/selector/database.selector';

@Component({
    selector: 'app-survey',
    templateUrl: 'survey.component.html'
})
export class SurveyComponent implements OnInit {
    public surveyListIsLoading: Observable<boolean>;
    public surveyListIsLoaded: Observable<boolean>;
    public surveyList: Observable<Survey[]>;
    public databaseListIsLoading: Observable<boolean>;
    public databaseListIsLoaded: Observable<boolean>;
    public databaseList: Observable<Database[]>;

    constructor(private store: Store<reducer.State>) {
        this.surveyListIsLoading = store.select(surveySelector.getSurveyListIsLoading);
        this.surveyListIsLoaded = store.select(surveySelector.getSurveyListIsLoaded);
        this.surveyList = store.select(surveySelector.getSurveyList);
        this.databaseListIsLoading = store.select(databaseSelector.getDatabaseListIsLoading);
        this.databaseListIsLoaded = store.select(databaseSelector.getDatabaseListIsLoaded);
        this.databaseList = store.select(databaseSelector.getDatabaseList);
    }

    ngOnInit() {
        this.store.dispatch(new surveyActions.LoadSurveyListAction());
        this.store.dispatch(new databaseActions.LoadDatabaseListAction());
    }

    deleteSurvey(survey: Survey) {
        this.store.dispatch(new surveyActions.DeleteSurveyAction(survey));
    }
}
