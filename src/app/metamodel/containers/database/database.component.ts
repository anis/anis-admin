import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Database, Survey } from '../../store/model';
import * as databaseActions from '../../store/action/database.action';
import * as surveyActions from '../../store/action/survey.action';
import * as databaseReducer from '../../store/reducer/database.reducer';
import * as databaseSelector from '../../store/selector/database.selector';
import * as surveySelector from '../../store/selector/survey.selector';

@Component({
    selector: 'app-database',
    templateUrl: 'database.component.html'
})
export class DatabaseComponent implements OnInit {
    public databaseListIsLoading: Observable<boolean>;
    public databaseListIsLoaded: Observable<boolean>;
    public databaseList: Observable<Database[]>;
    public surveyListIsLoading: Observable<boolean>;
    public surveyListIsLoaded: Observable<boolean>;
    public surveyList: Observable<Survey[]>;

    constructor(private store: Store<databaseReducer.State>) {
        this.databaseListIsLoading = store.select(databaseSelector.getDatabaseListIsLoading);
        this.databaseListIsLoaded = store.select(databaseSelector.getDatabaseListIsLoaded);
        this.databaseList = store.select(databaseSelector.getDatabaseList);
        this.surveyListIsLoading = store.select(surveySelector.getSurveyListIsLoading);
        this.surveyListIsLoaded = store.select(surveySelector.getSurveyListIsLoaded);
        this.surveyList = store.select(surveySelector.getSurveyList);
    }

    ngOnInit() {
        this.store.dispatch(new databaseActions.LoadDatabaseListAction());
        this.store.dispatch(new surveyActions.LoadSurveyListAction());
    }

    deleteDatabase(database: Database) {
        this.store.dispatch(new databaseActions.DeleteDatabaseAction(database));
    }
}
