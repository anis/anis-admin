import { Component, OnInit} from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Database } from '../../store/model';
import * as databaseActions from '../../store/action/database.action';
import * as databaseReducer from '../../store/reducer/database.reducer';
import * as databaseSelector from '../../store/selector/database.selector';

@Component({
    selector: 'app-edit-database',
    templateUrl: 'edit-database.component.html'
})
export class EditDatabaseComponent implements OnInit {
    public databaseListIsLoading: Observable<boolean>;
    public databaseListIsLoaded: Observable<boolean>;
    public database: Observable<Database>;

    constructor(private store: Store<databaseReducer.State>) {
        this.databaseListIsLoading = store.select(databaseSelector.getDatabaseListIsLoading);
        this.databaseListIsLoaded = store.select(databaseSelector.getDatabaseListIsLoaded);
        this.database = store.select(databaseSelector.getDatabaseByRouteId);
    }

    ngOnInit() {
        this.store.dispatch(new databaseActions.LoadDatabaseListAction());
    }

    editDatabase(database: Database) {
        this.store.dispatch(new databaseActions.EditDatabaseAction(database));
    }
}
