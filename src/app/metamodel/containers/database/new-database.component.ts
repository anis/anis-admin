import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { Database } from '../../store/model';
import * as databaseActions from '../../store/action/database.action';
import * as databaseReducer from '../../store/reducer/database.reducer';

@Component({
    selector: 'app-new-database',
    templateUrl: 'new-database.component.html'
})
export class NewDatabaseComponent {
    constructor(private store: Store<databaseReducer.State>) { }

    addNewDatabase(database: Database) {
        this.store.dispatch(new databaseActions.AddNewDatabaseAction(database));
    }
}
