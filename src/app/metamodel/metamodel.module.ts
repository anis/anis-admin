import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '../shared/shared.module';
import { MetamodelRoutingModule, routedComponents } from './metamodel.routing';
import { dummiesComponents } from './components';
import { reducer } from './store/reducer';
import { metamodelEffects } from './store/effects';
import { metamodelServices} from './store/service';

@NgModule({
    imports: [
        SharedModule,
        MetamodelRoutingModule,
        StoreModule.forFeature('metamodel', reducer),
        EffectsModule.forFeature(metamodelEffects)
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ],
    providers: metamodelServices
})
export class MetamodelModule { }
