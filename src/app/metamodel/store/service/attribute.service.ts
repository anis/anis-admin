import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Attribute } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class AttributeService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveAttributeList(datasetName: string): Observable<Attribute[]> {
        return this.http.get<Attribute[]>(this.API_PATH + 'dataset/' + datasetName + '/attribute').pipe(
            map(attributeList => [...attributeList].sort(sortAttributeListByOutputDisplay))
        );
    }

    addAttribute(datasetName: string, attribute: Attribute): Observable<Attribute> {
        return this.http.post<Attribute>(this.API_PATH + 'dataset/' + datasetName + '/attribute', attribute);
    }

    editAttribute(datasetName: string, attribute: Attribute): Observable<Attribute> {
        return this.http.put<Attribute>(this.API_PATH + 'dataset/' + datasetName + '/attribute/' + attribute.id, attribute);
    }

    deleteAttribute(datasetName: string, attribute: Attribute) {
        return this.http.delete(this.API_PATH + 'dataset/' + datasetName + '/attribute/' + attribute.id); 
    }

    generateOptionList(datasetName: string, attribute: Attribute): Observable<string[]> {
        return this.http.get<string[]>(this.API_PATH + 'dataset/' + datasetName + '/attribute/' + attribute.id + '/distinct');
    }
}

export const sortAttributeListByOutputDisplay = (a: Attribute, b: Attribute) => a.output_display - b.output_display;
