import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Instance } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class InstanceService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveInstanceList(): Observable<Instance[]> {
        return this.http.get<Instance[]>(this.API_PATH + 'instance');
    }

    addInstance(newInstance: Instance): Observable<Instance> {
        return this.http.post<Instance>(this.API_PATH + 'instance', newInstance);
    }

    editInstance(instance: Instance): Observable<Instance> {
        return this.http.put<Instance>(this.API_PATH + 'instance/' + instance.name, instance);
    }

    deleteInstance(instanceName: string) {
        return this.http.delete(this.API_PATH + 'instance/' + instanceName);
    }
}
