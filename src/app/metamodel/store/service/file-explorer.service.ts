import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { FileInfo, ImageLimit } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class FileExplorerService {
    private API_PATH: string = environment.apiUrl + '/';
    private SERVICES_PATH: string = environment.servicesUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveRootDirectoryInfo(path: string): Observable<FileInfo[]> {
        return this.http.get<FileInfo[]>(this.API_PATH + 'file-explorer/' + path);
    }

    retrieveDatasetDirectoryInfo(datasetName: string, path: string): Observable<FileInfo[]> {
        return this.http.get<FileInfo[]>(this.API_PATH + 'dataset-file-explorer/' + datasetName + path);
    }

    retrieveFitsImageLimits(datasetName: string, filename: string) {
        return this.http.get<ImageLimit>(this.SERVICES_PATH + 'get-fits-image-limits/' + datasetName + '?filename=' + filename);
    }
}
