import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { sortDisplay } from '../../../shared/utils';
import { Dataset } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class DatasetService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveDatasetList(instanceName: string): Observable<Dataset[]> {
        return this.http.get<Dataset[]>(this.API_PATH + 'instance/' + instanceName + '/dataset').pipe(
            map(datasetList => [...datasetList].sort(sortDisplay))
        );
    }

    addDataset(newDataset: Dataset): Observable<Dataset> {
        return this.http.post<Dataset>(this.API_PATH + 'dataset-family/' + newDataset.id_dataset_family + '/dataset', newDataset);
    }

    editDataset(dataset: Dataset): Observable<Dataset> {
        return this.http.put<Dataset>(this.API_PATH + 'dataset/' + dataset.name, dataset);
    }

    deleteDataset(datasetName: string) {
        return this.http.delete(this.API_PATH + 'dataset/' + datasetName);
    }
}
