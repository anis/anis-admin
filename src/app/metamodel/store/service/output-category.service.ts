import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { sortDisplay } from '../../../shared/utils';
import { OutputCategory } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class OutputCategoryService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveOutputCategoryList(datasetName: string): Observable<OutputCategory[]> {
        return this.http.get<OutputCategory[]>(this.API_PATH + 'dataset/' + datasetName + '/output-category').pipe(
            map(outputCategoryList => [...outputCategoryList].sort(sortDisplay))
        );
    }

    addOutputCategory(newOutputCategory: OutputCategory): Observable<OutputCategory> {
        return this.http.post<OutputCategory>(this.API_PATH + 'output-family/' + newOutputCategory.id_output_family + '/output-category', newOutputCategory);
    }

    editOutputCategory(outputCategory: OutputCategory): Observable<OutputCategory> {
        return this.http.put<OutputCategory>(this.API_PATH + 'output-category/' + outputCategory.id, outputCategory);
    }

    deleteOutputCategory(outputCategoryId: number) {
        return this.http.delete(this.API_PATH + 'output-category/' + outputCategoryId);
    }
}
