import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Survey } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class SurveyService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveSurveyList(): Observable<Survey[]> {
        return this.http.get<Survey[]>(this.API_PATH + 'survey');
    }

    addSurvey(newSurvey: Survey): Observable<Survey> {
        return this.http.post<Survey>(this.API_PATH + 'survey', newSurvey);
    }

    editSurvey(survey: Survey): Observable<Survey> {
        return this.http.put<Survey>(this.API_PATH + 'survey/' + survey.name, survey);
    }

    deleteSurvey(surveyName: string) {
        return this.http.delete(this.API_PATH + 'survey/' + surveyName);
    }
}
