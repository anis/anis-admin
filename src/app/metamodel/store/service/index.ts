import { DatabaseService } from './database.service';
import { SurveyService } from './survey.service';
import { InstanceService } from './instance.service';
import { DatasetFamilyService } from './dataset-family.service';
import { DatasetService } from './dataset.service';
import { AttributeService } from './attribute.service';
import { CriteriaFamilyService } from './criteria-family.service';
import { OutputFamilyService } from './output-family.service';
import { OutputCategoryService } from './output-category.service';
import { GroupService } from './group.service';
import { FileExplorerService } from './file-explorer.service';

export const metamodelServices = [
    DatabaseService,
    SurveyService,
    InstanceService,
    DatasetFamilyService,
    DatasetService,
    AttributeService,
    CriteriaFamilyService,
    OutputFamilyService,
    OutputCategoryService,
    GroupService,
    FileExplorerService
];
