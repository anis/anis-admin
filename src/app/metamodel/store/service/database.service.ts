import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Database, Column } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class DatabaseService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveDatabaseList(): Observable<Database[]> {
        return this.http.get<Database[]>(this.API_PATH + 'database');
    }

    retrieveTables(databaseId: number) {
        return this.http.get<string[]>(this.API_PATH + 'database/' + databaseId + '/table');
    }
    
    retrieveColumns(datasetName: string) {
        return this.http.get<Column[]>(this.API_PATH + 'dataset/' + datasetName + '/column');
    }

    addDatabase(newDatabase: Database): Observable<Database> {
        return this.http.post<Database>(this.API_PATH + 'database', newDatabase);
    }

    editDatabase(database: Database): Observable<Database> {
        return this.http.put<Database>(this.API_PATH + 'database/' + database.id, database);
    }

    deleteDatabase(databaseId: number) {
        return this.http.delete(this.API_PATH + 'database/' + databaseId);
    }
}
