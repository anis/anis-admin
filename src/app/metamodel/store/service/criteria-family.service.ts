import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { sortDisplay } from '../../../shared/utils';
import { CriteriaFamily } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class CriteriaFamilyService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveCriteriaFamilyList(datasetName: string): Observable<CriteriaFamily[]> {
        return this.http.get<CriteriaFamily[]>(this.API_PATH + 'dataset/' + datasetName + '/criteria-family').pipe(
            map(criteriaFamilyList => [...criteriaFamilyList].sort(sortDisplay))
        );
    }

    addCriteriaFamily(datasetName: string, newCriteriaFamily: CriteriaFamily): Observable<CriteriaFamily> {
        return this.http.post<CriteriaFamily>(this.API_PATH + 'dataset/' + datasetName + '/criteria-family', newCriteriaFamily);
    }

    editCriteriaFamily(criteriaFamily: CriteriaFamily): Observable<CriteriaFamily> {
        return this.http.put<CriteriaFamily>(this.API_PATH + 'criteria-family/' + criteriaFamily.id, criteriaFamily);
    }

    deleteCriteriaFamily(criteriaFamilyId: number) {
        return this.http.delete(this.API_PATH + 'criteria-family/' + criteriaFamilyId);
    }
}
