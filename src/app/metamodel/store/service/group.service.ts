import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Group } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class GroupService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveGroupList(instanceName: string): Observable<Group[]> {
        return this.http.get<Group[]>(this.API_PATH + 'instance/' + instanceName +  '/group');
    }

    addGroup(instanceName: string, newGroup: Group): Observable<Group> {
        return this.http.post<Group>(this.API_PATH + 'instance/' + instanceName + '/group', newGroup);
    }

    editGroup(group: Group): Observable<Group> {
        return this.http.put<Group>(this.API_PATH + 'group/' + group.id, group);
    }

    deleteGroup(groupId: number) {
        return this.http.delete(this.API_PATH + 'group/' + groupId);
    }
}
