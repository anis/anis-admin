import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { sortDisplay } from '../../../shared/utils';
import { OutputFamily } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class OutputFamilyService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveOutputFamilyList(datasetName: string): Observable<OutputFamily[]> {
        return this.http.get<OutputFamily[]>(this.API_PATH + 'dataset/' + datasetName + '/output-family').pipe(
            map(outputFamilyList => [...outputFamilyList].sort(sortDisplay))
        );
    }

    addOutputFamily(datasetName: string, newOutputFamily: OutputFamily): Observable<OutputFamily> {
        return this.http.post<OutputFamily>(this.API_PATH + 'dataset/' + datasetName + '/output-family', newOutputFamily);
    }

    editOutputFamily(criteriaFamily: OutputFamily): Observable<OutputFamily> {
        return this.http.put<OutputFamily>(this.API_PATH + 'output-family/' + criteriaFamily.id, criteriaFamily);
    }

    deleteOutputFamily(outputFamilyId: number) {
        return this.http.delete(this.API_PATH + 'output-family/' + outputFamilyId);
    }
}
