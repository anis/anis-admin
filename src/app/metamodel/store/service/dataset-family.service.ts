import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { sortDisplay } from '../../../shared/utils';
import { DatasetFamily } from '../model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class DatasetFamilyService {
    private API_PATH: string = environment.apiUrl + '/';

    constructor(private http: HttpClient) { }

    retrieveDatasetFamilyList(instanceName: string): Observable<DatasetFamily[]> {
        return this.http.get<DatasetFamily[]>(this.API_PATH + 'instance/' + instanceName + '/dataset-family').pipe(
            map(datasetFamilyList => datasetFamilyList.sort(sortDisplay))
        );
    }

    addDatasetFamily(instanceName: string, newDatasetFamily: DatasetFamily): Observable<DatasetFamily> {
        return this.http.post<DatasetFamily>(this.API_PATH + 'instance/' + instanceName + '/dataset-family', newDatasetFamily);
    }

    editDatasetFamily(datasetFamily: DatasetFamily): Observable<DatasetFamily> {
        return this.http.put<DatasetFamily>(this.API_PATH + 'dataset-family/' + datasetFamily.id, datasetFamily);
    }

    deleteDatasetFamily(datasetFamilyId: number) {
        return this.http.delete(this.API_PATH + 'dataset-family/' + datasetFamilyId);
    }
}
