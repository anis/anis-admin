import { RendererConfig } from './renderer-config.model';

export interface LinkRendererConfig extends RendererConfig {
    href: string;
    display: string;
    text: string;
    icon: string;
    blank: boolean;
}