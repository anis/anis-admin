import { RendererConfig, DetailRendererConfig, LinkRendererConfig, DownloadRendererConfig, ImageRendererConfig } from './index';

export abstract class RendererConfigFactory {
    static create(renderer: string): RendererConfig {
        switch (renderer) {
            case 'detail':
                return {
                    display: 'text',
                    blank: true
                } as DetailRendererConfig;
            case 'link':
                return {
                    href: '$value',
                    display: 'text',
                    text: '$value',
                    icon: 'fas fa-link',
                    blank: true
                } as LinkRendererConfig;
            case 'download':
                return {
                    display: 'icon-button',
                    text: 'DOWNLOAD',
                    icon: 'fas fa-download'
                } as DownloadRendererConfig;
            case 'image':
                return {
                    type: 'fits',
                    display: 'modal',
                    width: '',
                    height: ''
                } as ImageRendererConfig
        }
    }
}