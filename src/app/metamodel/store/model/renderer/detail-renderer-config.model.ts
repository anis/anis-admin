import { RendererConfig } from './renderer-config.model';

export interface DetailRendererConfig extends RendererConfig {
    display: string;
    icon_button: string;
    blank: boolean;
}