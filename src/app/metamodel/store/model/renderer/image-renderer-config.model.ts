import { RendererConfig } from './renderer-config.model';

export interface ImageRendererConfig extends RendererConfig {
    type: string;
    display: string;
    width: string;
    height: string;
}