import { RendererConfig } from './renderer-config.model';

export interface DownloadRendererConfig extends RendererConfig {
    display: string;
    text: string;
    icon: string;
}