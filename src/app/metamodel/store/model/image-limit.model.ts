export interface ImageLimit {
    ra_min: number;
    ra_max: number;
    dec_min: number;
    dec_max: number;
}
