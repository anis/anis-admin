import { Displayable } from './displayable.model';

export class CriteriaFamily implements Displayable {
    id: number;
    label: string;
    display: number;
}
