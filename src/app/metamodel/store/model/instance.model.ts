export class Instance {
    name: string;
    label: string;
    client_url: string;
    config: {
        authentication: {
            allowed: boolean;
        };
        search: {
            allowed: boolean;
        };
        search_multiple: {
            allowed: boolean;
            all_datasets_selected: boolean;
        };
        documentation: {
            allowed: boolean;
        };
    };
}
