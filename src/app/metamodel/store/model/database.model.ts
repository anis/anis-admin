export class Database {
    id: number;
    label: string;
    dbname: string;
    dbtype: string;
    dbhost: string;
    dbport: number;
    dblogin: string;
    dbpassword: string;
}
