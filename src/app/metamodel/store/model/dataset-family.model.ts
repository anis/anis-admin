import { Displayable } from './displayable.model';

export class DatasetFamily implements Displayable {
    id: number;
    label: string;
    display: number;
}
