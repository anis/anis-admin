export class Group {
    id: number;
    role: string;
    instance_name: string;
    datasets: string[];
}
