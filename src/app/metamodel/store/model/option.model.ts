import { Displayable } from './displayable.model';

export class Option implements Displayable {
    label: string;
    value: string;
    display: number;
}
