import { Option } from './option.model';
import { RendererConfig } from './renderer/renderer-config.model';

export interface Attribute {
    id: number;
    name: string;
    label: string;
    form_label: string;
    description?: string;
    output_display: number;
    criteria_display: number;
    search_flag?: string;
    search_type?: string;
    operator?: string;
    type: string;
    min?: string;
    max?: string;
    placeholder_min?: string;
    placeholder_max?: string;
    renderer?: string;
    renderer_config?: RendererConfig;
    display_detail: number;
    selected?: boolean;
    order_by?: boolean;
    order_display?: number;
    detail?: boolean;
    renderer_detail?: string;
    options?: Option[];
    vo_utype?: string;
    vo_ucd?: string;
    vo_unit?: string;
    vo_description?: string;
    vo_datatype?: string;
    vo_size?: number;
    id_criteria_family?: number;
    id_output_category?: number;
}
