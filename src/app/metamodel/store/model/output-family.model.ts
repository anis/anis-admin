import { Displayable } from './displayable.model';

export class OutputFamily implements Displayable {
    id: number;
    label: string;
    display: number;
}
