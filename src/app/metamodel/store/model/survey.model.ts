export class Survey {
    name: string;
    label: string;
    description: string;
    link: string;
    manager: string;
    id_database: number;
    nb_datasets: number;
}