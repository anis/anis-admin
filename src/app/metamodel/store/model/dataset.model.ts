import { Displayable } from './displayable.model';
import { Image } from './image.model';

export class Dataset implements Displayable {
    name: string;
    table_ref: string;
    label: string;
    description: string;
    display: number;
    count: number;
    vo: boolean;
    data_path: string;
    survey_name: string;
    id_dataset_family: number;
    public: boolean;
    config: {
        images: Image[],
        cone_search: {
            enabled: boolean;
            opened: boolean;
            column_ra: number;
            column_dec: number;
            plot_enabled: boolean;
            sdss_enabled: boolean;
            sdss_display: number;
            background: {id: number, enabled: boolean, display: number}[];
        },
        download: {
            enabled: boolean;
            opened: boolean;
            csv: boolean;
            ascii: boolean;
            vo: boolean;
            archive: boolean;
        },
        summary: {
            enabled: boolean;
            opened: boolean;
        },
        server_link: {
            enabled: boolean;
            opened: boolean;
        },
        samp: {
            enabled: boolean;
            opened: boolean;
        },
        datatable: {
            enabled: boolean;
            opened: boolean;
            selectable_row: boolean;
        }
    };

    constructor() {
        this.public = true;
        this.config = {
            images: [],
            cone_search: {
                enabled: false,
                opened: false,
                column_ra: null,
                column_dec: null,
                plot_enabled: false,
                sdss_enabled: false,
                sdss_display: 10,
                background: []
            },
            download: {
                enabled: true,
                opened: false,
                csv: true,
                ascii: true,
                vo: false,
                archive: true
            },
            summary: {
                enabled: true,
                opened: false
            },
            server_link: {
                enabled: false,
                opened: false
            },
            samp: {
                enabled: false,
                opened: false
            },
            datatable: {
                enabled: true,
                opened: false,
                selectable_row: false
            }
        }
    }
}
