import { Displayable } from './displayable.model';

export class OutputCategory implements Displayable {
    id: number;
    label: string;
    display: number;
    id_output_family: number;
}
