import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromRouter from '../../../shared/utils';
import { Group } from '../model';
import * as groupActions from '../action/group.action';
import { GroupService } from '../service/group.service';

@Injectable()
export class GroupEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState}>,
        private groupService: GroupService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadGroupListAction$ = this.actions$.pipe(
        ofType(groupActions.LOAD_GROUP_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const instanceName = state.router.state.params.iname;
            return this.groupService.retrieveGroupList(instanceName).pipe(
                map((groupList: Group[]) =>
                    new groupActions.LoadGroupListSuccessAction(groupList)),
                catchError(() => of(new groupActions.LoadGroupListFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    loadGroupListFailedAction$ = this.actions$.pipe(
        ofType(groupActions.LOAD_GROUP_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Group list loading failed'))
    );

    @Effect()
    addNewGroupAction$ = this.actions$.pipe(
        ofType(groupActions.ADD_NEW_GROUP),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const addNewGroupAction = action as groupActions.AddNewGroupAction;
            const instanceName = state.router.state.params.iname;
            return this.groupService.addGroup(instanceName, addNewGroupAction.payload).pipe(
                map((newGroup: Group) => new groupActions.AddNewGroupSuccessAction(newGroup)),
                catchError(() => of(new groupActions.AddNewGroupFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewGroupSuccessAction$ = this.actions$.pipe(
        ofType(groupActions.ADD_NEW_GROUP_SUCCESS),
        withLatestFrom(this.store$),
        map(([action, state]) => {
            const instanceName = state.router.state.params.iname;
            this.router.navigateByUrl('/configure-instance/' + instanceName + '/group');
            this.toastr.success('Add group success!', 'The new group has been created!');
        })
    );

    @Effect({dispatch: false})
    addNewGroupFailedAction$ = this.actions$.pipe(
        ofType(groupActions.ADD_NEW_GROUP_FAIL),
        map(_ => this.toastr.error('Add group failed!', 'The new group could not be created into the database'))
    );

    @Effect()
    editGroupAction$ = this.actions$.pipe(
        ofType(groupActions.EDIT_GROUP),
        switchMap(action => {
            const editGroupAction = action as groupActions.EditGroupAction;
            return this.groupService.editGroup(editGroupAction.payload).pipe(
                map((group: Group) => new groupActions.EditGroupSuccessAction(group)),
                catchError(() => of(new groupActions.EditGroupFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editGroupSuccessAction$ = this.actions$.pipe(
        ofType(groupActions.EDIT_GROUP_SUCCESS),
        withLatestFrom(this.store$),
        map(([action, state]) => {
            const instanceName = state.router.state.params.iname;
            this.router.navigateByUrl('/configure-instance/' + instanceName + '/group');
            this.toastr.success('Edit group success!', 'The existing entity has been edited into the database')
        })
    );

    @Effect({dispatch: false})
    editGroupFailedAction$ = this.actions$.pipe(
        ofType(groupActions.EDIT_GROUP_FAIL),
        map(_ => this.toastr.error('Edit group failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteGroupAction$ = this.actions$.pipe(
        ofType(groupActions.DELETE_GROUP),
        switchMap(action => {
            const deleteGroupAction = action as groupActions.DeleteGroupAction;
            return this.groupService.deleteGroup(deleteGroupAction.payload.id).pipe(
                map(_ => new groupActions.DeleteGroupSuccessAction(deleteGroupAction.payload)),
                catchError(() => of(new groupActions.DeleteGroupFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteGroupSuccessAction$ = this.actions$.pipe(
        ofType(groupActions.DELETE_GROUP_SUCCESS),
        map(_ => {
            this.toastr.success('Delete group success!', 'The group has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteGroupFailedAction$ = this.actions$.pipe(
        ofType(groupActions.DELETE_GROUP_FAIL),
        map(_ => this.toastr.error('Delete group failed!', 'The group could not be deleted into the database'))
    );
}
