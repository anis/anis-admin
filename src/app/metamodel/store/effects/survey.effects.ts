import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromMetamodel from '../reducer';
import { Survey } from '../model';
import * as surveyActions from '../action/survey.action';
import { SurveyService } from '../service/survey.service';

@Injectable()
export class SurveyEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{metamodel: fromMetamodel.State}>,
        private surveyService: SurveyService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadSurveyListAction$ = this.actions$.pipe(
        ofType(surveyActions.LOAD_SURVEY_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.metamodel.survey.surveyListIsLoaded) {
                return of({ type: '[No Action] [Survey] Survey list is already loaded' });
            } else {
                return of(new surveyActions.LoadSurveyListWipAction());
            }
        })
    );

    @Effect()
    loadSurveyListWipAction$ = this.actions$.pipe(
        ofType(surveyActions.LOAD_SURVEY_LIST_WIP),
        switchMap(_ =>
            this.surveyService.retrieveSurveyList().pipe(
                map((surveyList: Survey[]) =>
                    new surveyActions.LoadSurveyListSuccessAction(surveyList)),
                catchError(() => of(new surveyActions.LoadSurveyListFailAction()))
            )
        )
    );

    @Effect({ dispatch: false })
    loadSurveyListFailedAction$ = this.actions$.pipe(
        ofType(surveyActions.LOAD_SURVEY_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Survey list loading failed'))
    );

    @Effect()
    addNewSurveyAction$ = this.actions$.pipe(
        ofType(surveyActions.ADD_NEW_SURVEY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const addNewSurveyAction = action as surveyActions.AddNewSurveyAction;
            return this.surveyService.addSurvey(addNewSurveyAction.payload).pipe(
                map((newSurvey: Survey) => new surveyActions.AddNewSurveySuccessAction(newSurvey)),
                catchError(() => of(new surveyActions.AddNewSurveyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewSurveySuccessAction$ = this.actions$.pipe(
        ofType(surveyActions.ADD_NEW_SURVEY_SUCCESS),
        map(_ => {
            this.router.navigate(['/survey/survey-list']);
            this.toastr.success('Add survey success!', 'The new survey has been created!');
        })
    );

    @Effect({dispatch: false})
    addNewSurveyFailedAction$ = this.actions$.pipe(
        ofType(surveyActions.ADD_NEW_SURVEY_FAIL),
        map(_ => this.toastr.error('Add survey failed!', 'The new survey could not be created into the database'))
    );

    @Effect()
    editSurveyAction$ = this.actions$.pipe(
        ofType(surveyActions.EDIT_SURVEY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const editSurveyAction = action as surveyActions.EditSurveyAction;
            return this.surveyService.editSurvey(editSurveyAction.payload).pipe(
                map((survey: Survey) => new surveyActions.EditSurveySuccessAction(survey)),
                catchError(() => of(new surveyActions.EditSurveyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editSurveySuccessAction$ = this.actions$.pipe(
        ofType(surveyActions.EDIT_SURVEY_SUCCESS),
        map(_ => {
            this.router.navigate(['/survey/survey-list']);
            this.toastr.success('Edit survey success!', 'The existing entity has been edited into the database')
        })
    );

    @Effect({dispatch: false})
    editSurveyFailedAction$ = this.actions$.pipe(
        ofType(surveyActions.EDIT_SURVEY_FAIL),
        map(_ => this.toastr.error('Edit survey failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteSurveyAction$ = this.actions$.pipe(
        ofType(surveyActions.DELETE_SURVEY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const deleteSurveyAction = action as surveyActions.DeleteSurveyAction;
            return this.surveyService.deleteSurvey(deleteSurveyAction.payload.name).pipe(
                map(_ => new surveyActions.DeleteSurveySuccessAction(deleteSurveyAction.payload)),
                catchError(() => of(new surveyActions.DeleteSurveyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteSurveySuccessAction$ = this.actions$.pipe(
        ofType(surveyActions.DELETE_SURVEY_SUCCESS),
        map(_ => {
            this.toastr.success('Delete survey success!', 'The survey has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteSurveyFailedAction$ = this.actions$.pipe(
        ofType(surveyActions.DELETE_SURVEY_FAIL),
        map(_ => this.toastr.error('Delete survey failed!', 'The survey could not be deleted into the database'))
    );
}
