import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromRouter from '../../../shared/utils';
import { OutputFamily } from '../model';
import * as outputFamilyActions from '../action/output-family.action';
import { OutputFamilyService } from '../service/output-family.service';

@Injectable()
export class OutputFamilyEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState}>,
        private outputFamilyService: OutputFamilyService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadOutputFamilyListAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.LOAD_OUTPUT_FAMILY_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            return this.outputFamilyService.retrieveOutputFamilyList(datasetName).pipe(
                map((outputFamilyList: OutputFamily[]) =>
                    new outputFamilyActions.LoadOutputFamilyListSuccessAction(outputFamilyList)),
                catchError(() => of(new outputFamilyActions.LoadOutputFamilyListFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    loadOutputFamilyListFailedAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.LOAD_OUTPUT_FAMILY_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Output family list loading failed'))
    );

    @Effect()
    addNewOutputFamilyAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.ADD_NEW_OUTPUT_FAMILY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const addNewOutputFamilyAction = action as outputFamilyActions.AddNewOutputFamilyAction;
            const datasetName = state.router.state.params.dname;
            return this.outputFamilyService.addOutputFamily(datasetName, addNewOutputFamilyAction.payload).pipe(
                map((newOutputFamily: OutputFamily) => new outputFamilyActions.AddNewOutputFamilySuccessAction(newOutputFamily)),
                catchError(() => of(new outputFamilyActions.AddNewOutputFamilyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewOutputFamilySuccessAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.ADD_NEW_OUTPUT_FAMILY_SUCCESS),
        map(_ => this.toastr.success('Add output family success!', 'The new output family has been created!'))
    );

    @Effect({dispatch: false})
    addNewOutputFamilyFailedAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.ADD_NEW_OUTPUT_FAMILY_FAIL),
        map(_ => this.toastr.error('Add output family failed!', 'The new output family could not be created into the database'))
    );

    @Effect()
    editOutputFamilyAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.EDIT_OUTPUT_FAMILY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const editOutputFamilyAction = action as outputFamilyActions.EditOutputFamilyAction;
            return this.outputFamilyService.editOutputFamily(editOutputFamilyAction.payload).pipe(
                map((datasetfamily: OutputFamily) => new outputFamilyActions.EditOutputFamilySuccessAction(datasetfamily)),
                catchError(() => of(new outputFamilyActions.EditOutputFamilyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editOutputFamilySuccessAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.EDIT_OUTPUT_FAMILY_SUCCESS),
        map(_ => this.toastr.success('Edit output family success!', 'The existing entity has been edited into the database'))
    );

    @Effect({dispatch: false})
    editOutputFamilyFailedAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.EDIT_OUTPUT_FAMILY_FAIL),
        map(_ => this.toastr.error('Edit output family failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteOutputFamilyAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.DELETE_OUTPUT_FAMILY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const deleteOutputFamilyAction = action as outputFamilyActions.DeleteOutputFamilyAction;
            return this.outputFamilyService.deleteOutputFamily(deleteOutputFamilyAction.payload.id).pipe(
                map(_ => new outputFamilyActions.DeleteOutputFamilySuccessAction(deleteOutputFamilyAction.payload)),
                catchError(() => of(new outputFamilyActions.DeleteOutputFamilyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteOutputFamilySuccessAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.DELETE_OUTPUT_FAMILY_SUCCESS),
        map(_ => this.toastr.success('Delete output family success!', 'The output family has been deleted!'))
    );

    @Effect({dispatch: false})
    deleteOutputFamilyFailedAction$ = this.actions$.pipe(
        ofType(outputFamilyActions.DELETE_OUTPUT_FAMILY_FAIL),
        map(_ => this.toastr.error('Delete output family failed!', 'The output family could not be deleted into the database'))
    );
}
