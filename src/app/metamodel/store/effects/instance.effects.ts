import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromMetamodel from '../reducer';
import { Instance } from '../model';
import * as instanceActions from '../action/instance.action';
import { InstanceService } from '../service/instance.service';

@Injectable()
export class InstanceEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{metamodel: fromMetamodel.State}>,
        private instanceService: InstanceService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadInstanceListAction$ = this.actions$.pipe(
        ofType(instanceActions.LOAD_INSTANCE_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.metamodel.instance.instanceListIsLoaded) {
                return of({ type: '[No Action] [Instance] Instance list is already loaded' });
            } else {
                return of(new instanceActions.LoadInstanceListWipAction());
            }
        })
    );

    @Effect()
    loadInstanceListWipAction$ = this.actions$.pipe(
        ofType(instanceActions.LOAD_INSTANCE_LIST_WIP),
        switchMap(_ =>
            this.instanceService.retrieveInstanceList().pipe(
                map((instanceList: Instance[]) =>
                    new instanceActions.LoadInstanceListSuccessAction(instanceList)),
                catchError(() => of(new instanceActions.LoadInstanceListFailAction()))
            )
        )
    );

    @Effect({ dispatch: false })
    loadInstanceListFailedAction$ = this.actions$.pipe(
        ofType(instanceActions.LOAD_INSTANCE_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Instance list loading failed'))
    );

    @Effect()
    addNewInstanceAction$ = this.actions$.pipe(
        ofType(instanceActions.ADD_NEW_INSTANCE),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const addNewInstanceAction = action as instanceActions.AddNewInstanceAction;
            return this.instanceService.addInstance(addNewInstanceAction.payload).pipe(
                map((newInstance: Instance) => new instanceActions.AddNewInstanceSuccessAction(newInstance)),
                catchError(() => of(new instanceActions.AddNewInstanceFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewInstanceSuccessAction$ = this.actions$.pipe(
        ofType(instanceActions.ADD_NEW_INSTANCE_SUCCESS),
        map(_ => {
            this.router.navigate(['/instance-list']);
            this.toastr.success('Add instance success!', 'The new instance has been created!');
        })
    );

    @Effect({dispatch: false})
    addNewInstanceFailedAction$ = this.actions$.pipe(
        ofType(instanceActions.ADD_NEW_INSTANCE_FAIL),
        map(_ => this.toastr.error('Add instance failed!', 'The new instance could not be created into the instance'))
    );

    @Effect()
    editInstanceAction$ = this.actions$.pipe(
        ofType(instanceActions.EDIT_INSTANCE),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const editInstanceAction = action as instanceActions.EditInstanceAction;
            return this.instanceService.editInstance(editInstanceAction.payload).pipe(
                map((instance: Instance) => new instanceActions.EditInstanceSuccessAction(instance)),
                catchError(() => of(new instanceActions.EditInstanceFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editInstanceSuccessAction$ = this.actions$.pipe(
        ofType(instanceActions.EDIT_INSTANCE_SUCCESS),
        map(_ => {
            this.router.navigate(['/instance-list']);
            this.toastr.success('Edit instance success!', 'The existing entity has been edited into the instance')
        })
    );

    @Effect({dispatch: false})
    editInstanceFailedAction$ = this.actions$.pipe(
        ofType(instanceActions.EDIT_INSTANCE_FAIL),
        map(_ => this.toastr.error('Edit instance failed!', 'The existing entity could not be edited into the instance'))
    );

    @Effect()
    deleteInstanceAction$ = this.actions$.pipe(
        ofType(instanceActions.DELETE_INSTANCE),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const deleteInstanceAction = action as instanceActions.DeleteInstanceAction;
            return this.instanceService.deleteInstance(deleteInstanceAction.payload.name).pipe(
                map(_ => new instanceActions.DeleteInstanceSuccessAction(deleteInstanceAction.payload)),
                catchError(() => of(new instanceActions.DeleteInstanceFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteInstanceSuccessAction$ = this.actions$.pipe(
        ofType(instanceActions.DELETE_INSTANCE_SUCCESS),
        map(_ => {
            this.toastr.success('Delete instance success!', 'The instance has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteInstanceFailedAction$ = this.actions$.pipe(
        ofType(instanceActions.DELETE_INSTANCE_FAIL),
        map(_ => this.toastr.error('Delete instance failed!', 'The instance could not be deleted into the instance'))
    );
}
