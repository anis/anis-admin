import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromRouter from '../../../shared/utils';
import { OutputCategory } from '../model';
import * as outputCategoryActions from '../action/output-category.action';
import { OutputCategoryService } from '../service/output-category.service';

@Injectable()
export class OutputCategoryEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState}>,
        private outputCategoryService: OutputCategoryService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadOutputCategoryListAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.LOAD_OUTPUT_CATEGORY_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            return this.outputCategoryService.retrieveOutputCategoryList(datasetName).pipe(
                map((outputCategoryList: OutputCategory[]) =>
                    new outputCategoryActions.LoadOutputCategoryListSuccessAction(outputCategoryList)),
                catchError(() => of(new outputCategoryActions.LoadOutputCategoryListFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    loadOutputCategoryListFailedAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.LOAD_OUTPUT_CATEGORY_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Output category list loading failed'))
    );

    @Effect()
    addNewOutputCategoryAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.ADD_NEW_OUTPUT_CATEGORY),
        switchMap(action => {
            const addNewOutputCategoryAction = action as outputCategoryActions.AddNewOutputCategoryAction;
            return this.outputCategoryService.addOutputCategory(addNewOutputCategoryAction.payload).pipe(
                map((newOutputCategory: OutputCategory) => new outputCategoryActions.AddNewOutputCategorySuccessAction(newOutputCategory)),
                catchError(() => of(new outputCategoryActions.AddNewOutputCategoryFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewOutputCategorySuccessAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.ADD_NEW_OUTPUT_CATEGORY_SUCCESS),
        map(_ => this.toastr.success('Add output category success!', 'The new output category has been created!'))
    );

    @Effect({dispatch: false})
    addNewOutputCategoryFailedAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.ADD_NEW_OUTPUT_CATEGORY_FAIL),
        map(_ => this.toastr.error('Add output category failed!', 'The new output category could not be created into the database'))
    );

    @Effect()
    editOutputCategoryAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.EDIT_OUTPUT_CATEGORY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const editOutputCategoryAction = action as outputCategoryActions.EditOutputCategoryAction;
            return this.outputCategoryService.editOutputCategory(editOutputCategoryAction.payload).pipe(
                map((outputcategory: OutputCategory) => new outputCategoryActions.EditOutputCategorySuccessAction(outputcategory)),
                catchError(() => of(new outputCategoryActions.EditOutputCategoryFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editOutputCategorySuccessAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.EDIT_OUTPUT_CATEGORY_SUCCESS),
        map(_ => this.toastr.success('Edit output category success!', 'The existing entity has been edited into the database'))
    );

    @Effect({dispatch: false})
    editOutputCategoryFailedAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.EDIT_OUTPUT_CATEGORY_FAIL),
        map(_ => this.toastr.error('Edit output category failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteOutputCategoryAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.DELETE_OUTPUT_CATEGORY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const deleteOutputCategoryAction = action as outputCategoryActions.DeleteOutputCategoryAction;
            return this.outputCategoryService.deleteOutputCategory(deleteOutputCategoryAction.payload.id).pipe(
                map(_ => new outputCategoryActions.DeleteOutputCategorySuccessAction(deleteOutputCategoryAction.payload)),
                catchError(() => of(new outputCategoryActions.DeleteOutputCategoryFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteOutputCategorySuccessAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.DELETE_OUTPUT_CATEGORY_SUCCESS),
        map(_ => {
            this.toastr.success('Delete output category success!', 'The output category has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteOutputCategoryFailedAction$ = this.actions$.pipe(
        ofType(outputCategoryActions.DELETE_OUTPUT_CATEGORY_FAIL),
        map(_ => this.toastr.error('Delete output category failed!', 'The output category could not be deleted into the database'))
    );
}
