import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromRouter from '../../../shared/utils';
import { Dataset } from '../model';
import * as datasetActions from '../action/dataset.action';
import * as surveyActions from '../action/survey.action';
import { DatasetService } from '../service/dataset.service';

@Injectable()
export class DatasetEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState}>,
        private datasetService: DatasetService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadDatasetListAction$ = this.actions$.pipe(
        ofType(datasetActions.LOAD_DATASET_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const instanceName = state.router.state.params.iname;
            return this.datasetService.retrieveDatasetList(instanceName).pipe(
                map((datasetList: Dataset[]) =>
                    new datasetActions.LoadDatasetListSuccessAction(datasetList)),
                catchError(() => of(new datasetActions.LoadDatasetListFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    loadDatasetListFailedAction$ = this.actions$.pipe(
        ofType(datasetActions.LOAD_DATASET_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Dataset list loading failed'))
    );

    @Effect()
    addNewDatasetAction$ = this.actions$.pipe(
        ofType(datasetActions.ADD_NEW_DATASET),
        switchMap((action: datasetActions.AddNewDatasetAction) => {
            return this.datasetService.addDataset(action.payload).pipe(
                switchMap((newDataset: Dataset) => [
                    new datasetActions.AddNewDatasetSuccessAction(newDataset),
                    new surveyActions.IncrementNbDatasetsAction(newDataset.survey_name)
                ]),
                catchError(() => of(new datasetActions.AddNewDatasetFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewDatasetSuccessAction$ = this.actions$.pipe(
        ofType(datasetActions.ADD_NEW_DATASET_SUCCESS),
        withLatestFrom(this.store$),
        map(([action, state]) => {
            const instanceName = state.router.state.params.iname;
            this.router.navigate(['/configure-instance/' + instanceName]);
            this.toastr.success('Add dataset success!', 'The new dataset has been created!');
        })
    );

    @Effect({dispatch: false})
    addNewDatasetFailedAction$ = this.actions$.pipe(
        ofType(datasetActions.ADD_NEW_DATASET_FAIL),
        map(_ => this.toastr.error('Add dataset failed!', 'The new dataset could not be created into the database'))
    );

    @Effect()
    editDatasetAction$ = this.actions$.pipe(
        ofType(datasetActions.EDIT_DATASET),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const editDatasetAction = action as datasetActions.EditDatasetAction;
            return this.datasetService.editDataset(editDatasetAction.payload).pipe(
                map((datasetfamily: Dataset) => new datasetActions.EditDatasetSuccessAction(datasetfamily)),
                catchError(() => of(new datasetActions.EditDatasetFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editDatasetSuccessAction$ = this.actions$.pipe(
        ofType(datasetActions.EDIT_DATASET_SUCCESS),
        withLatestFrom(this.store$),
        map(([action, state]) => {
            const instanceName = state.router.state.params.iname;
            this.router.navigate(['/configure-instance/' + instanceName]);
            this.toastr.success('Edit dataset success!', 'The existing entity has been edited into the database')
        })
    );

    @Effect({dispatch: false})
    editDatasetFailedAction$ = this.actions$.pipe(
        ofType(datasetActions.EDIT_DATASET_FAIL),
        map(_ => this.toastr.error('Edit dataset failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteDatasetAction$ = this.actions$.pipe(
        ofType(datasetActions.DELETE_DATASET),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const deleteDatasetAction = action as datasetActions.DeleteDatasetAction;
            return this.datasetService.deleteDataset(deleteDatasetAction.payload.name).pipe(
                switchMap(_ => [
                    new datasetActions.DeleteDatasetSuccessAction(deleteDatasetAction.payload),
                    new surveyActions.DecrementNbDatasetsAction(deleteDatasetAction.payload.survey_name)
                ]),
                catchError(() => of(new datasetActions.DeleteDatasetFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteDatasetSuccessAction$ = this.actions$.pipe(
        ofType(datasetActions.DELETE_DATASET_SUCCESS),
        map(_ => {
            this.toastr.success('Delete dataset success!', 'The datase has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteDatasetFailedAction$ = this.actions$.pipe(
        ofType(datasetActions.DELETE_DATASET_FAIL),
        map(_ => this.toastr.error('Delete dataset failed!', 'The dataset could not be deleted into the database'))
    );
}
