import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromMetamodel from '../reducer';
import * as fromRouter from '../../../shared/utils';
import { Database, Column } from '../model';
import * as databaseActions from '../action/database.action';
import { DatabaseService } from '../service/database.service';

@Injectable()
export class DatabaseEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState, metamodel: fromMetamodel.State}>,
        private databaseService: DatabaseService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadDatabaseListAction$ = this.actions$.pipe(
        ofType(databaseActions.LOAD_DATABASE_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.metamodel.database.databaseListIsLoaded) {
                return of({ type: '[No Action] [Database] Database list is already loaded' });
            } else {
                return of(new databaseActions.LoadDatabaseListWipAction());
            }
        })
    );

    @Effect()
    loadDatabaseListWipAction$ = this.actions$.pipe(
        ofType(databaseActions.LOAD_DATABASE_LIST_WIP),
        switchMap(_ =>
            this.databaseService.retrieveDatabaseList().pipe(
                map((databaseList: Database[]) =>
                    new databaseActions.LoadDatabaseListSuccessAction(databaseList)),
                catchError(() => of(new databaseActions.LoadDatabaseListFailAction()))
            )
        )
    );

    @Effect({ dispatch: false })
    loadDatabaseListFailedAction$ = this.actions$.pipe(
        ofType(databaseActions.LOAD_DATABASE_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Database list loading failed'))
    );

    @Effect()
    loadTableListAction$ = this.actions$.pipe(
        ofType(databaseActions.LOAD_TABLE_LIST),
        switchMap((action: databaseActions.LoadTableListAction) => {
            return this.databaseService.retrieveTables(action.payload).pipe(
                map((tableList: string[]) => new databaseActions.LoadTableListSuccessAction(tableList)),
                catchError(() => of(new databaseActions.LoadTableListFailAction()))
            )
        })
    );

    @Effect()
    loadTableListFailAction$ = this.actions$.pipe(
        ofType(databaseActions.LOAD_TABLE_LIST_FAIL),
        map(_ => this.toastr.error('Loading Failed!', 'Table list loading failed'))
    );

    @Effect()
    loadColumnListAction$ = this.actions$.pipe(
        ofType(databaseActions.LOAD_COLUMN_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            return this.databaseService.retrieveColumns(datasetName).pipe(
                map((columnList: Column[]) => new databaseActions.LoadColumnListSuccessAction(columnList)),
                catchError(() => of(new databaseActions.LoadColumnListFailAction()))
            )
        })
    );

    @Effect()
    loadColumnListFailAction$ = this.actions$.pipe(
        ofType(databaseActions.LOAD_COLUMN_LIST_FAIL),
        map(_ => this.toastr.error('Loading Failed!', 'Column list loading failed'))
    );

    @Effect()
    addNewDatabaseAction$ = this.actions$.pipe(
        ofType(databaseActions.ADD_NEW_DATABASE),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const addNewDatabaseAction = action as databaseActions.AddNewDatabaseAction;
            return this.databaseService.addDatabase(addNewDatabaseAction.payload).pipe(
                map((newDatabase: Database) => new databaseActions.AddNewDatabaseSuccessAction(newDatabase)),
                catchError(() => of(new databaseActions.AddNewDatabaseFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewDatabaseSuccessAction$ = this.actions$.pipe(
        ofType(databaseActions.ADD_NEW_DATABASE_SUCCESS),
        map(_ => {
            this.router.navigate(['/survey/database-list']);
            this.toastr.success('Add database success!', 'The new database has been created!');
        })
    );

    @Effect({dispatch: false})
    addNewDatabaseFailedAction$ = this.actions$.pipe(
        ofType(databaseActions.ADD_NEW_DATABASE_FAIL),
        map(_ => this.toastr.error('Add database failed!', 'The new database could not be created into the database'))
    );

    @Effect()
    editDatabaseAction$ = this.actions$.pipe(
        ofType(databaseActions.EDIT_DATABASE),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const editDatabaseAction = action as databaseActions.EditDatabaseAction;
            return this.databaseService.editDatabase(editDatabaseAction.payload).pipe(
                map((database: Database) => new databaseActions.EditDatabaseSuccessAction(database)),
                catchError(() => of(new databaseActions.EditDatabaseFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editDatabaseSuccessAction$ = this.actions$.pipe(
        ofType(databaseActions.EDIT_DATABASE_SUCCESS),
        map(_ => {
            this.router.navigate(['/survey/database-list']);
            this.toastr.success('Edit database success!', 'The existing entity has been edited into the database')
        })
    );

    @Effect({dispatch: false})
    editDatabaseFailedAction$ = this.actions$.pipe(
        ofType(databaseActions.EDIT_DATABASE_FAIL),
        map(_ => this.toastr.error('Edit database failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteDatabaseAction$ = this.actions$.pipe(
        ofType(databaseActions.DELETE_DATABASE),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const deleteDatabaseAction = action as databaseActions.DeleteDatabaseAction;
            return this.databaseService.deleteDatabase(deleteDatabaseAction.payload.id).pipe(
                map(_ => new databaseActions.DeleteDatabaseSuccessAction(deleteDatabaseAction.payload)),
                catchError(() => of(new databaseActions.DeleteDatabaseFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteDatabaseSuccessAction$ = this.actions$.pipe(
        ofType(databaseActions.DELETE_DATABASE_SUCCESS),
        map(_ => {
            this.toastr.success('Delete database success!', 'The database has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteDatabaseFailedAction$ = this.actions$.pipe(
        ofType(databaseActions.DELETE_DATABASE_FAIL),
        map(_ => this.toastr.error('Delete database failed!', 'The database could not be deleted into the database'))
    );
}
