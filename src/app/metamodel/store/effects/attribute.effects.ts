import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store'
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, withLatestFrom, catchError } from 'rxjs/operators';

import * as fromRouter from '../../../shared/utils';
import { Attribute } from '../model';
import * as attributeActions from '../action/attribute.action';
import { AttributeService } from '../service/attribute.service';

@Injectable()
export class AttributeEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState}>,
        private attributeService: AttributeService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadAttributeListAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_ATTRIBUTE_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            return this.attributeService.retrieveAttributeList(datasetName).pipe(
                map((attributes: Attribute[]) => new attributeActions.LoadAttributeListSuccessAction(attributes)),
                catchError(() => of(new attributeActions.LoadAttributeListFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    loadAttributeListFailAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_ATTRIBUTE_LIST_FAIL),
        map(_ => this.toastr.error('Loading Failed!', 'The attribute list loading failed'))
    );

    @Effect()
    generateOptionListAction$ = this.actions$.pipe(
        ofType(attributeActions.GENERATE_OPTION_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            const generateOptionListAction = action as attributeActions.GenerateOptionListAction;
            return this.attributeService.generateOptionList(datasetName, generateOptionListAction.payload).pipe(
                map((optionListGenerated: string[]) => new attributeActions.GenerateOptionListSuccessAction(optionListGenerated)),
                catchError(() => of(new attributeActions.GenerateOptionListFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    generateOptionListFailAction$ = this.actions$.pipe(
        ofType(attributeActions.GENERATE_OPTION_LIST_FAIL),
        map(_ => this.toastr.error('Loading Failed!', 'Generate option list failed'))
    );

    @Effect()
    addNewAttributeAction$ = this.actions$.pipe(
        ofType(attributeActions.ADD_NEW_ATTRIBUTE),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            const addNewAttributeAction = action as attributeActions.AddNewAttributeAction;
            return this.attributeService.addAttribute(datasetName, addNewAttributeAction.payload).pipe(
                map((attribute: Attribute) => new attributeActions.AddNewAttributeSuccessAction(attribute)),
                catchError(() => of(new attributeActions.AddNewAttributeFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewAttributeSuccessAction$ = this.actions$.pipe(
        ofType(attributeActions.ADD_NEW_ATTRIBUTE_SUCCESS),
        map(action => {
            this.toastr.success('Add attribute success!', 'The new attribute has been created!');
        })
    );

    @Effect({dispatch: false})
    addNewAttributeFailedAction$ = this.actions$.pipe(
        ofType(attributeActions.ADD_NEW_ATTRIBUTE_FAIL),
        map(_ => this.toastr.error('Add attribute failed!', 'The new attribute could not be created into the database'))
    );

    @Effect()
    editAttributeAction$ = this.actions$.pipe(
        ofType(attributeActions.EDIT_ATTRIBUTE),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            const editAttributeAction = action as attributeActions.EditAttributeAction;
            return this.attributeService.editAttribute(datasetName, editAttributeAction.payload).pipe(
                map((attribute: Attribute) => new attributeActions.EditAttributeSuccessAction(attribute)),
                catchError(() => of(new attributeActions.EditAttributeFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    editAttributeSuccessAction$ = this.actions$.pipe(
        ofType(attributeActions.EDIT_ATTRIBUTE_SUCCESS),
        map(_ => {
            this.toastr.success('Edit attribute success!', 'The existing entities has been edited into the database');
        })
    );

    @Effect({ dispatch: false })
    editAttributeFailAction$ = this.actions$.pipe(
        ofType(attributeActions.EDIT_ATTRIBUTE_FAIL),
        map(_ => this.toastr.error('Edit attribute failed!', 'The existing entities could not be edited into the database'))
    );

    @Effect()
    deleteAttributeAction$ = this.actions$.pipe(
        ofType(attributeActions.DELETE_ATTRIBUTE),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            const deleteAttributeAction = action as attributeActions.DeleteAttributeAction;
            return this.attributeService.deleteAttribute(datasetName, deleteAttributeAction.payload).pipe(
                map(_ => new attributeActions.DeleteAttributeSuccessAction(deleteAttributeAction.payload)),
                catchError(() => of(new attributeActions.DeleteAttributeFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteDatabaseSuccessAction$ = this.actions$.pipe(
        ofType(attributeActions.DELETE_ATTRIBUTE_SUCCESS),
        map(_ => {
            this.toastr.success('Delete attribute success!', 'The attribute has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteAttributeFailedAction$ = this.actions$.pipe(
        ofType(attributeActions.DELETE_ATTRIBUTE_FAIL),
        map(_ => this.toastr.error('Delete attribute failed!', 'The attribute could not be deleted into the database'))
    );
}
