import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromRouter from '../../../shared/utils';
import { CriteriaFamily } from '../model';
import * as criteriaFamilyActions from '../action/criteria-family.action';
import { CriteriaFamilyService } from '../service/criteria-family.service';

@Injectable()
export class CriteriaFamilyEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState}>,
        private criteriaFamilyService: CriteriaFamilyService,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadCriteriaFamilyListAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.LOAD_CRITERIA_FAMILY_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            return this.criteriaFamilyService.retrieveCriteriaFamilyList(datasetName).pipe(
                map((criteriaFamilyList: CriteriaFamily[]) =>
                    new criteriaFamilyActions.LoadCriteriaFamilyListSuccessAction(criteriaFamilyList)),
                catchError(() => of(new criteriaFamilyActions.LoadCriteriaFamilyListFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    loadCriteriaFamilyListFailedAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.LOAD_CRITERIA_FAMILY_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Criteria family list loading failed'))
    );

    @Effect()
    addNewCriteriaFamilyAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.ADD_NEW_CRITERIA_FAMILY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const addNewCriteriaFamilyAction = action as criteriaFamilyActions.AddNewCriteriaFamilyAction;
            const datasetName = state.router.state.params.dname;
            return this.criteriaFamilyService.addCriteriaFamily(datasetName, addNewCriteriaFamilyAction.payload).pipe(
                map((newCriteriaFamily: CriteriaFamily) => new criteriaFamilyActions.AddNewCriteriaFamilySuccessAction(newCriteriaFamily)),
                catchError(() => of(new criteriaFamilyActions.AddNewCriteriaFamilyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewCriteriaFamilySuccessAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.ADD_NEW_CRITERIA_FAMILY_SUCCESS),
        map(_ => this.toastr.success('Add criteria family success!', 'The new criteria family has been created!'))
    );

    @Effect({dispatch: false})
    addNewCriteriaFamilyFailedAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.ADD_NEW_CRITERIA_FAMILY_FAIL),
        map(_ => this.toastr.error('Add criteria family failed!', 'The new criteria family could not be created into the database'))
    );

    @Effect()
    editCriteriaFamilyAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.EDIT_CRITERIA_FAMILY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const editCriteriaFamilyAction = action as criteriaFamilyActions.EditCriteriaFamilyAction;
            return this.criteriaFamilyService.editCriteriaFamily(editCriteriaFamilyAction.payload).pipe(
                map((datasetfamily: CriteriaFamily) => new criteriaFamilyActions.EditCriteriaFamilySuccessAction(datasetfamily)),
                catchError(() => of(new criteriaFamilyActions.EditCriteriaFamilyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editCriteriaFamilySuccessAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.EDIT_CRITERIA_FAMILY_SUCCESS),
        map(_ => this.toastr.success('Edit criteria family success!', 'The existing entity has been edited into the database'))
    );

    @Effect({dispatch: false})
    editCriteriaFamilyFailedAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.EDIT_CRITERIA_FAMILY_FAIL),
        map(_ => this.toastr.error('Edit criteria family failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteCriteriaFamilyAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.DELETE_CRITERIA_FAMILY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const deleteCriteriaFamilyAction = action as criteriaFamilyActions.DeleteCriteriaFamilyAction;
            return this.criteriaFamilyService.deleteCriteriaFamily(deleteCriteriaFamilyAction.payload.id).pipe(
                map(_ => new criteriaFamilyActions.DeleteCriteriaFamilySuccessAction(deleteCriteriaFamilyAction.payload)),
                catchError(() => of(new criteriaFamilyActions.DeleteCriteriaFamilyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteCriteriaFamilySuccessAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.DELETE_CRITERIA_FAMILY_SUCCESS),
        map(_ => this.toastr.success('Delete criteria family success!', 'The criteria family has been deleted!'))
    );

    @Effect({dispatch: false})
    deleteCriteriaFamilyFailedAction$ = this.actions$.pipe(
        ofType(criteriaFamilyActions.DELETE_CRITERIA_FAMILY_FAIL),
        map(_ => this.toastr.error('Delete criteria family failed!', 'The criteria family could not be deleted into the database'))
    );
}
