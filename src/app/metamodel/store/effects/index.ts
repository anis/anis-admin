import { DatabaseEffects} from './database.effects';
import { SurveyEffects } from './survey.effects';
import { InstanceEffects } from './instance.effects';
import { DatasetFamilyEffects } from './dataset-family.effects';
import { DatasetEffects } from './dataset.effects';
import { AttributeEffects } from './attribute.effects';
import { CriteriaFamilyEffects } from './criteria-family.effects';
import { OutputFamilyEffects } from './output-family.effects';
import { OutputCategoryEffects } from './output-category.effects';
import { GroupEffects } from './group.effects';
import { FileExplorerEffects } from './file-explorer.effects';

export const metamodelEffects = [
    DatabaseEffects,
    SurveyEffects,
    InstanceEffects,
    DatasetFamilyEffects,
    DatasetEffects,
    AttributeEffects,
    CriteriaFamilyEffects,
    OutputFamilyEffects,
    OutputCategoryEffects,
    GroupEffects,
    FileExplorerEffects
];
