import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { switchMap, map, catchError, tap, withLatestFrom } from 'rxjs/operators';

import * as fromRouter from '../../../shared/utils';
import { FileInfo, ImageLimit } from '../model';
import * as fileExplorerActions from '../action/file-explorer.action';
import { FileExplorerService } from '../service/file-explorer.service';

@Injectable()
export class FileExplorerEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState}>,
        private fileExplorerService: FileExplorerService,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadRootDirectoryInfoAction$ = this.actions$.pipe(
        ofType(fileExplorerActions.LOAD_ROOT_DIRECTORY_INFO),
        switchMap((action: fileExplorerActions.LoadRootDirectoryInfoAction) => {
            return this.fileExplorerService.retrieveRootDirectoryInfo(action.payload).pipe(
                map((directoryInfo: FileInfo[]) =>
                    new fileExplorerActions.LoadRootDirectoryInfoSuccessAction(directoryInfo)),
                catchError(() => of(new fileExplorerActions.LoadRootDirectoryInfoFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    loadRootDirectoryInfoFailedAction$ = this.actions$.pipe(
        ofType(fileExplorerActions.LOAD_ROOT_DIRECTORY_INFO_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Directory info failed'))
    );

    @Effect()
    loadDatasetDirectoryInfoAction$ = this.actions$.pipe(
        ofType(fileExplorerActions.LOAD_DATASET_DIRECTORY_INFO),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const loadDatasetDirectoryInfoAction = action as fileExplorerActions.LoadDatasetDirectoryInfoAction;
            const dname = state.router.state.params.dname;
            return this.fileExplorerService.retrieveDatasetDirectoryInfo(dname, loadDatasetDirectoryInfoAction.payload).pipe(
                map((directoryInfo: FileInfo[]) =>
                    new fileExplorerActions.LoadDatasetDirectoryInfoSuccessAction(directoryInfo)),
                catchError(() => of(new fileExplorerActions.LoadDatasetDirectoryInfoFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    loadDatasetDirectoryInfoFailedAction$ = this.actions$.pipe(
        ofType(fileExplorerActions.LOAD_DATASET_DIRECTORY_INFO_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Directory info failed'))
    );

    @Effect()
    loadImageLimitAction$ = this.actions$.pipe(
        ofType(fileExplorerActions.LOAD_IMAGE_LIMIT),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const loadImageLimitAction = action as fileExplorerActions.LoadImageLimit;
            const dname = state.router.state.params.dname;
            return this.fileExplorerService.retrieveFitsImageLimits(dname, loadImageLimitAction.payload).pipe(
                map((imageLimit: ImageLimit) =>
                    new fileExplorerActions.LoadImageLimitSuccess(imageLimit)),
                catchError(() => of(new fileExplorerActions.LoadImageLimitFail()))
            )
        })
    )

    @Effect({ dispatch: false })
    loadImageLimitFailedAction$ = this.actions$.pipe(
        ofType(fileExplorerActions.LOAD_IMAGE_LIMIT_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Image limit info failed'))
    );
}
