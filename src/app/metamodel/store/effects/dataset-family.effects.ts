import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import * as fromRouter from '../../../shared/utils';
import { DatasetFamily } from '../model';
import * as datasetFamilyActions from '../action/dataset-family.action';
import { DatasetFamilyService } from '../service/dataset-family.service';

@Injectable()
export class DatasetFamilyEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{router: fromRouter.RouterReducerState}>,
        private datasetFamilyService: DatasetFamilyService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    @Effect()
    loadDatasetFamilyListAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.LOAD_DATASET_FAMILY_LIST),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const instanceName = state.router.state.params.iname;
            return this.datasetFamilyService.retrieveDatasetFamilyList(instanceName).pipe(
                map((datasetFamilyList: DatasetFamily[]) =>
                    new datasetFamilyActions.LoadDatasetFamilyListSuccessAction(datasetFamilyList)),
                catchError(() => of(new datasetFamilyActions.LoadDatasetFamilyListFailAction()))
            )
        })
    );

    @Effect({ dispatch: false })
    loadDatasetFamilyListFailedAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.LOAD_DATASET_FAMILY_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Dataset family list loading failed'))
    );

    @Effect()
    addNewDatasetFamilyAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.ADD_NEW_DATASET_FAMILY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const addNewDatasetFamilyAction = action as datasetFamilyActions.AddNewDatasetFamilyAction;
            const instanceName = state.router.state.params.iname;
            return this.datasetFamilyService.addDatasetFamily(instanceName, addNewDatasetFamilyAction.payload).pipe(
                map((newDatasetFamily: DatasetFamily) => new datasetFamilyActions.AddNewDatasetFamilySuccessAction(newDatasetFamily)),
                catchError(() => of(new datasetFamilyActions.AddNewDatasetFamilyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    addNewDatasetFamilySuccessAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.ADD_NEW_DATASET_FAMILY_SUCCESS),
        withLatestFrom(this.store$),
        map(([action, state]) => {
            const instanceName = state.router.state.params.iname;
            this.router.navigate(['/configure-instance/' + instanceName]);
            this.toastr.success('Add dataset family success!', 'The new dataset family has been created!');
        })
    );

    @Effect({dispatch: false})
    addNewDatasetFamilyFailedAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.ADD_NEW_DATASET_FAMILY_FAIL),
        map(_ => this.toastr.error('Add dataset family failed!', 'The new dataset family could not be created into the database'))
    );

    @Effect()
    editDatasetFamilyAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.EDIT_DATASET_FAMILY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const editDatasetFamilyAction = action as datasetFamilyActions.EditDatasetFamilyAction;
            return this.datasetFamilyService.editDatasetFamily(editDatasetFamilyAction.payload).pipe(
                map((datasetfamily: DatasetFamily) => new datasetFamilyActions.EditDatasetFamilySuccessAction(datasetfamily)),
                catchError(() => of(new datasetFamilyActions.EditDatasetFamilyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    editDatasetFamilySuccessAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.EDIT_DATASET_FAMILY_SUCCESS),
        withLatestFrom(this.store$),
        map(([action, state]) => {
            const instanceName = state.router.state.params.iname;
            this.router.navigate(['/configure-instance/' + instanceName]);
            this.toastr.success('Edit dataset family success!', 'The existing entity has been edited into the database')
        })
    );

    @Effect({dispatch: false})
    editDatasetFamilyFailedAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.EDIT_DATASET_FAMILY_FAIL),
        map(_ => this.toastr.error('Edit dataset family failed!', 'The existing entity could not be edited into the database'))
    );

    @Effect()
    deleteDatasetFamilyAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.DELETE_DATASET_FAMILY),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const deleteDatasetFamilyAction = action as datasetFamilyActions.DeleteDatasetFamilyAction;
            return this.datasetFamilyService.deleteDatasetFamily(deleteDatasetFamilyAction.payload.id).pipe(
                map(_ => new datasetFamilyActions.DeleteDatasetFamilySuccessAction(deleteDatasetFamilyAction.payload)),
                catchError(() => of(new datasetFamilyActions.DeleteDatasetFamilyFailAction()))
            )
        })
    );

    @Effect({dispatch: false})
    deleteDatasetFamilySuccessAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.DELETE_DATASET_FAMILY_SUCCESS),
        map(_ => {
            this.toastr.success('Delete dataset family success!', 'The dataset family has been deleted!');
        })
    );

    @Effect({dispatch: false})
    deleteDatasetFamilyFailedAction$ = this.actions$.pipe(
        ofType(datasetFamilyActions.DELETE_DATASET_FAMILY_FAIL),
        map(_ => this.toastr.error('Delete dataset family failed!', 'The dataset family could not be deleted into the database'))
    );
}
