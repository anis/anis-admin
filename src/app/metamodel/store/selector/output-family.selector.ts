import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as outputFamily from '../reducer/output-family.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getOutputFamilyState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.outputFamily
);

export const getOutputFamilyListIsLoading = createSelector(
    getOutputFamilyState,
    outputFamily.getOutputFamilyListIsLoading
);

export const getOutputFamilyListIsLoaded = createSelector(
    getOutputFamilyState,
    outputFamily.getOutputFamilyListIsLoaded
);

export const getOutputFamilyList = createSelector(
    getOutputFamilyState,
    outputFamily.getOutputFamilyList
);

export const getOutputFamilyByRouteId = createSelector(
    getOutputFamilyList,
    selectRouterState,
    (outputFamilyList, router) => outputFamilyList.find(outputFamily => outputFamily.id === +router.state.params.ofid)
);
