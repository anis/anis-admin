import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as criteriaFamily from '../reducer/criteria-family.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getCriteriaFamilyState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.criteriaFamily
);

export const getCriteriaFamilyListIsLoading = createSelector(
    getCriteriaFamilyState,
    criteriaFamily.getCriteriaFamilyListIsLoading
);

export const getCriteriaFamilyListIsLoaded = createSelector(
    getCriteriaFamilyState,
    criteriaFamily.getCriteriaFamilyListIsLoaded
);

export const getCriteriaFamilyList = createSelector(
    getCriteriaFamilyState,
    criteriaFamily.getCriteriaFamilyList
);

export const getCriteriaFamilyByRouteId = createSelector(
    getCriteriaFamilyList,
    selectRouterState,
    (criteriaFamilyList, router) => criteriaFamilyList.find(criteriaFamily => criteriaFamily.id === +router.state.params.cfid)
);
