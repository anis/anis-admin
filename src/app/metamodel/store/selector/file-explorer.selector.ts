import { createSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as fileExplorer from '../reducer/file-explorer.reducer';

export const getFileExplorerState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.fileExplorer
);

export const getRootDirectoryInfoIsLoading = createSelector(
    getFileExplorerState,
    fileExplorer.getRootDirectoryInfoIsLoading
);

export const getRootDirectoryInfoIsLoaded = createSelector(
    getFileExplorerState,
    fileExplorer.getRootDirectoryInfoIsLoaded
);

export const getRootDirectoryInfo = createSelector(
    getFileExplorerState,
    fileExplorer.getRootDirectoryInfo
);

export const getDatasetDirectoryInfoIsLoading = createSelector(
    getFileExplorerState,
    fileExplorer.getDatasetDirectoryInfoIsLoading
);

export const getDatasetDirectoryInfoIsLoaded = createSelector(
    getFileExplorerState,
    fileExplorer.getDatasetDirectoryInfoIsLoaded
);

export const getDatasetDirectoryInfo = createSelector(
    getFileExplorerState,
    fileExplorer.getDatasetDirectoryInfo
);

export const getImageLimitIsLoading = createSelector(
    getFileExplorerState,
    fileExplorer.getImageLimitIsLoading
);

export const getImageLimitIsLoaded = createSelector(
    getFileExplorerState,
    fileExplorer.getImageLimitIsLoaded
);

export const getImageLimit= createSelector(
    getFileExplorerState,
    fileExplorer.getImageLimit
);
