import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as attribute from '../reducer/attribute.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getAttributeState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.attribute
);

export const getAttributeListIsLoading = createSelector(
    getAttributeState,
    attribute.getAttributeListIsLoading
);

export const getAttributeListIsLoaded = createSelector(
    getAttributeState,
    attribute.getAttributeListIsLoaded
);

export const getAttributeList = createSelector(
    getAttributeState,
    attribute.getAttributeList
);

export const getOptionListGeneratedIsLoading = createSelector(
    getAttributeState,
    attribute.getOptionListGeneratedIsLoading
);

export const getOptionListGeneratedIsLoaded = createSelector(
    getAttributeState,
    attribute.getOptionListGeneratedIsLoaded
);

export const getOptionListGenerated = createSelector(
    getAttributeState,
    attribute.getOptionListGenerated
);

export const getTabSelectedQueryParam = createSelector(
    selectRouterState,
    (router: RouterReducerState) => (router.state.queryParams.tab_selected) ? router.state.queryParams.tab_selected : 'design'
);