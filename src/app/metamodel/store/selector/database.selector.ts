import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as database from '../reducer/database.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getDatabaseState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.database
);

export const getDatabaseList = createSelector(
    getDatabaseState,
    database.getDatabaseList
);

export const getDatabaseListIsLoading = createSelector(
    getDatabaseState,
    database.getDatabaseListIsLoading
);

export const getDatabaseListIsLoaded = createSelector(
    getDatabaseState,
    database.getDatabaseListIsLoaded
);

export const getTableList = createSelector(
    getDatabaseState,
    database.getTableList
);

export const getTableListIsLoading = createSelector(
    getDatabaseState,
    database.getTableListIsLoading
);

export const getTableListIsLoaded = createSelector(
    getDatabaseState,
    database.getTableListIsLoaded
);

export const getColumnList = createSelector(
    getDatabaseState,
    database.getColumnList
);

export const getColumnListIsLoading = createSelector(
    getDatabaseState,
    database.getColumnListIsLoading
);

export const getColumnListIsLoaded = createSelector(
    getDatabaseState,
    database.getColumnListIsLoaded
);

export const getDatabaseByRouteId = createSelector(
    getDatabaseList,
    selectRouterState,
    (databaseList, router) => databaseList.find(database => database.id === +router.state.params.id)
);
