import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as outputCategory from '../reducer/output-category.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getOutputCategoryState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.outputCategory
);

export const getOutputCategoryListIsLoading = createSelector(
    getOutputCategoryState,
    outputCategory.getOutputCategoryListIsLoading
);

export const getOutputCategoryListIsLoaded = createSelector(
    getOutputCategoryState,
    outputCategory.getOutputCategoryListIsLoaded
);

export const getOutputCategoryList = createSelector(
    getOutputCategoryState,
    outputCategory.getOutputCategoryList
);

export const getOutputCategoryByRouteId = createSelector(
    getOutputCategoryList,
    selectRouterState,
    (outputCategoryList, router) => outputCategoryList.find(outputCategory => outputCategory.id === +router.state.params.ocid)
);
