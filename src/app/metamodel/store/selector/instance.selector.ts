import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as instance from '../reducer/instance.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getInstanceState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.instance
);

export const getInstanceListIsLoading = createSelector(
    getInstanceState,
    instance.getInstanceListIsLoading
);

export const getInstanceListIsLoaded = createSelector(
    getInstanceState,
    instance.getInstanceListIsLoaded
);

export const getInstanceList = createSelector(
    getInstanceState,
    instance.getInstanceList
);

export const getInstanceSelected = createSelector(
    selectRouterState,
    (router: RouterReducerState) => router.state.params.iname
);

export const getInstanceByRouteName = createSelector(
    getInstanceList,
    selectRouterState,
    (instanceList, router) => instanceList.find(instance => instance.name === router.state.params.iname)
);
