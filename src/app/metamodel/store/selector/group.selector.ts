import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as group from '../reducer/group.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getGroupState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.group
);

export const getGroupListIsLoading = createSelector(
    getGroupState,
    group.getGroupListIsLoading
);

export const getGroupListIsLoaded = createSelector(
    getGroupState,
    group.getGroupListIsLoaded
);

export const getGroupList = createSelector(
    getGroupState,
    group.getGroupList
);

export const getGroupByRouteId = createSelector(
    getGroupList,
    selectRouterState,
    (groupList, router) => groupList.find(group => group.id === +router.state.params.id)
);
