import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as datasetFamily from '../reducer/dataset-family.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getDatasetFamilyState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.datasetFamily
);

export const getDatasetFamilyListIsLoading = createSelector(
    getDatasetFamilyState,
    datasetFamily.getDatasetFamilyListIsLoading
);

export const getDatasetFamilyListIsLoaded = createSelector(
    getDatasetFamilyState,
    datasetFamily.getDatasetFamilyListIsLoaded
);

export const getDatasetFamilyList = createSelector(
    getDatasetFamilyState,
    datasetFamily.getDatasetFamilyList
);

export const getDatasetFamilyByRouteId = createSelector(
    getDatasetFamilyList,
    selectRouterState,
    (datasetfamilyList, router) => datasetfamilyList.find(datasetfamily => datasetfamily.id === +router.state.params.id)
);

export const getIdDatasetFamilyQueryParam = createSelector(
    selectRouterState,
    (router: RouterReducerState) => +router.state.queryParams.id_dataset_family
);