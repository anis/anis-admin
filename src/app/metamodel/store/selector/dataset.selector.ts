import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as dataset from '../reducer/dataset.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getDatasetState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.dataset
);

export const getDatasetListIsLoading = createSelector(
    getDatasetState,
    dataset.getDatasetListIsLoading
);

export const getDatasetListIsLoaded = createSelector(
    getDatasetState,
    dataset.getDatasetListIsLoaded
);

export const getDatasetList = createSelector(
    getDatasetState,
    dataset.getDatasetList
);

export const getDatasetSelected = createSelector(
    selectRouterState,
    (router: RouterReducerState) => router.state.params.dname
);

export const getDatasetByRouteName = createSelector(
    getDatasetList,
    selectRouterState,
    (datasetList, router) => datasetList.find(dataset => dataset.name === router.state.params.dname)
);
