import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as reducer from '../reducer';
import * as survey from '../reducer/survey.reducer';
import { RouterReducerState } from '../../../shared/utils';

const selectRouterState = createFeatureSelector<RouterReducerState>('router');

export const getSurveyState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.survey
);

export const getSurveyListIsLoading = createSelector(
    getSurveyState,
    survey.getSurveyListIsLoading
);

export const getSurveyListIsLoaded = createSelector(
    getSurveyState,
    survey.getSurveyListIsLoaded
);

export const getSurveyList = createSelector(
    getSurveyState,
    survey.getSurveyList
);

export const getSurveyByRouteName = createSelector(
    getSurveyList,
    selectRouterState,
    (surveyList, router) => surveyList.find(survey => survey.name === router.state.params.name)
);
