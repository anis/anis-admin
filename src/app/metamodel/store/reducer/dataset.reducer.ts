import * as actions from '../action/dataset.action';

import { sortDisplay } from '../../../shared/utils';
import { Dataset } from '../model';

export interface State {
    datasetListIsLoading: boolean;
    datasetListIsLoaded: boolean;
    datasetList: Dataset[];
}

const initialState: State = {
    datasetListIsLoading: false,
    datasetListIsLoaded: false,
    datasetList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_DATASET_LIST:
            return {
                ...state,
                datasetListIsLoading: true,
                datasetListIsLoaded: false,
                datasetList: []
            }

        case actions.LOAD_DATASET_LIST_SUCCESS:
            const datasetList = action.payload as Dataset[];

            return {
                ...state,
                datasetList,
                datasetListIsLoading: false,
                datasetListIsLoaded: true
            };

        case actions.LOAD_DATASET_LIST_FAIL:
            return {
                ...state,
                datasetListIsLoading: false
            };

        case actions.ADD_NEW_DATASET_SUCCESS:
            const newDataset = action.payload as Dataset;

            return {
                ...state,
                datasetList: [...state.datasetList, newDataset].sort(sortDisplay)
            };

        case actions.EDIT_DATASET_SUCCESS:
            const editedDataset = action.payload as Dataset;

            return {
                ...state,
                datasetList: [...state.datasetList.filter(dataset => dataset.name !== editedDataset.name), editedDataset].sort(sortDisplay)
            };    

        case actions.DELETE_DATASET_SUCCESS:
            const deletedDataset = action.payload as Dataset;

            return {
                ...state,
                datasetList: state.datasetList.filter(dataset => dataset.name !== deletedDataset.name)
            }

        default:
            return state;
    }
}

export const getDatasetListIsLoading = (state: State) => state.datasetListIsLoading;
export const getDatasetListIsLoaded = (state: State) => state.datasetListIsLoaded;
export const getDatasetList = (state: State) => state.datasetList;
