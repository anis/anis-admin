import * as actions from '../action/group.action';

import { Group } from '../model';

export interface State {
    groupListIsLoading: boolean;
    groupListIsLoaded: boolean;
    groupList: Group[];
}

const initialState: State = {
    groupListIsLoading: false,
    groupListIsLoaded: false,
    groupList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_GROUP_LIST:
            return {
                ...state,
                groupListIsLoading: true,
                groupListIsLoaded: false
            };

        case actions.LOAD_GROUP_LIST_SUCCESS:
            const groupList = action.payload as Group[];

            return {
                ...state,
                groupList,
                groupListIsLoading: false,
                groupListIsLoaded: true
            };

        case actions.LOAD_GROUP_LIST_FAIL:
            return {
                ...state,
                groupListIsLoading: false
            };

        case actions.ADD_NEW_GROUP_SUCCESS:
            const newGroup = action.payload as Group;

            return {
                ...state,
                groupList: [...state.groupList, newGroup]
            };

        case actions.EDIT_GROUP_SUCCESS:
            const editedGroup = action.payload as Group;

            return {
                ...state,
                groupList: [...state.groupList.filter(group => group.id !== editedGroup.id), editedGroup]
            };    

        case actions.DELETE_GROUP_SUCCESS:
            const deletedGroup = action.payload as Group;

            return {
                ...state,
                groupList: state.groupList.filter(group => group.id !== deletedGroup.id)
            }

        default:
            return state;
    }
}

export const getGroupListIsLoading = (state: State) => state.groupListIsLoading;
export const getGroupListIsLoaded = (state: State) => state.groupListIsLoaded;
export const getGroupList = (state: State) => state.groupList;