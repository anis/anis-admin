import * as actions from '../action/criteria-family.action';

import { sortDisplay } from '../../../shared/utils';
import { CriteriaFamily } from '../model';

export interface State {
    criteriaFamilyListIsLoading: boolean;
    criteriaFamilyListIsLoaded: boolean;
    criteriaFamilyList: CriteriaFamily[];
}

const initialState: State = {
    criteriaFamilyListIsLoading: false,
    criteriaFamilyListIsLoaded: false,
    criteriaFamilyList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_CRITERIA_FAMILY_LIST:
            return {
                ...state,
                criteriaFamilyListIsLoading: true,
                criteriaFamilyListIsLoaded: false,
                criteriaFamilyList: []
            }

        case actions.LOAD_CRITERIA_FAMILY_LIST_SUCCESS:
            const criteriaFamilyList = action.payload as CriteriaFamily[];

            return {
                ...state,
                criteriaFamilyList,
                criteriaFamilyListIsLoading: false,
                criteriaFamilyListIsLoaded: true
            };

        case actions.LOAD_CRITERIA_FAMILY_LIST_FAIL:
            return {
                ...state,
                criteriaFamilyListIsLoading: false
            };

        case actions.ADD_NEW_CRITERIA_FAMILY_SUCCESS:
            const newCriteriaFamily = action.payload as CriteriaFamily;

            return {
                ...state,
                criteriaFamilyList: [...state.criteriaFamilyList, newCriteriaFamily].sort(sortDisplay)
            };

        case actions.EDIT_CRITERIA_FAMILY_SUCCESS:
            const editedCriteriaFamily = action.payload as CriteriaFamily;

            return {
                ...state,
                criteriaFamilyList: [
                    ...state.criteriaFamilyList.filter(datasetfamily => datasetfamily.id !== editedCriteriaFamily.id),
                    editedCriteriaFamily
                ].sort(sortDisplay)
            };

        case actions.DELETE_CRITERIA_FAMILY_SUCCESS:
            const deletedCriteriaFamily = action.payload as CriteriaFamily;

            return {
                ...state,
                criteriaFamilyList: state.criteriaFamilyList.filter(datasetfamily => datasetfamily.id !== deletedCriteriaFamily.id)
            }

        default:
            return state;
    }
}

export const getCriteriaFamilyListIsLoading = (state: State) => state.criteriaFamilyListIsLoading;
export const getCriteriaFamilyListIsLoaded = (state: State) => state.criteriaFamilyListIsLoaded;
export const getCriteriaFamilyList = (state: State) => state.criteriaFamilyList;
