import { combineReducers, createFeatureSelector } from '@ngrx/store';

import * as survey from './survey.reducer';
import * as database from './database.reducer';
import * as instance from './instance.reducer';
import * as datasetFamily from './dataset-family.reducer';
import * as dataset from './dataset.reducer';
import * as attribute from './attribute.reducer';
import * as criteriaFamily from './criteria-family.reducer';
import * as outputFamily from './output-family.reducer';
import * as outputCategory from './output-category.reducer';
import * as group from './group.reducer';
import * as fileExplorer from './file-explorer.reducer';

export interface State {
    survey: survey.State;
    database: database.State;
    instance: instance.State;
    datasetFamily: datasetFamily.State;
    dataset: dataset.State;
    attribute: attribute.State;
    criteriaFamily: criteriaFamily.State;
    outputFamily: outputFamily.State;
    outputCategory: outputCategory.State;
    group: group.State;
    fileExplorer: fileExplorer.State;
}

const reducers = {
    survey: survey.reducer,
    database: database.reducer,
    instance: instance.reducer,
    datasetFamily: datasetFamily.reducer,
    dataset: dataset.reducer,
    attribute: attribute.reducer,
    criteriaFamily: criteriaFamily.reducer,
    outputFamily: outputFamily.reducer,
    outputCategory: outputCategory.reducer,
    group: group.reducer,
    fileExplorer: fileExplorer.reducer
};

const productionReducer = combineReducers(reducers);

export function reducer(state: any, action: any) {
    return productionReducer(state, action);
}

export const getMetamodelState = createFeatureSelector<State>('metamodel');
