import * as actions from '../action/output-category.action';

import { sortDisplay } from '../../../shared/utils';
import { OutputCategory } from '../model';

export interface State {
    outputCategoryListIsLoading: boolean;
    outputCategoryListIsLoaded: boolean;
    outputCategoryList: OutputCategory[];
}

const initialState: State = {
    outputCategoryListIsLoading: false,
    outputCategoryListIsLoaded: false,
    outputCategoryList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_OUTPUT_CATEGORY_LIST:
            return {
                ...state,
                outputCategoryListIsLoading: true,
                outputCategoryListIsLoaded: false,
                outputCategoryList: []
            }

        case actions.LOAD_OUTPUT_CATEGORY_LIST_SUCCESS:
            const outputCategoryList = action.payload as OutputCategory[];

            return {
                ...state,
                outputCategoryList,
                outputCategoryListIsLoading: false,
                outputCategoryListIsLoaded: true
            };

        case actions.LOAD_OUTPUT_CATEGORY_LIST_FAIL:
            return {
                ...state,
                outputCategoryListIsLoading: false
            };

        case actions.ADD_NEW_OUTPUT_CATEGORY_SUCCESS:
            const newOutputCategory = action.payload as OutputCategory;

            return {
                ...state,
                outputCategoryList: [...state.outputCategoryList, newOutputCategory].sort(sortDisplay)
            };

        case actions.EDIT_OUTPUT_CATEGORY_SUCCESS:
            const editedOutputCategory = action.payload as OutputCategory;

            return {
                ...state,
                outputCategoryList: [
                    ...state.outputCategoryList.filter(datasetfamily => datasetfamily.id !== editedOutputCategory.id),
                    editedOutputCategory
                ].sort(sortDisplay)
            };

        case actions.DELETE_OUTPUT_CATEGORY_SUCCESS:
            const deletedOutputCategory = action.payload as OutputCategory;

            return {
                ...state,
                outputCategoryList: state.outputCategoryList.filter(datasetfamily => datasetfamily.id !== deletedOutputCategory.id)
            }

        default:
            return state;
    }
}

export const getOutputCategoryListIsLoading = (state: State) => state.outputCategoryListIsLoading;
export const getOutputCategoryListIsLoaded = (state: State) => state.outputCategoryListIsLoaded;
export const getOutputCategoryList = (state: State) => state.outputCategoryList;
