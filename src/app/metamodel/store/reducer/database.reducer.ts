import * as actions from '../action/database.action';

import { Database, Column } from '../model';

export interface State {
    databaseListIsLoading: boolean;
    databaseListIsLoaded: boolean;
    databaseList: Database[];
    tableList: string[];
    tableListIsLoading: boolean;
    tableListIsLoaded: boolean;
    columnList: Column[];
    columnListIsLoading: boolean;
    columnListIsLoaded: boolean;
}

const initialState: State = {
    databaseListIsLoading: false,
    databaseListIsLoaded: false,
    databaseList: [],
    tableList: [],
    tableListIsLoading: false,
    tableListIsLoaded: false,
    columnList: [],
    columnListIsLoading: false,
    columnListIsLoaded: false
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_DATABASE_LIST_WIP:
            return {
                ...state,
                databaseListIsLoading: true
            };

        case actions.LOAD_DATABASE_LIST_SUCCESS:
            const databaseList = action.payload as Database[];

            return {
                ...state,
                databaseList,
                databaseListIsLoading: false,
                databaseListIsLoaded: true
            };

        case actions.LOAD_DATABASE_LIST_FAIL:
            return {
                ...state,
                databaseListIsLoading: false
            };

        case actions.LOAD_TABLE_LIST:
            return {
                ...state,
                tableList: [],
                tableListIsLoading: true,
                tableListIsLoaded: false
            };

        case actions.LOAD_TABLE_LIST_SUCCESS:
            const tableList = action.payload as string[];

            return {
                ...state,
                tableList,
                tableListIsLoading: false,
                tableListIsLoaded: true
            };

        case actions.LOAD_TABLE_LIST_FAIL:
            return {
                ...state,
                tableListIsLoading: false
            };

        case actions.LOAD_COLUMN_LIST:
            return {
                ...state,
                columnList: [],
                columnListIsLoading: true,
                columnListIsLoaded: false
            };
    
        case actions.LOAD_COLUMN_LIST_SUCCESS:
            const columnList = action.payload as Column[];

            return {
                ...state,
                columnList,
                columnListIsLoading: false,
                columnListIsLoaded: true
            };
    
        case actions.LOAD_COLUMN_LIST_FAIL:
            return {
                ...state,
                columnListIsLoading: false
            };

        case actions.ADD_NEW_DATABASE_SUCCESS:
            const newDatabase = action.payload as Database;

            return {
                ...state,
                databaseList: [...state.databaseList, newDatabase]
            };

        case actions.EDIT_DATABASE_SUCCESS:
            const editedDatabase = action.payload as Database;

            return {
                ...state,
                databaseList: [...state.databaseList.filter(database => database.id !== editedDatabase.id), editedDatabase]
            };    

        case actions.DELETE_DATABASE_SUCCESS:
            const deletedDatabase = action.payload as Database;

            return {
                ...state,
                databaseList: state.databaseList.filter(database => database.id !== deletedDatabase.id)
            }

        default:
            return state;
    }
}

export const getDatabaseList = (state: State) => state.databaseList;
export const getDatabaseListIsLoading = (state: State) => state.databaseListIsLoading;
export const getDatabaseListIsLoaded = (state: State) => state.databaseListIsLoaded;
export const getTableList = (state: State) => state.tableList;
export const getTableListIsLoading = (state: State) => state.tableListIsLoading;
export const getTableListIsLoaded = (state: State) => state.tableListIsLoaded;
export const getColumnList = (state: State) => state.columnList;
export const getColumnListIsLoading = (state: State) => state.columnListIsLoading;
export const getColumnListIsLoaded = (state: State) => state.columnListIsLoaded;