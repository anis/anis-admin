import * as actions from '../action/instance.action';

import { Instance } from '../model';

export interface State {
    instanceListIsLoading: boolean;
    instanceListIsLoaded: boolean;
    instanceList: Instance[];
}

const initialState: State = {
    instanceListIsLoading: false,
    instanceListIsLoaded: false,
    instanceList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_INSTANCE_LIST_WIP:
            return {
                ...state,
                instanceListIsLoading: true
            };

        case actions.LOAD_INSTANCE_LIST_SUCCESS:
            const instanceList = action.payload as Instance[];

            return {
                ...state,
                instanceList,
                instanceListIsLoading: false,
                instanceListIsLoaded: true
            };

        case actions.LOAD_INSTANCE_LIST_FAIL:
            return {
                ...state,
                instanceListIsLoading: false
            };

        case actions.ADD_NEW_INSTANCE_SUCCESS:
            const newInstance = action.payload as Instance;

            return {
                ...state,
                instanceList: [...state.instanceList, newInstance]
            };

        case actions.EDIT_INSTANCE_SUCCESS:
            const editedInstance = action.payload as Instance;

            return {
                ...state,
                instanceList: [...state.instanceList.filter(instance => instance.name !== editedInstance.name), editedInstance]
            };    

        case actions.DELETE_INSTANCE_SUCCESS:
            const deletedInstance = action.payload as Instance;

            return {
                ...state,
                instanceList: state.instanceList.filter(instance => instance.name !== deletedInstance.name)
            };

        default:
            return state;
    }
}

export const getInstanceListIsLoading = (state: State) => state.instanceListIsLoading;
export const getInstanceListIsLoaded = (state: State) => state.instanceListIsLoaded;
export const getInstanceList = (state: State) => state.instanceList;
