import * as actions from '../action/attribute.action';

import { Attribute } from '../model';
import { sortAttributeListByOutputDisplay } from '../service/attribute.service';

export interface State {
    attributeListIsLoading: boolean;
    attributeListIsLoaded: boolean;
    attributeList: Attribute[];
    optionListGeneratedIsLoading: boolean;
    optionListGeneratedIsLoaded: boolean;
    optionListGenerated: string[];
}

const initialState: State = {
    attributeListIsLoading: false,
    attributeListIsLoaded: false,
    attributeList: [],
    optionListGeneratedIsLoading: false,
    optionListGeneratedIsLoaded: false,
    optionListGenerated: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_ATTRIBUTE_LIST:
            return {
                ...state,
                attributeList: [],
                attributeListIsLoading: true,
                attributeListIsLoaded: false
            }

        case actions.LOAD_ATTRIBUTE_LIST_SUCCESS:
            const attributeList = action.payload as Attribute[];

            return {
                ...state,
                attributeList,
                attributeListIsLoading: false,
                attributeListIsLoaded: true
            };

        case actions.LOAD_ATTRIBUTE_LIST_FAIL:
            return {
                ...state,
                attributeListIsLoading: false
            }

        case actions.GENERATE_OPTION_LIST:
            return {
                ...state,
                optionListGenerated: [],
                optionListGeneratedIsLoading: true,
                optionListGeneratedIsLoaded: false
            }

        case actions.GENERATE_OPTION_LIST_SUCCESS:
            const optionListGenerated = action.payload as string[];

            return {
                ...state,
                optionListGenerated,
                optionListGeneratedIsLoading: false,
                optionListGeneratedIsLoaded: true
            }

        case actions.GENERATE_OPTION_LIST_FAIL:
            return {
                ...state,
                optionListGeneratedIsLoading: false
            }

        case actions.ADD_NEW_ATTRIBUTE_SUCCESS:
            const addedAttribute = action.payload as Attribute;

            return {
                ...state,
                attributeList: [
                    ...state.attributeList,
                    addedAttribute
                ]
            };

        case actions.EDIT_ATTRIBUTE_SUCCESS:
            const editedAttribute = action.payload as Attribute;

            return {
                ...state,
                attributeList: [
                    ...state.attributeList.filter(attribute => attribute.id !== editedAttribute.id),
                    editedAttribute
                ].sort(sortAttributeListByOutputDisplay)
            };

        case actions.DELETE_ATTRIBUTE_SUCCESS:
            const deletedAttribute = action.payload as Attribute;

            return {
                ...state,
                attributeList: state.attributeList.filter(attribute => attribute.id !== deletedAttribute.id)
            };

        default:
            return state;
    }
}

export const getAttributeListIsLoading = (state: State) => state.attributeListIsLoading;
export const getAttributeListIsLoaded = (state: State) => state.attributeListIsLoaded;
export const getAttributeList = (state: State) => state.attributeList;
export const getOptionListGeneratedIsLoading = (state: State) => state.optionListGeneratedIsLoading;
export const getOptionListGeneratedIsLoaded = (state: State) => state.optionListGeneratedIsLoaded;
export const getOptionListGenerated = (state: State) => state.optionListGenerated;
