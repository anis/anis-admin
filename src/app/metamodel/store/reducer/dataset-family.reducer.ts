import * as actions from '../action/dataset-family.action';

import { sortDisplay } from '../../../shared/utils';
import { DatasetFamily } from '../model';

export interface State {
    datasetFamilyListIsLoading: boolean;
    datasetFamilyListIsLoaded: boolean;
    datasetFamilyList: DatasetFamily[];
}

const initialState: State = {
    datasetFamilyListIsLoading: false,
    datasetFamilyListIsLoaded: false,
    datasetFamilyList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_DATASET_FAMILY_LIST:
            return {
                ...state,
                datasetFamilyListIsLoading: true,
                datasetFamilyListIsLoaded: false,
                datasetFamilyList: []
            }

        case actions.LOAD_DATASET_FAMILY_LIST_SUCCESS:
            const datasetFamilyList = action.payload as DatasetFamily[];

            return {
                ...state,
                datasetFamilyList,
                datasetFamilyListIsLoading: false,
                datasetFamilyListIsLoaded: true
            };

        case actions.LOAD_DATASET_FAMILY_LIST_FAIL:
            return {
                ...state,
                datasetFamilyListIsLoading: false
            };

        case actions.ADD_NEW_DATASET_FAMILY_SUCCESS:
            const newDatasetFamily = action.payload as DatasetFamily;

            return {
                ...state,
                datasetFamilyList: [...state.datasetFamilyList, newDatasetFamily].sort(sortDisplay)
            };

        case actions.EDIT_DATASET_FAMILY_SUCCESS:
            const editedDatasetFamily = action.payload as DatasetFamily;

            return {
                ...state,
                datasetFamilyList: [
                    ...state.datasetFamilyList.filter(datasetfamily => datasetfamily.id !== editedDatasetFamily.id),
                    editedDatasetFamily
                ].sort(sortDisplay)
            };

        case actions.DELETE_DATASET_FAMILY_SUCCESS:
            const deletedDatasetFamily = action.payload as DatasetFamily;

            return {
                ...state,
                datasetFamilyList: state.datasetFamilyList.filter(datasetfamily => datasetfamily.id !== deletedDatasetFamily.id)
            }

        default:
            return state;
    }
}

export const getDatasetFamilyListIsLoading = (state: State) => state.datasetFamilyListIsLoading;
export const getDatasetFamilyListIsLoaded = (state: State) => state.datasetFamilyListIsLoaded;
export const getDatasetFamilyList = (state: State) => state.datasetFamilyList;
