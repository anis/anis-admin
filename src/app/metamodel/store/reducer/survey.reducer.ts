import * as actions from '../action/survey.action';

import { Survey } from '../model';

export interface State {
    surveyListIsLoading: boolean;
    surveyListIsLoaded: boolean;
    surveyList: Survey[];
}

const initialState: State = {
    surveyListIsLoading: false,
    surveyListIsLoaded: false,
    surveyList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_SURVEY_LIST_WIP:
            return {
                ...state,
                surveyListIsLoading: true
            };

        case actions.LOAD_SURVEY_LIST_SUCCESS:
            const surveyList = action.payload as Survey[];

            return {
                ...state,
                surveyList,
                surveyListIsLoading: false,
                surveyListIsLoaded: true
            };

        case actions.LOAD_SURVEY_LIST_FAIL:
            return {
                ...state,
                surveyListIsLoading: false
            };

        case actions.ADD_NEW_SURVEY_SUCCESS:
            const newSurvey = action.payload as Survey;

            return {
                ...state,
                surveyList: [...state.surveyList, newSurvey]
            };

        case actions.EDIT_SURVEY_SUCCESS:
            const editedSurvey = action.payload as Survey;

            return {
                ...state,
                surveyList: [...state.surveyList.filter(survey => survey.name !== editedSurvey.name), editedSurvey]
            };    

        case actions.DELETE_SURVEY_SUCCESS:
            const deletedSurvey = action.payload as Survey;

            return {
                ...state,
                surveyList: state.surveyList.filter(survey => survey.name !== deletedSurvey.name)
            };

        case actions.INCREMENT_NB_DATASETS:
            return {
                ...state,
                surveyList: state.surveyList.map(p => {
                    if (p.name !== action.payload) {
                        return p;
                    }

                    return {
                        ...p,
                        nb_datasets: p.nb_datasets + 1
                    };
                })
            };

        case actions.DECREMENT_NB_DATASETS:
            return {
                ...state,
                surveyList: state.surveyList.map(p => {
                    if (p.name !== action.payload) {
                        return p;
                    }

                    return {
                        ...p,
                        nb_datasets: p.nb_datasets - 1
                    };
                })
            };

        default:
            return state;
    }
}

export const getSurveyListIsLoading = (state: State) => state.surveyListIsLoading;
export const getSurveyListIsLoaded = (state: State) => state.surveyListIsLoaded;
export const getSurveyList = (state: State) => state.surveyList;
