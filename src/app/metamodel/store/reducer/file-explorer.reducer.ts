import * as actions from '../action/file-explorer.action';

import { FileInfo, ImageLimit } from '../model';

export interface State {
    rootDirectoryInfoIsLoading: boolean;
    rootDirectoryInfoIsLoaded: boolean;
    rootDirectoryInfo: FileInfo[];
    datasetDirectoryInfoIsLoading: boolean;
    datasetDirectoryInfoIsLoaded: boolean;
    datasetDirectoryInfo: FileInfo[];
    imageLimitIsLoading: boolean;
    imageLimitIsLoaded: boolean;
    imageLimit: ImageLimit;
}

const initialState: State = {
    rootDirectoryInfoIsLoading: false,
    rootDirectoryInfoIsLoaded: false,
    rootDirectoryInfo: [],
    datasetDirectoryInfoIsLoading: false,
    datasetDirectoryInfoIsLoaded: false,
    datasetDirectoryInfo: [],
    imageLimitIsLoading: false,
    imageLimitIsLoaded: false,
    imageLimit: null
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_ROOT_DIRECTORY_INFO:
            return {
                ...state,
                rootDirectoryInfoIsLoading: true,
                rootDirectoryInfoIsLoaded: false,
                rootDirectoryInfo: []
            };

        case actions.LOAD_ROOT_DIRECTORY_INFO_SUCCESS:
            return {
                ...state,
                rootDirectoryInfoIsLoading: false,
                rootDirectoryInfoIsLoaded: true,
                rootDirectoryInfo: action.payload
            };

        case actions.LOAD_ROOT_DIRECTORY_INFO_FAIL:
            return {
                ...state,
                rootDirectoryInfoIsLoading: false
            };

        case actions.LOAD_DATASET_DIRECTORY_INFO:
            return {
                ...state,
                datasetDirectoryInfoIsLoading: true,
                datasetDirectoryInfoIsLoaded: false,
                datasetDirectoryInfo: []
            };

        case actions.LOAD_DATASET_DIRECTORY_INFO_SUCCESS:
            return {
                ...state,
                datasetDirectoryInfoIsLoading: false,
                datasetDirectoryInfoIsLoaded: true,
                datasetDirectoryInfo: action.payload
            };

        case actions.LOAD_DATASET_DIRECTORY_INFO_FAIL:
            return {
                ...state,
                datasetDirectoryInfoIsLoading: false
            };

        case actions.LOAD_IMAGE_LIMIT:
            return {
                ...state,
                imageLimitIsLoading: true,
                imageLimitIsLoaded: false,
                imageLimit: null
            };

        case actions.LOAD_IMAGE_LIMIT_SUCCESS:
            return {
                ...state,
                imageLimitIsLoading: false,
                imageLimitIsLoaded: true,
                imageLimit: action.payload
            };

        case actions.LOAD_IMAGE_LIMIT_FAIL:
            return {
                ...state,
                imageLimitIsLoading: false
            };

        default:
            return state;
    }
}

export const getRootDirectoryInfoIsLoading = (state: State) => state.rootDirectoryInfoIsLoading;
export const getRootDirectoryInfoIsLoaded = (state: State) => state.rootDirectoryInfoIsLoaded;
export const getRootDirectoryInfo = (state: State) => state.rootDirectoryInfo;
export const getDatasetDirectoryInfoIsLoading = (state: State) => state.datasetDirectoryInfoIsLoading;
export const getDatasetDirectoryInfoIsLoaded = (state: State) => state.datasetDirectoryInfoIsLoaded;
export const getDatasetDirectoryInfo = (state: State) => state.datasetDirectoryInfo;
export const getImageLimitIsLoading = (state: State) => state.imageLimitIsLoading;
export const getImageLimitIsLoaded = (state: State) => state.imageLimitIsLoaded;
export const getImageLimit = (state: State) => state.imageLimit;
