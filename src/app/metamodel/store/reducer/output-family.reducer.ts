import * as actions from '../action/output-family.action';

import { sortDisplay } from '../../../shared/utils';
import { OutputFamily } from '../model';

export interface State {
    outputFamilyListIsLoading: boolean;
    outputFamilyListIsLoaded: boolean;
    outputFamilyList: OutputFamily[];
}

const initialState: State = {
    outputFamilyListIsLoading: false,
    outputFamilyListIsLoaded: false,
    outputFamilyList: []
};

export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_OUTPUT_FAMILY_LIST:
            return {
                ...state,
                outputFamilyListIsLoading: true,
                outputFamilyListIsLoaded: false,
                outputFamilyList: []
            }

        case actions.LOAD_OUTPUT_FAMILY_LIST_SUCCESS:
            const outputFamilyList = action.payload as OutputFamily[];

            return {
                ...state,
                outputFamilyList,
                outputFamilyListIsLoading: false,
                outputFamilyListIsLoaded: true
            };

        case actions.LOAD_OUTPUT_FAMILY_LIST_FAIL:
            return {
                ...state,
                outputFamilyListIsLoading: false
            };

        case actions.ADD_NEW_OUTPUT_FAMILY_SUCCESS:
            const newOutputFamily = action.payload as OutputFamily;

            return {
                ...state,
                outputFamilyList: [...state.outputFamilyList, newOutputFamily].sort(sortDisplay)
            };

        case actions.EDIT_OUTPUT_FAMILY_SUCCESS:
            const editedOutputFamily = action.payload as OutputFamily;

            return {
                ...state,
                outputFamilyList: [
                    ...state.outputFamilyList.filter(datasetfamily => datasetfamily.id !== editedOutputFamily.id),
                    editedOutputFamily
                ].sort(sortDisplay)
            };

        case actions.DELETE_OUTPUT_FAMILY_SUCCESS:
            const deletedOutputFamily = action.payload as OutputFamily;

            return {
                ...state,
                outputFamilyList: state.outputFamilyList.filter(datasetfamily => datasetfamily.id !== deletedOutputFamily.id)
            }

        default:
            return state;
    }
}

export const getOutputFamilyListIsLoading = (state: State) => state.outputFamilyListIsLoading;
export const getOutputFamilyListIsLoaded = (state: State) => state.outputFamilyListIsLoaded;
export const getOutputFamilyList = (state: State) => state.outputFamilyList;
