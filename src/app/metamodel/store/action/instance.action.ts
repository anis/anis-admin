import { Action } from '@ngrx/store';

import { Instance } from '../model';

export const LOAD_INSTANCE_LIST = '[Instance] Load Instance List';
export const LOAD_INSTANCE_LIST_WIP = '[Instance] Load Instance List WIP';
export const LOAD_INSTANCE_LIST_SUCCESS = '[Instance] Load Instance List Success';
export const LOAD_INSTANCE_LIST_FAIL = '[Instance] Load Instance List Fail';
export const ADD_NEW_INSTANCE = '[Instance] Add New Instance';
export const ADD_NEW_INSTANCE_SUCCESS = '[Instance] Add New Instance Success';
export const ADD_NEW_INSTANCE_FAIL = '[Instance] Add New Instance Fail';
export const EDIT_INSTANCE = '[Instance] Edit Instance';
export const EDIT_INSTANCE_SUCCESS = '[Instance] Edit Instance Success';
export const EDIT_INSTANCE_FAIL = '[Instance] Edit Instance Fail';
export const DELETE_INSTANCE = '[Instance] Delete Instance';
export const DELETE_INSTANCE_SUCCESS = '[Instance] Delete Instance Success';
export const DELETE_INSTANCE_FAIL = '[Instance] Delete Instance Fail';

export class LoadInstanceListAction implements Action {
    type = LOAD_INSTANCE_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadInstanceListWipAction implements Action {
    type = LOAD_INSTANCE_LIST_WIP;

    constructor(public payload: {} = null) { }
}

export class LoadInstanceListSuccessAction implements Action {
    type = LOAD_INSTANCE_LIST_SUCCESS;

    constructor(public payload: Instance[]) { }
}

export class LoadInstanceListFailAction implements Action {
    type = LOAD_INSTANCE_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewInstanceAction implements Action {
    type = ADD_NEW_INSTANCE;

    constructor(public payload: Instance) { }
}

export class AddNewInstanceSuccessAction implements Action {
    type = ADD_NEW_INSTANCE_SUCCESS;

    constructor(public payload: Instance) { }
}

export class AddNewInstanceFailAction implements Action {
    type = ADD_NEW_INSTANCE_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditInstanceAction implements Action {
    type = EDIT_INSTANCE;

    constructor(public payload: Instance) { }
}

export class EditInstanceSuccessAction implements Action {
    type = EDIT_INSTANCE_SUCCESS;

    constructor(public payload: Instance) { }
}

export class EditInstanceFailAction implements Action {
    type = EDIT_INSTANCE_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteInstanceAction implements Action {
    type = DELETE_INSTANCE;

    constructor(public payload: Instance) { }
}

export class DeleteInstanceSuccessAction implements Action {
    type = DELETE_INSTANCE_SUCCESS;

    constructor(public payload: Instance) { }
}

export class DeleteInstanceFailAction implements Action {
    type = DELETE_INSTANCE_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadInstanceListAction
    | LoadInstanceListWipAction
    | LoadInstanceListSuccessAction
    | LoadInstanceListFailAction
    | AddNewInstanceAction
    | AddNewInstanceSuccessAction
    | AddNewInstanceFailAction
    | EditInstanceAction
    | EditInstanceSuccessAction
    | EditInstanceFailAction
    | DeleteInstanceAction
    | DeleteInstanceSuccessAction
    | DeleteInstanceFailAction;
