import { Action } from '@ngrx/store';

import { Group } from '../model';

export const LOAD_GROUP_LIST = '[Group] Load Group List';
export const LOAD_GROUP_LIST_SUCCESS = '[Group] Load Group List Success';
export const LOAD_GROUP_LIST_FAIL = '[Group] Load Group List Fail';
export const ADD_NEW_GROUP = '[Group] Add New Group';
export const ADD_NEW_GROUP_SUCCESS = '[Group] Add New Group Success';
export const ADD_NEW_GROUP_FAIL = '[Group] Add New Group Fail';
export const EDIT_GROUP = '[Group] Edit Group';
export const EDIT_GROUP_SUCCESS = '[Group] Edit Group Success';
export const EDIT_GROUP_FAIL = '[Group] Edit Group Fail';
export const DELETE_GROUP = '[Group] Delete Group';
export const DELETE_GROUP_SUCCESS = '[Group] Delete Group Success';
export const DELETE_GROUP_FAIL = '[Group] Delete Group Fail';

export class LoadGroupListAction implements Action {
    type = LOAD_GROUP_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadGroupListSuccessAction implements Action {
    type = LOAD_GROUP_LIST_SUCCESS;

    constructor(public payload: Group[]) { }
}

export class LoadGroupListFailAction implements Action {
    type = LOAD_GROUP_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewGroupAction implements Action {
    type = ADD_NEW_GROUP;

    constructor(public payload: Group) { }
}

export class AddNewGroupSuccessAction implements Action {
    type = ADD_NEW_GROUP_SUCCESS;

    constructor(public payload: Group) { }
}

export class AddNewGroupFailAction implements Action {
    type = ADD_NEW_GROUP_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditGroupAction implements Action {
    type = EDIT_GROUP;

    constructor(public payload: Group) { }
}

export class EditGroupSuccessAction implements Action {
    type = EDIT_GROUP_SUCCESS;

    constructor(public payload: Group) { }
}

export class EditGroupFailAction implements Action {
    type = EDIT_GROUP_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteGroupAction implements Action {
    type = DELETE_GROUP;

    constructor(public payload: Group) { }
}

export class DeleteGroupSuccessAction implements Action {
    type = DELETE_GROUP_SUCCESS;

    constructor(public payload: Group) { }
}

export class DeleteGroupFailAction implements Action {
    type = DELETE_GROUP_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadGroupListAction
    | LoadGroupListSuccessAction
    | LoadGroupListFailAction
    | AddNewGroupAction
    | AddNewGroupSuccessAction
    | AddNewGroupFailAction
    | EditGroupAction
    | EditGroupSuccessAction
    | EditGroupFailAction
    | DeleteGroupAction
    | DeleteGroupSuccessAction
    | DeleteGroupFailAction;
