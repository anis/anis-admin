import { Action } from '@ngrx/store';

import { Dataset } from '../model';

export const LOAD_DATASET_LIST = '[Dataset] Load Dataset List';
export const LOAD_DATASET_LIST_SUCCESS = '[Dataset] Load Dataset List Success';
export const LOAD_DATASET_LIST_FAIL = '[Dataset] Load Dataset List Fail';
export const ADD_NEW_DATASET = '[Dataset] Add New Dataset';
export const ADD_NEW_DATASET_SUCCESS = '[Dataset] Add New Dataset Success';
export const ADD_NEW_DATASET_FAIL = '[Dataset] Add New Dataset Fail';
export const EDIT_DATASET = '[Dataset] Edit Dataset';
export const EDIT_DATASET_SUCCESS = '[Dataset] Edit Dataset Success';
export const EDIT_DATASET_FAIL = '[Dataset] Edit Dataset Fail';
export const DELETE_DATASET = '[Dataset] Delete Dataset';
export const DELETE_DATASET_SUCCESS = '[Dataset] Delete Dataset Success';
export const DELETE_DATASET_FAIL = '[Dataset] Delete Dataset Fail';

export class LoadDatasetListAction implements Action {
    type = LOAD_DATASET_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadDatasetListSuccessAction implements Action {
    type = LOAD_DATASET_LIST_SUCCESS;

    constructor(public payload: Dataset[]) { }
}

export class LoadDatasetListFailAction implements Action {
    type = LOAD_DATASET_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewDatasetAction implements Action {
    type = ADD_NEW_DATASET;

    constructor(public payload: Dataset) { }
}

export class AddNewDatasetSuccessAction implements Action {
    type = ADD_NEW_DATASET_SUCCESS;

    constructor(public payload: Dataset) { }
}

export class AddNewDatasetFailAction implements Action {
    type = ADD_NEW_DATASET_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditDatasetAction implements Action {
    type = EDIT_DATASET;

    constructor(public payload: Dataset) { }
}

export class EditDatasetSuccessAction implements Action {
    type = EDIT_DATASET_SUCCESS;

    constructor(public payload: Dataset) { }
}

export class EditDatasetFailAction implements Action {
    type = EDIT_DATASET_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteDatasetAction implements Action {
    type = DELETE_DATASET;

    constructor(public payload: Dataset) { }
}

export class DeleteDatasetSuccessAction implements Action {
    type = DELETE_DATASET_SUCCESS;

    constructor(public payload: Dataset) { }
}

export class DeleteDatasetFailAction implements Action {
    type = DELETE_DATASET_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadDatasetListAction
    | LoadDatasetListSuccessAction
    | LoadDatasetListFailAction
    | AddNewDatasetAction
    | AddNewDatasetSuccessAction
    | AddNewDatasetFailAction
    | EditDatasetAction
    | EditDatasetSuccessAction
    | EditDatasetFailAction
    | DeleteDatasetAction
    | DeleteDatasetSuccessAction
    | DeleteDatasetFailAction;
