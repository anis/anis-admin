import { Action } from '@ngrx/store';

import { Database, Column } from '../model';

export const LOAD_DATABASE_LIST = '[Database] Load Database List';
export const LOAD_DATABASE_LIST_WIP = '[Database] Load Database List WIP';
export const LOAD_DATABASE_LIST_SUCCESS = '[Database] Load Database List Success';
export const LOAD_DATABASE_LIST_FAIL = '[Database] Load Database List Fail';
export const LOAD_TABLE_LIST = '[Database] Load Table List';
export const LOAD_TABLE_LIST_SUCCESS = '[Database] Load Table List Success';
export const LOAD_TABLE_LIST_FAIL = '[Database] Load Table List Fail';
export const LOAD_COLUMN_LIST = '[Database] Load Column List';
export const LOAD_COLUMN_LIST_SUCCESS = '[Database] Load Column List Success';
export const LOAD_COLUMN_LIST_FAIL = '[Database] Load Column List Fail';
export const ADD_NEW_DATABASE = '[Database] Add New Database';
export const ADD_NEW_DATABASE_SUCCESS = '[Database] Add New Database Success';
export const ADD_NEW_DATABASE_FAIL = '[Database] Add New Database Fail';
export const EDIT_DATABASE = '[Database] Edit Database';
export const EDIT_DATABASE_SUCCESS = '[Database] Edit Database Success';
export const EDIT_DATABASE_FAIL = '[Database] Edit Database Fail';
export const DELETE_DATABASE = '[Database] Delete Database';
export const DELETE_DATABASE_SUCCESS = '[Database] Delete Database Success';
export const DELETE_DATABASE_FAIL = '[Database] Delete Database Fail';

export class LoadDatabaseListAction implements Action {
    type = LOAD_DATABASE_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadDatabaseListWipAction implements Action {
    type = LOAD_DATABASE_LIST_WIP;

    constructor(public payload: {} = null) { }
}

export class LoadDatabaseListSuccessAction implements Action {
    type = LOAD_DATABASE_LIST_SUCCESS;

    constructor(public payload: Database[]) { }
}

export class LoadDatabaseListFailAction implements Action {
    type = LOAD_DATABASE_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class LoadTableListAction implements Action {
    type = LOAD_TABLE_LIST;

    constructor(public payload: number) { }
}

export class LoadTableListSuccessAction implements Action {
    type = LOAD_TABLE_LIST_SUCCESS;

    constructor(public payload: string[]) { }
}

export class LoadTableListFailAction implements Action {
    type = LOAD_TABLE_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class LoadColumnListAction implements Action {
    type = LOAD_COLUMN_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadColumnListSuccessAction implements Action {
    type = LOAD_COLUMN_LIST_SUCCESS;

    constructor(public payload: Column[]) { }
}

export class LoadColumnListFailAction implements Action {
    type = LOAD_COLUMN_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewDatabaseAction implements Action {
    type = ADD_NEW_DATABASE;

    constructor(public payload: Database) { }
}

export class AddNewDatabaseSuccessAction implements Action {
    type = ADD_NEW_DATABASE_SUCCESS;

    constructor(public payload: Database) { }
}

export class AddNewDatabaseFailAction implements Action {
    type = ADD_NEW_DATABASE_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditDatabaseAction implements Action {
    type = EDIT_DATABASE;

    constructor(public payload: Database) { }
}

export class EditDatabaseSuccessAction implements Action {
    type = EDIT_DATABASE_SUCCESS;

    constructor(public payload: Database) { }
}

export class EditDatabaseFailAction implements Action {
    type = EDIT_DATABASE_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteDatabaseAction implements Action {
    type = DELETE_DATABASE;

    constructor(public payload: Database) { }
}

export class DeleteDatabaseSuccessAction implements Action {
    type = DELETE_DATABASE_SUCCESS;

    constructor(public payload: Database) { }
}

export class DeleteDatabaseFailAction implements Action {
    type = DELETE_DATABASE_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadDatabaseListAction
    | LoadDatabaseListWipAction
    | LoadDatabaseListSuccessAction
    | LoadDatabaseListFailAction
    | LoadTableListAction
    | LoadTableListSuccessAction
    | LoadTableListFailAction
    | LoadColumnListAction
    | LoadColumnListSuccessAction
    | LoadColumnListFailAction
    | AddNewDatabaseAction
    | AddNewDatabaseSuccessAction
    | AddNewDatabaseFailAction
    | EditDatabaseAction
    | EditDatabaseSuccessAction
    | EditDatabaseFailAction
    | DeleteDatabaseAction
    | DeleteDatabaseSuccessAction
    | DeleteDatabaseFailAction;
