import { Action } from '@ngrx/store';

import { Attribute } from '../model';

export const LOAD_ATTRIBUTE_LIST = '[Attribute] Load Attribute List';
export const LOAD_ATTRIBUTE_LIST_SUCCESS = '[Attribute] Load Attribute List Sucess';
export const LOAD_ATTRIBUTE_LIST_FAIL = '[Attribute] Load Attribute List Fail';
export const ADD_NEW_ATTRIBUTE = '[Attribute] Add New Attribute';
export const ADD_NEW_ATTRIBUTE_SUCCESS = '[Attribute] Add New Attribute Success';
export const ADD_NEW_ATTRIBUTE_FAIL = '[Attribute] Add New Attribute Fail';
export const EDIT_ATTRIBUTE = '[Attribute] Edit Attribute';
export const EDIT_ATTRIBUTE_SUCCESS = '[Attribute] Edit Attribute Success';
export const EDIT_ATTRIBUTE_FAIL = '[Attribute] Edit Attribute Fail';
export const DELETE_ATTRIBUTE = '[Attribute] Delete Attribute';
export const DELETE_ATTRIBUTE_SUCCESS = '[Attribute] Delete Attribute Success';
export const DELETE_ATTRIBUTE_FAIL = '[Attribute] Delete Attribute Fail';
export const GENERATE_OPTION_LIST = '[Attribute] Generate Option List';
export const GENERATE_OPTION_LIST_SUCCESS = '[Attribute] Generate Option List Success';
export const GENERATE_OPTION_LIST_FAIL = '[Attribute] Generate Option List Fail';

export class LoadAttributeListAction implements Action {
    type = LOAD_ATTRIBUTE_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadAttributeListSuccessAction implements Action {
    type = LOAD_ATTRIBUTE_LIST_SUCCESS;

    constructor(public payload: Attribute[]) { }
}

export class LoadAttributeListFailAction implements Action {
    type = LOAD_ATTRIBUTE_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewAttributeAction implements Action {
    type = ADD_NEW_ATTRIBUTE;

    constructor(public payload: Attribute) { }
}

export class AddNewAttributeSuccessAction implements Action {
    type = ADD_NEW_ATTRIBUTE_SUCCESS;

    constructor(public payload: Attribute) { }
}

export class AddNewAttributeFailAction implements Action {
    type = ADD_NEW_ATTRIBUTE_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditAttributeAction implements Action {
    type = EDIT_ATTRIBUTE;

    constructor(public payload: Attribute) { }
}

export class EditAttributeSuccessAction implements Action {
    type = EDIT_ATTRIBUTE_SUCCESS;

    constructor(public payload: Attribute) { }
}

export class EditAttributeFailAction implements Action {
    type = EDIT_ATTRIBUTE_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteAttributeAction implements Action {
    type = DELETE_ATTRIBUTE;

    constructor(public payload: Attribute) { }
}

export class DeleteAttributeSuccessAction implements Action {
    type = DELETE_ATTRIBUTE_SUCCESS;

    constructor(public payload: Attribute) { }
}

export class DeleteAttributeFailAction implements Action {
    type = DELETE_ATTRIBUTE_FAIL;

    constructor(public payload: {} = null) { }
}

export class GenerateOptionListAction implements Action {
    type = GENERATE_OPTION_LIST;

    constructor(public payload: Attribute) { }
}

export class GenerateOptionListSuccessAction implements Action {
    type = GENERATE_OPTION_LIST_SUCCESS;

    constructor(public payload: string[]) { }
}

export class GenerateOptionListFailAction implements Action {
    type = GENERATE_OPTION_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadAttributeListAction
    | LoadAttributeListSuccessAction
    | LoadAttributeListFailAction
    | AddNewAttributeAction
    | AddNewAttributeSuccessAction
    | AddNewAttributeFailAction
    | EditAttributeAction
    | EditAttributeSuccessAction
    | EditAttributeFailAction
    | DeleteAttributeAction
    | DeleteAttributeSuccessAction
    | DeleteAttributeFailAction
    | GenerateOptionListAction
    | GenerateOptionListSuccessAction
    | GenerateOptionListFailAction;
