import { Action } from '@ngrx/store';

import { FileInfo, ImageLimit } from '../model';

export const LOAD_ROOT_DIRECTORY_INFO = '[FileExplorer] Load Root Directory Info';
export const LOAD_ROOT_DIRECTORY_INFO_SUCCESS = '[FileExplorer] Load Root Directory Info Success';
export const LOAD_ROOT_DIRECTORY_INFO_FAIL = '[FileExplorer] Load Root Directory Info Fail';
export const LOAD_DATASET_DIRECTORY_INFO = '[FileExplorer] Load Dataset Directory Info';
export const LOAD_DATASET_DIRECTORY_INFO_SUCCESS = '[FileExplorer] Load Dataset Directory Info Success';
export const LOAD_DATASET_DIRECTORY_INFO_FAIL = '[FileExplorer] Load Dataset Directory Info Fail';
export const LOAD_IMAGE_LIMIT = '[FileExplorer] Load Image Limit';
export const LOAD_IMAGE_LIMIT_SUCCESS = '[FileExplorer] Load Image Limit Success';
export const LOAD_IMAGE_LIMIT_FAIL = '[FileExplorer] Load Image Limit Fail';

export class LoadRootDirectoryInfoAction implements Action {
    readonly type = LOAD_ROOT_DIRECTORY_INFO;

    constructor(public payload: string) { }
}

export class LoadRootDirectoryInfoSuccessAction implements Action {
    readonly type = LOAD_ROOT_DIRECTORY_INFO_SUCCESS;

    constructor(public payload: FileInfo[]) { }
}

export class LoadRootDirectoryInfoFailAction implements Action {
    readonly type = LOAD_ROOT_DIRECTORY_INFO_FAIL;

    constructor(public payload: {} = null) { }
}

export class LoadDatasetDirectoryInfoAction implements Action {
    readonly type = LOAD_DATASET_DIRECTORY_INFO;

    constructor(public payload: string) { }
}

export class LoadDatasetDirectoryInfoSuccessAction implements Action {
    readonly type = LOAD_DATASET_DIRECTORY_INFO_SUCCESS;

    constructor(public payload: FileInfo[]) { }
}

export class LoadDatasetDirectoryInfoFailAction implements Action {
    readonly type = LOAD_DATASET_DIRECTORY_INFO_FAIL;

    constructor(public payload: {} = null) { }
}

export class LoadImageLimit implements Action {
    readonly type = LOAD_IMAGE_LIMIT;

    constructor(public payload: string) { }
}

export class LoadImageLimitSuccess implements Action {
    readonly type = LOAD_IMAGE_LIMIT_SUCCESS;

    constructor(public payload: ImageLimit) { }
}

export class LoadImageLimitFail implements Action {
    readonly type = LOAD_IMAGE_LIMIT_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadRootDirectoryInfoAction
    | LoadRootDirectoryInfoSuccessAction
    | LoadRootDirectoryInfoFailAction
    | LoadDatasetDirectoryInfoAction
    | LoadDatasetDirectoryInfoSuccessAction
    | LoadDatasetDirectoryInfoFailAction
    | LoadImageLimit
    | LoadImageLimitSuccess
    | LoadImageLimitFail;
