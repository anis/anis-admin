import { Action } from '@ngrx/store';

import { OutputFamily } from '../model';

export const LOAD_OUTPUT_FAMILY_LIST = '[OutputFamily] Load Output Family List';
export const LOAD_OUTPUT_FAMILY_LIST_SUCCESS = '[OutputFamily] Load Output Family List Success';
export const LOAD_OUTPUT_FAMILY_LIST_FAIL = '[OutputFamily] Load Output Family List Fail';
export const ADD_NEW_OUTPUT_FAMILY = '[OutputFamily] Add New Output Family';
export const ADD_NEW_OUTPUT_FAMILY_SUCCESS = '[OutputFamily] Add New Output Family Success';
export const ADD_NEW_OUTPUT_FAMILY_FAIL = '[OutputFamily] Add New Output Family Fail';
export const EDIT_OUTPUT_FAMILY = '[OutputFamily] Edit Output Family';
export const EDIT_OUTPUT_FAMILY_SUCCESS = '[OutputFamily] Edit Output Family Success';
export const EDIT_OUTPUT_FAMILY_FAIL = '[OutputFamily] Edit Output Family Fail';
export const DELETE_OUTPUT_FAMILY = '[OutputFamily] Delete Output Family';
export const DELETE_OUTPUT_FAMILY_SUCCESS = '[OutputFamily] Delete Output Family Success';
export const DELETE_OUTPUT_FAMILY_FAIL = '[OutputFamily] Delete Output Family Fail';

export class LoadOutputFamilyListAction implements Action {
    type = LOAD_OUTPUT_FAMILY_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadOutputFamilyListSuccessAction implements Action {
    type = LOAD_OUTPUT_FAMILY_LIST_SUCCESS;

    constructor(public payload: OutputFamily[]) { }
}

export class LoadOutputFamilyListFailAction implements Action {
    type = LOAD_OUTPUT_FAMILY_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewOutputFamilyAction implements Action {
    type = ADD_NEW_OUTPUT_FAMILY;

    constructor(public payload: OutputFamily) { }
}

export class AddNewOutputFamilySuccessAction implements Action {
    type = ADD_NEW_OUTPUT_FAMILY_SUCCESS;

    constructor(public payload: OutputFamily) { }
}

export class AddNewOutputFamilyFailAction implements Action {
    type = ADD_NEW_OUTPUT_FAMILY_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditOutputFamilyAction implements Action {
    type = EDIT_OUTPUT_FAMILY;

    constructor(public payload: OutputFamily) { }
}

export class EditOutputFamilySuccessAction implements Action {
    type = EDIT_OUTPUT_FAMILY_SUCCESS;

    constructor(public payload: OutputFamily) { }
}

export class EditOutputFamilyFailAction implements Action {
    type = EDIT_OUTPUT_FAMILY_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteOutputFamilyAction implements Action {
    type = DELETE_OUTPUT_FAMILY;

    constructor(public payload: OutputFamily) { }
}

export class DeleteOutputFamilySuccessAction implements Action {
    type = DELETE_OUTPUT_FAMILY_SUCCESS;

    constructor(public payload: OutputFamily) { }
}

export class DeleteOutputFamilyFailAction implements Action {
    type = DELETE_OUTPUT_FAMILY_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadOutputFamilyListAction
    | LoadOutputFamilyListSuccessAction
    | LoadOutputFamilyListFailAction
    | AddNewOutputFamilyAction
    | AddNewOutputFamilySuccessAction
    | AddNewOutputFamilyFailAction
    | EditOutputFamilyAction
    | EditOutputFamilySuccessAction
    | EditOutputFamilyFailAction
    | DeleteOutputFamilyAction
    | DeleteOutputFamilySuccessAction
    | DeleteOutputFamilyFailAction;
