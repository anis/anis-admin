import { Action } from '@ngrx/store';

import { Survey } from '../model';

export const LOAD_SURVEY_LIST = '[Survey] Load Survey List';
export const LOAD_SURVEY_LIST_WIP = '[Survey] Load Survey List WIP';
export const LOAD_SURVEY_LIST_SUCCESS = '[Survey] Load Survey List Success';
export const LOAD_SURVEY_LIST_FAIL = '[Survey] Load Survey List Fail';
export const ADD_NEW_SURVEY = '[Survey] Add New Survey';
export const ADD_NEW_SURVEY_SUCCESS = '[Survey] Add New Survey Success';
export const ADD_NEW_SURVEY_FAIL = '[Survey] Add New Survey Fail';
export const EDIT_SURVEY = '[Survey] Edit Survey';
export const EDIT_SURVEY_SUCCESS = '[Survey] Edit Survey Success';
export const EDIT_SURVEY_FAIL = '[Survey] Edit Survey Fail';
export const DELETE_SURVEY = '[Survey] Delete Survey';
export const DELETE_SURVEY_SUCCESS = '[Survey] Delete Survey Success';
export const DELETE_SURVEY_FAIL = '[Survey] Delete Survey Fail';
export const INCREMENT_NB_DATASETS = '[Survey] Increment Nb Datasets';
export const DECREMENT_NB_DATASETS = '[Survey] Decrement Nb Datasets';

export class LoadSurveyListAction implements Action {
    type = LOAD_SURVEY_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadSurveyListWipAction implements Action {
    type = LOAD_SURVEY_LIST_WIP;

    constructor(public payload: {} = null) { }
}

export class LoadSurveyListSuccessAction implements Action {
    type = LOAD_SURVEY_LIST_SUCCESS;

    constructor(public payload: Survey[]) { }
}

export class LoadSurveyListFailAction implements Action {
    type = LOAD_SURVEY_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewSurveyAction implements Action {
    type = ADD_NEW_SURVEY;

    constructor(public payload: Survey) { }
}

export class AddNewSurveySuccessAction implements Action {
    type = ADD_NEW_SURVEY_SUCCESS;

    constructor(public payload: Survey) { }
}

export class AddNewSurveyFailAction implements Action {
    type = ADD_NEW_SURVEY_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditSurveyAction implements Action {
    type = EDIT_SURVEY;

    constructor(public payload: Survey) { }
}

export class EditSurveySuccessAction implements Action {
    type = EDIT_SURVEY_SUCCESS;

    constructor(public payload: Survey) { }
}

export class EditSurveyFailAction implements Action {
    type = EDIT_SURVEY_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteSurveyAction implements Action {
    type = DELETE_SURVEY;

    constructor(public payload: Survey) { }
}

export class DeleteSurveySuccessAction implements Action {
    type = DELETE_SURVEY_SUCCESS;

    constructor(public payload: Survey) { }
}

export class DeleteSurveyFailAction implements Action {
    type = DELETE_SURVEY_FAIL;

    constructor(public payload: {} = null) { }
}

export class IncrementNbDatasetsAction implements Action {
    type = INCREMENT_NB_DATASETS;

    constructor(public payload: string) { }
}

export class DecrementNbDatasetsAction implements Action {
    type = DECREMENT_NB_DATASETS;

    constructor(public payload: string) { }
}

export type Actions
    = LoadSurveyListAction
    | LoadSurveyListWipAction
    | LoadSurveyListSuccessAction
    | LoadSurveyListFailAction
    | AddNewSurveyAction
    | AddNewSurveySuccessAction
    | AddNewSurveyFailAction
    | EditSurveyAction
    | EditSurveySuccessAction
    | EditSurveyFailAction
    | DeleteSurveyAction
    | DeleteSurveySuccessAction
    | DeleteSurveyFailAction
    | IncrementNbDatasetsAction
    | DecrementNbDatasetsAction;
