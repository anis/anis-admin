import { Action } from '@ngrx/store';

import { OutputCategory } from '../model';

export const LOAD_OUTPUT_CATEGORY_LIST = '[OutputCategory] Load Output Family List';
export const LOAD_OUTPUT_CATEGORY_LIST_SUCCESS = '[OutputCategory] Load Output Family List Success';
export const LOAD_OUTPUT_CATEGORY_LIST_FAIL = '[OutputCategory] Load Output Family List Fail';
export const ADD_NEW_OUTPUT_CATEGORY = '[OutputCategory] Add New Output Family';
export const ADD_NEW_OUTPUT_CATEGORY_SUCCESS = '[OutputCategory] Add New Output Family Success';
export const ADD_NEW_OUTPUT_CATEGORY_FAIL = '[OutputCategory] Add New Output Family Fail';
export const EDIT_OUTPUT_CATEGORY = '[OutputCategory] Edit Output Family';
export const EDIT_OUTPUT_CATEGORY_SUCCESS = '[OutputCategory] Edit Output Family Success';
export const EDIT_OUTPUT_CATEGORY_FAIL = '[OutputCategory] Edit Output Family Fail';
export const DELETE_OUTPUT_CATEGORY = '[OutputCategory] Delete Output Family';
export const DELETE_OUTPUT_CATEGORY_SUCCESS = '[OutputCategory] Delete Output Family Success';
export const DELETE_OUTPUT_CATEGORY_FAIL = '[OutputCategory] Delete Output Family Fail';

export class LoadOutputCategoryListAction implements Action {
    type = LOAD_OUTPUT_CATEGORY_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadOutputCategoryListSuccessAction implements Action {
    type = LOAD_OUTPUT_CATEGORY_LIST_SUCCESS;

    constructor(public payload: OutputCategory[]) { }
}

export class LoadOutputCategoryListFailAction implements Action {
    type = LOAD_OUTPUT_CATEGORY_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewOutputCategoryAction implements Action {
    type = ADD_NEW_OUTPUT_CATEGORY;

    constructor(public payload: OutputCategory) { }
}

export class AddNewOutputCategorySuccessAction implements Action {
    type = ADD_NEW_OUTPUT_CATEGORY_SUCCESS;

    constructor(public payload: OutputCategory) { }
}

export class AddNewOutputCategoryFailAction implements Action {
    type = ADD_NEW_OUTPUT_CATEGORY_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditOutputCategoryAction implements Action {
    type = EDIT_OUTPUT_CATEGORY;

    constructor(public payload: OutputCategory) { }
}

export class EditOutputCategorySuccessAction implements Action {
    type = EDIT_OUTPUT_CATEGORY_SUCCESS;

    constructor(public payload: OutputCategory) { }
}

export class EditOutputCategoryFailAction implements Action {
    type = EDIT_OUTPUT_CATEGORY_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteOutputCategoryAction implements Action {
    type = DELETE_OUTPUT_CATEGORY;

    constructor(public payload: OutputCategory) { }
}

export class DeleteOutputCategorySuccessAction implements Action {
    type = DELETE_OUTPUT_CATEGORY_SUCCESS;

    constructor(public payload: OutputCategory) { }
}

export class DeleteOutputCategoryFailAction implements Action {
    type = DELETE_OUTPUT_CATEGORY_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadOutputCategoryListAction
    | LoadOutputCategoryListSuccessAction
    | LoadOutputCategoryListFailAction
    | AddNewOutputCategoryAction
    | AddNewOutputCategorySuccessAction
    | AddNewOutputCategoryFailAction
    | EditOutputCategoryAction
    | EditOutputCategorySuccessAction
    | EditOutputCategoryFailAction
    | DeleteOutputCategoryAction
    | DeleteOutputCategorySuccessAction
    | DeleteOutputCategoryFailAction;
