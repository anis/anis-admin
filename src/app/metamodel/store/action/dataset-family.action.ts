import { Action } from '@ngrx/store';

import { DatasetFamily } from '../model';

export const LOAD_DATASET_FAMILY_LIST = '[DatasetFamily] Load Dataset Family List';
export const LOAD_DATASET_FAMILY_LIST_SUCCESS = '[DatasetFamily] Load Dataset Family List Success';
export const LOAD_DATASET_FAMILY_LIST_FAIL = '[DatasetFamily] Load Dataset Family List Fail';
export const ADD_NEW_DATASET_FAMILY = '[DatasetFamily] Add New Dataset Family';
export const ADD_NEW_DATASET_FAMILY_SUCCESS = '[DatasetFamily] Add New Dataset Family Success';
export const ADD_NEW_DATASET_FAMILY_FAIL = '[DatasetFamily] Add New Dataset Family Fail';
export const EDIT_DATASET_FAMILY = '[DatasetFamily] Edit Dataset Family';
export const EDIT_DATASET_FAMILY_SUCCESS = '[DatasetFamily] Edit Dataset Family Success';
export const EDIT_DATASET_FAMILY_FAIL = '[DatasetFamily] Edit Dataset Family Fail';
export const DELETE_DATASET_FAMILY = '[DatasetFamily] Delete Dataset Family';
export const DELETE_DATASET_FAMILY_SUCCESS = '[DatasetFamily] Delete Dataset Family Success';
export const DELETE_DATASET_FAMILY_FAIL = '[DatasetFamily] Delete Dataset Family Fail';

export class LoadDatasetFamilyListAction implements Action {
    type = LOAD_DATASET_FAMILY_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadDatasetFamilyListSuccessAction implements Action {
    type = LOAD_DATASET_FAMILY_LIST_SUCCESS;

    constructor(public payload: DatasetFamily[]) { }
}

export class LoadDatasetFamilyListFailAction implements Action {
    type = LOAD_DATASET_FAMILY_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewDatasetFamilyAction implements Action {
    type = ADD_NEW_DATASET_FAMILY;

    constructor(public payload: DatasetFamily) { }
}

export class AddNewDatasetFamilySuccessAction implements Action {
    type = ADD_NEW_DATASET_FAMILY_SUCCESS;

    constructor(public payload: DatasetFamily) { }
}

export class AddNewDatasetFamilyFailAction implements Action {
    type = ADD_NEW_DATASET_FAMILY_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditDatasetFamilyAction implements Action {
    type = EDIT_DATASET_FAMILY;

    constructor(public payload: DatasetFamily) { }
}

export class EditDatasetFamilySuccessAction implements Action {
    type = EDIT_DATASET_FAMILY_SUCCESS;

    constructor(public payload: DatasetFamily) { }
}

export class EditDatasetFamilyFailAction implements Action {
    type = EDIT_DATASET_FAMILY_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteDatasetFamilyAction implements Action {
    type = DELETE_DATASET_FAMILY;

    constructor(public payload: DatasetFamily) { }
}

export class DeleteDatasetFamilySuccessAction implements Action {
    type = DELETE_DATASET_FAMILY_SUCCESS;

    constructor(public payload: DatasetFamily) { }
}

export class DeleteDatasetFamilyFailAction implements Action {
    type = DELETE_DATASET_FAMILY_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadDatasetFamilyListAction
    | LoadDatasetFamilyListSuccessAction
    | LoadDatasetFamilyListFailAction
    | AddNewDatasetFamilyAction
    | AddNewDatasetFamilySuccessAction
    | AddNewDatasetFamilyFailAction
    | EditDatasetFamilyAction
    | EditDatasetFamilySuccessAction
    | EditDatasetFamilyFailAction
    | DeleteDatasetFamilyAction
    | DeleteDatasetFamilySuccessAction
    | DeleteDatasetFamilyFailAction;
