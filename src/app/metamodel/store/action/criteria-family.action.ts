import { Action } from '@ngrx/store';

import { CriteriaFamily } from '../model';

export const LOAD_CRITERIA_FAMILY_LIST = '[CriteriaFamily] Load Criteria Family List';
export const LOAD_CRITERIA_FAMILY_LIST_SUCCESS = '[CriteriaFamily] Load Criteria Family List Success';
export const LOAD_CRITERIA_FAMILY_LIST_FAIL = '[CriteriaFamily] Load Criteria Family List Fail';
export const ADD_NEW_CRITERIA_FAMILY = '[CriteriaFamily] Add New Criteria Family';
export const ADD_NEW_CRITERIA_FAMILY_SUCCESS = '[CriteriaFamily] Add New Criteria Family Success';
export const ADD_NEW_CRITERIA_FAMILY_FAIL = '[CriteriaFamily] Add New Criteria Family Fail';
export const EDIT_CRITERIA_FAMILY = '[CriteriaFamily] Edit Criteria Family';
export const EDIT_CRITERIA_FAMILY_SUCCESS = '[CriteriaFamily] Edit Criteria Family Success';
export const EDIT_CRITERIA_FAMILY_FAIL = '[CriteriaFamily] Edit Criteria Family Fail';
export const DELETE_CRITERIA_FAMILY = '[CriteriaFamily] Delete Criteria Family';
export const DELETE_CRITERIA_FAMILY_SUCCESS = '[CriteriaFamily] Delete Criteria Family Success';
export const DELETE_CRITERIA_FAMILY_FAIL = '[CriteriaFamily] Delete Criteria Family Fail';

export class LoadCriteriaFamilyListAction implements Action {
    type = LOAD_CRITERIA_FAMILY_LIST;

    constructor(public payload: {} = null) { }
}

export class LoadCriteriaFamilyListSuccessAction implements Action {
    type = LOAD_CRITERIA_FAMILY_LIST_SUCCESS;

    constructor(public payload: CriteriaFamily[]) { }
}

export class LoadCriteriaFamilyListFailAction implements Action {
    type = LOAD_CRITERIA_FAMILY_LIST_FAIL;

    constructor(public payload: {} = null) { }
}

export class AddNewCriteriaFamilyAction implements Action {
    type = ADD_NEW_CRITERIA_FAMILY;

    constructor(public payload: CriteriaFamily) { }
}

export class AddNewCriteriaFamilySuccessAction implements Action {
    type = ADD_NEW_CRITERIA_FAMILY_SUCCESS;

    constructor(public payload: CriteriaFamily) { }
}

export class AddNewCriteriaFamilyFailAction implements Action {
    type = ADD_NEW_CRITERIA_FAMILY_FAIL;

    constructor(public payload: {} = null) { }
}

export class EditCriteriaFamilyAction implements Action {
    type = EDIT_CRITERIA_FAMILY;

    constructor(public payload: CriteriaFamily) { }
}

export class EditCriteriaFamilySuccessAction implements Action {
    type = EDIT_CRITERIA_FAMILY_SUCCESS;

    constructor(public payload: CriteriaFamily) { }
}

export class EditCriteriaFamilyFailAction implements Action {
    type = EDIT_CRITERIA_FAMILY_FAIL;

    constructor(public payload: {} = null) { }
}

export class DeleteCriteriaFamilyAction implements Action {
    type = DELETE_CRITERIA_FAMILY;

    constructor(public payload: CriteriaFamily) { }
}

export class DeleteCriteriaFamilySuccessAction implements Action {
    type = DELETE_CRITERIA_FAMILY_SUCCESS;

    constructor(public payload: CriteriaFamily) { }
}

export class DeleteCriteriaFamilyFailAction implements Action {
    type = DELETE_CRITERIA_FAMILY_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadCriteriaFamilyListAction
    | LoadCriteriaFamilyListSuccessAction
    | LoadCriteriaFamilyListFailAction
    | AddNewCriteriaFamilyAction
    | AddNewCriteriaFamilySuccessAction
    | AddNewCriteriaFamilyFailAction
    | EditCriteriaFamilyAction
    | EditCriteriaFamilySuccessAction
    | EditCriteriaFamilyFailAction
    | DeleteCriteriaFamilyAction
    | DeleteCriteriaFamilySuccessAction
    | DeleteCriteriaFamilyFailAction;
