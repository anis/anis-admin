import { Component, Input, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Survey, Database } from '../../store/model';

@Component({
    selector: 'app-form-survey',
    templateUrl: 'form-survey.component.html'
})
export class FormSurveyComponent {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: Survey = new Survey();
    @Input() databaseList: Database[];
    @Output() submitted: EventEmitter<Survey> = new EventEmitter();

    emit(survey: Survey) {
        let surveyEmitted: Survey;
        (this.model.name) ? surveyEmitted = {name: this.model.name, ...survey} : surveyEmitted = survey;
        this.submitted.emit(surveyEmitted);
    }
}
