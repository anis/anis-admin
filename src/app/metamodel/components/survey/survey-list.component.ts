import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Survey, Database } from '../../store/model';

@Component({
    selector: 'app-survey-list',
    templateUrl: 'survey-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SurveyListComponent {
    @Input() surveyList: Survey[];
    @Input() databaseList: Database[];
    @Output() deleteSurvey: EventEmitter<Survey> = new EventEmitter();

    modalRef: BsModalRef;
    surveyForDel: Survey;

    constructor(private modalService: BsModalService) { }

    isNoDatasetsAttachedToSurveyd(surveyName: string): boolean {
        return this.surveyList.find(p => p.name === surveyName).nb_datasets === 0;
    }

    getDatabaseById(idDatabase: number): Database {
        return this.databaseList.find(database => database.id === idDatabase);
    }

    openModal(template: TemplateRef<any>, survey: Survey) {
        this.surveyForDel = survey;
        this.modalRef = this.modalService.show(template);
    }

    confirmDel() {
        this.deleteSurvey.emit(this.surveyForDel);
        this.modalRef.hide();
    }
}
