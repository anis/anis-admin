import { Component, Input, Output, EventEmitter, ViewChild, TemplateRef, OnChanges, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Dataset, Survey, Attribute, DatasetFamily, FileInfo, ImageLimit } from '../../store/model';

@Component({
    selector: 'app-form-dataset',
    templateUrl: 'form-dataset.component.html',
    styleUrls: ['form-dataset.component.css']
})
export class FormDatasetComponent implements OnChanges {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: Dataset = new Dataset();
    @Input() surveyList: Survey[];
    @Input() attributeList: Attribute[];
    @Input() tableList: string[];
    @Input() datasetFamilyList: DatasetFamily[];
    @Input() idDatasetFamilyQueryParam: number = 0;
    @Input() rootDirectoryInfo: FileInfo[];
    @Input() rootDirectoryInfoIsLoading: boolean;
    @Input() rootDirectoryInfoIsLoaded: boolean;
    @Input() datasetDirectoryInfo: FileInfo[];
    @Input() datasetDirectoryInfoIsLoading: boolean;
    @Input() datasetDirectoryInfoIsLoaded: boolean;
    @Input() imageLimitIsLoading: boolean;
    @Input() imageLimitIsLoaded: boolean;
    @Input() imageLimit: ImageLimit;
    @Output() changeSurvey: EventEmitter<number> = new EventEmitter();
    @Output() loadRootDirectoryInfo: EventEmitter<string> = new EventEmitter();
    @Output() loadDatasetDirectoryInfo: EventEmitter<string> = new EventEmitter();
    @Output() loadFitsImageLimits: EventEmitter<string> = new EventEmitter();
    @Output() submitted: EventEmitter<Dataset> = new EventEmitter();

    modalRef: BsModalRef;
    dataPathFileExplorerPristine = true;
    fileExplorerPath = '';

    datasetFileExplorerPath = '';
    imageSelected = null;
    datasetImages = [];

    constructor(private modalService: BsModalService) { }

    ngOnChanges(changes: SimpleChanges) {
        this.datasetImages = changes.model.currentValue.config.images.map(i => ({...i}));
    }

    dataPathOpenModal(template: TemplateRef<any>) {
        this.dataPathFileExplorerPristine = true;
        this.fileExplorerPath = this.ngForm.controls['data_path'].value;
        if (!this.fileExplorerPath) {
            this.fileExplorerPath = '';
        }
        this.modalRef = this.modalService.show(template);
        this.loadRootDirectoryInfo.emit(this.fileExplorerPath);
    }
    
    dataPathAction(fileInfo: FileInfo): void {
        if (fileInfo.name === '.' || fileInfo.type !== 'dir') {
            return;
        }

        if (fileInfo.name === '..') {
            this.fileExplorerPath = this.fileExplorerPath.substr(0, this.fileExplorerPath.lastIndexOf("/"));
        } else {
            this.fileExplorerPath += '/' + fileInfo.name;
        }
        this.dataPathFileExplorerPristine = false;
        this.loadRootDirectoryInfo.emit(this.fileExplorerPath);
    }
    
    selectDirectory() {
        this.ngForm.controls['data_path'].setValue(this.fileExplorerPath);
        this.ngForm.controls['data_path'].markAsDirty();
        this.modalRef.hide();
    }

    newImageOpenModal(template: TemplateRef<any>): void {
        this.imageSelected = null;
        this.modalRef = this.modalService.show(template);
        this.loadDatasetDirectoryInfo.emit(this.datasetFileExplorerPath);
    }

    newImageAction(fileInfo: FileInfo): void {
        if (fileInfo.name === '.') {
            return;
        }

        if (fileInfo.type === 'file' && fileInfo.mimetype === 'image/fits') {
            this.loadFitsImageLimits.emit(this.datasetFileExplorerPath + '/' + fileInfo.name);
            this.imageSelected = fileInfo;
        } else {
            if (fileInfo.name === '..') {
                this.datasetFileExplorerPath = this.datasetFileExplorerPath.substr(0, this.datasetFileExplorerPath.lastIndexOf("/"));
            } else {
                this.datasetFileExplorerPath += '/' + fileInfo.name;
            }
            this.imageSelected = null;
            this.loadDatasetDirectoryInfo.emit(this.datasetFileExplorerPath);
        }
    }

    selectImage(): void {
        const name = this.datasetFileExplorerPath + '/' + this.imageSelected.name;
        let id = 1;
        if (this.datasetImages.length > 0) {
            id = Math.max(...this.datasetImages.map(i => i.id)) + 1;
        }
        this.datasetImages.push({ 
            id,
            name, 
            size: this.imageSelected.size,
            ...this.imageLimit,
            stretch: 'linear',
            pmin: 0.25,
            pmax: 99.75
        });
        this.modalRef.hide();
        this.ngForm.controls['data_path'].markAsDirty();
    }

    editImageOpenModal(template: TemplateRef<any>, image) {
        this.imageSelected = image;
        this.modalRef = this.modalService.show(template);
    }

    editImage(image) {
        this.datasetImages = [
            ...this.datasetImages.filter(i => i.id !== this.imageSelected.id), 
            {
                ...this.imageSelected,
                ...image
            }
        ];
        this.modalRef.hide();
        this.ngForm.controls['data_path'].markAsDirty();
    }

    deleteImage(image): void {
        this.datasetImages = this.datasetImages.filter(i => i.id !== image.id);
        this.ngForm.controls['data_path'].markAsDirty();
    }

    isConeSearchDisabled(): boolean {
        return (!this.model.name) ? true : false;
    }

    getDatasetFamilyByIdQueryParam(): DatasetFamily {
        return this.datasetFamilyList.find(datasetFamily => datasetFamily.id === this.idDatasetFamilyQueryParam);
    }

    getBackgroundEnabled(image) {
        const m = this.model.config.cone_search.background.find(i => i.id === image.id);
        if (m) {
            return m.enabled;
        } else {
            return false;
        }
    }

    getBackgroundDisplay(image) {
        const m = this.model.config.cone_search.background.find(i => i.id === image.id);
        if (m) {
            return m.display;
        } else {
            return (image.id + 1) * 10;
        }
    }

    getImageFilename(path: string) {
        return path.replace(/^.*[\\\/]/, '');
    }

    onChange(surveyName: string): void {
        this.changeSurvey.emit(this.surveyList.find(survey => survey.name === surveyName).id_database);
    }
    
    emit(dataset: Dataset): void {
        let datasetEmitted: Dataset;
        (this.model.name) ? datasetEmitted = {name: this.model.name, ...dataset} : datasetEmitted = dataset;
        if (!dataset.data_path) {
            datasetEmitted.data_path = '';
        }
        if (!dataset.vo) {
            datasetEmitted.vo = false;
        }
        if (this.idDatasetFamilyQueryParam > 0) {
            datasetEmitted.id_dataset_family = this.idDatasetFamilyQueryParam;
        }
        datasetEmitted.config = {
            images: this.datasetImages,
            cone_search: {
                enabled: this.ngForm.form.value.cone_search_enabled,
                opened: this.ngForm.form.value.cone_search_opened,
                column_ra: +this.ngForm.form.value.column_ra,
                column_dec: +this.ngForm.form.value.column_dec,
                plot_enabled: this.ngForm.form.value.plot_enabled,
                sdss_enabled: this.ngForm.form.value.sdss_enabled,
                sdss_display: this.ngForm.form.value.sdss_display,
                background: this.datasetImages.map((v, i) => {
                    const e = `${v.id}_enabled`;
                    const d = `${v.id}_display`;
                    return {id: v.id, enabled: this.ngForm.form.value[e], display: this.ngForm.form.value[d]}
                })
            },
            download: {
                enabled: this.ngForm.form.value.download_enabled,
                opened: this.ngForm.form.value.download_opened,
                csv: this.ngForm.form.value.download_result_csv,
                ascii: this.ngForm.form.value.download_result_ascii,
                vo: this.ngForm.form.value.download_result_vo,
                archive: this.ngForm.form.value.download_result_archive,
            },
            summary: {
                enabled: this.ngForm.form.value.summary_enabled,
                opened: this.ngForm.form.value.summary_opened
            },
            server_link: {
                enabled: this.ngForm.form.value.server_link_enabled,
                opened: this.ngForm.form.value.server_link_opened
            },
            samp: {
                enabled: this.ngForm.form.value.samp_enabled,
                opened: this.ngForm.form.value.samp_opened
            },
            datatable: {
                enabled: this.ngForm.form.value.datatable_enabled,
                opened: this.ngForm.form.value.datatable_opened,
                selectable_row: this.ngForm.form.value.selectable_row
            }
        };
        this.submitted.emit(datasetEmitted);
    }
}
