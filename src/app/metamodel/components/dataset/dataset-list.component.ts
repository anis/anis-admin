import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Dataset, DatasetFamily } from '../../store/model';

@Component({
    selector: 'app-dataset-list',
    templateUrl: 'dataset-list.component.html',
    styleUrls: [ 'dataset-list.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetListComponent {
    @Input() datasetList: Dataset[];
    @Input() datasetFamily: DatasetFamily;
    @Output() deleteDataset: EventEmitter<Dataset> = new EventEmitter();

    modalRef: BsModalRef;
    datasetForDel: Dataset;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>, dataset: Dataset) {
        this.datasetForDel = dataset;
        this.modalRef = this.modalService.show(template);
    }

    confirmDel() {
        this.deleteDataset.emit(this.datasetForDel);
        this.modalRef.hide();
    }
}
