import { Component, Input, Output, EventEmitter, ViewChild, SimpleChanges, OnChanges } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Group, Dataset } from '../../store/model';

@Component({
    selector: 'app-form-group',
    templateUrl: 'form-group.component.html'
})
export class FormGroupComponent implements OnChanges {
    @ViewChild(NgForm, { static: true }) ngForm: NgForm;
    @Input() model: Group = new Group();
    @Input() datasetList: Dataset[];
    @Output() submitted: EventEmitter<Group> = new EventEmitter();
    public availableDatasets: string[];
    public groupDatasets: string[] = [];

    ngOnChanges(changes: SimpleChanges) {
        if (changes.model && changes.model.currentValue) {
            this.groupDatasets = [...this.model.datasets];
        }
    }

    emit(group: Group) {
        this.submitted.emit({
            id: this.model.id,
            role: group.role,
            instance_name: this.model.instance_name,
            datasets: this.groupDatasets
        });
    }

    getAvailableDatasets() {
        return this.datasetList.filter(d => !this.groupDatasets.includes(d.name));
    }

    addDatasets(selectElement) {
        let availableDatasetsSelected = [];
        for (var i = 0; i < selectElement.options.length; i++) {
            const optionElement = selectElement.options[i];
            if (optionElement.selected == true) {
                availableDatasetsSelected.push(optionElement.value);
            }
        }
        this.groupDatasets.push(...availableDatasetsSelected);
        this.ngForm.control.markAsDirty();
    }

    removeDatasets(selectElement) {
        let groupDatasetsSelected = [];
        for (var i = 0; i < selectElement.options.length; i++) {
            const optionElement = selectElement.options[i];
            if (optionElement.selected == true) {
                groupDatasetsSelected.push(optionElement.value);
            }
        }
        this.groupDatasets = [...this.groupDatasets.filter(d => !groupDatasetsSelected.includes(d))]
        this.ngForm.control.markAsDirty();
    }
}
