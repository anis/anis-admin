import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Group } from '../../store/model';

@Component({
    selector: 'app-group-list',
    templateUrl: 'group-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupListComponent {
    @Input() groupList: Group[];
    @Output() deleteGroup: EventEmitter<Group> = new EventEmitter();

    modalRef: BsModalRef;
    groupForDel: Group;

    constructor(private modalService: BsModalService) { }

    getDatasetsLenght(group: Group) {
        return group.datasets.length;
    }

    openModal(template: TemplateRef<any>, group: Group) {
        this.groupForDel = group;
        this.modalRef = this.modalService.show(template);
    }

    confirmDel() {
        this.deleteGroup.emit(this.groupForDel);
        this.modalRef.hide();
    }
}
