import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Instance, DatasetFamily, Dataset } from '../../store/model';

@Component({
    selector: 'app-dataset-family-list',
    templateUrl: 'dataset-family-list.component.html',
    styleUrls: [ 'dataset-family-list.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetFamilyListComponent {
    @Input() instance: Instance;
    @Input() datasetFamilyList: DatasetFamily[];
    @Input() datasetList: Dataset[];
    @Output() addDatasetFamily: EventEmitter<DatasetFamily> = new EventEmitter();
    @Output() editDatasetFamily: EventEmitter<DatasetFamily> = new EventEmitter();
    @Output() deleteDatasetFamily: EventEmitter<DatasetFamily> = new EventEmitter();
    @Output() deleteDataset: EventEmitter<Dataset> = new EventEmitter();

    modalRef: BsModalRef;
    datasetFamilySelected: DatasetFamily;

    constructor(private modalService: BsModalService) { }

    isDatasetFamilyEmpty(idFamily: number): boolean {
        return this.getDatasetListByFamily(idFamily).length === 0;
    }

    isAuthenticationEnabled(): boolean {
        return this.instance.config.authentication.allowed;
    }

    getDatasetListByFamily(idFamily: number): Dataset[] {
        return this.datasetList.filter(dataset => dataset.id_dataset_family === idFamily);
    }

    openModal(template: TemplateRef<any>, datasetFamily: DatasetFamily) {
        this.datasetFamilySelected = datasetFamily;
        this.modalRef = this.modalService.show(template);
    }

    openModalNew(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    confirmAdd(datasetFamily: DatasetFamily) {
        this.addDatasetFamily.emit(datasetFamily);
        this.modalRef.hide();
    }

    confirmEdit(datasetFamily: DatasetFamily) {
        this.editDatasetFamily.emit(datasetFamily);
        this.modalRef.hide();
    }

    confirmDel() {
        this.deleteDatasetFamily.emit(this.datasetFamilySelected);
        this.modalRef.hide();
    }
}
