import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { DatasetFamily } from '../../store/model';

@Component({
    selector: 'app-form-dataset-family',
    templateUrl: 'form-dataset-family.component.html'
})
export class FormDatasetFamilyComponent {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: DatasetFamily = new DatasetFamily();
    @Output() submitted: EventEmitter<DatasetFamily> = new EventEmitter();

    emit(datasetFamily: DatasetFamily) {
        this.submitted.emit({id: this.model.id, ...datasetFamily});
    }
}
