import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { Attribute, OutputCategory } from '../../../store/model';

@Component({
    selector: '[output]',
    templateUrl: 'tr-output.component.html',
    styleUrls: [ '../tr.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrOutputComponent {
    @Input() set attribute(attribute: Attribute) {
        this._attribute = attribute;
        this.outputForm.controls.name.setValue(attribute.name);
        this.outputForm.controls.id_output_category.setValue(attribute.id_output_category);
        this.outputForm.controls.output_display.setValue(attribute.output_display);
        this.outputForm.controls.selected.setValue(attribute.selected);
    }
    @Input() public outputCategoryList: OutputCategory[];
    @Output() public save: EventEmitter<Attribute> = new EventEmitter();

    _attribute: Attribute;
    outputForm = new FormGroup({
        name: new FormControl({ value: '', disabled: true }),
        id_output_category: new FormControl(),
        output_display: new FormControl(),
        selected: new FormControl()
    });

    outputCategoryOnChange(id: string): void {
        if (id === '') {
            this.outputForm.controls.id_output_category.setValue(null);
        }
    }

    emitSave(): void {
        this.save.emit({
            ...this._attribute,
            ...this.outputForm.value
        });
    }
}
