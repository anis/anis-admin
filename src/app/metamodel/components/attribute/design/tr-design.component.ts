import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, TemplateRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { SettingsSelectOption } from '../../../../settings/store/model';
import { Attribute } from '../../../store/model';

@Component({
    selector: '[design]',
    templateUrl: 'tr-design.component.html',
    styleUrls: [ '../tr.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrDesignComponent {
    @Input() set attribute(attribute: Attribute) {
        this._attribute = attribute;
        this.designForm.controls.id.setValue(attribute.id);
        this.designForm.controls.name.setValue(attribute.name);
        this.designForm.controls.search_flag.setValue(attribute.search_flag);
        this.designForm.controls.label.setValue(attribute.label);
        this.designForm.controls.form_label.setValue(attribute.form_label);
        this.designForm.controls.description.setValue(attribute.description);
    }
    @Input() public searchFlags: SettingsSelectOption[];
    @Output() public save: EventEmitter<Attribute> = new EventEmitter();
    @Output() public delete: EventEmitter<Attribute> = new EventEmitter();

    _attribute: Attribute;
    designForm = new FormGroup({
        id: new FormControl({value: '', disabled: true}),
        name: new FormControl(),
        search_flag: new FormControl(),
        label: new FormControl(),
        form_label: new FormControl(),
        description: new FormControl()
    });

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    emitSave(): void {
        this.save.emit({
            ...this._attribute,
            ...this.designForm.value
        })
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    confirmDel() {
        this.delete.emit(this._attribute);
        this.modalRef.hide();
    }
}
