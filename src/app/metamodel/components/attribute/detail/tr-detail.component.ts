import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { SettingsSelectOption } from '../../../../settings/store/model';
import { Attribute } from '../../../store/model';

@Component({
    selector: '[detail]',
    templateUrl: 'tr-detail.component.html',
    styleUrls: [ '../tr.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrDetailComponent {
    @Input() set attribute(attribute: Attribute) {
        this._attribute = attribute;
        this.detailForm.controls.name.setValue(attribute.name);
        this.detailForm.controls.detail.setValue(attribute.detail);
        this.detailForm.controls.renderer_detail.setValue(attribute.renderer_detail);
        this.detailForm.controls.display_detail.setValue(attribute.display_detail);
    }
    @Input() public rendererDetailList: SettingsSelectOption[];
    @Output() public save: EventEmitter<Attribute> = new EventEmitter();

    _attribute: Attribute;
    detailForm = new FormGroup({
        name: new FormControl({ value: '', disabled: true }),
        detail: new FormControl(),
        renderer_detail: new FormControl(),
        display_detail: new FormControl()
    });

    newFunction() {
        if (!this.detailForm.controls.detail.value) {
            this.rendererDetailOnChange('');
        }
    }

    rendererDetailOnChange(rendererDetail: string) {
        if (rendererDetail === '') {
            this.detailForm.controls.renderer_detail.setValue(null);
        }
    }

    emitSave(): void {
        this.save.emit({
            ...this._attribute,
            ...this.detailForm.value
        });
    }
}
