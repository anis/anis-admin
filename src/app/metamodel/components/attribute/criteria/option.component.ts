import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Option } from '../../../store/model';

@Component({
    selector: 'app-option',
    templateUrl: 'option.component.html',
    styleUrls: [ 'option.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionComponent {
    @Input() option: Option = new Option();
    @Output() add: EventEmitter<Option> = new EventEmitter();
    @Output() delete: EventEmitter<{}> = new EventEmitter();

    addAction(form: Option) {
        if (this.option.label !== undefined) {
            this.add.emit({...this.option, value: form.value, display: form.display});
        } else {
            this.add.emit({...form});
        }
    }
}
