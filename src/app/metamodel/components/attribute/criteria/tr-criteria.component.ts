import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, TemplateRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { sortDisplay } from '../../../../shared/utils';
import { SettingsSelectOption } from '../../../../settings/store/model';
import { Attribute, CriteriaFamily, Option } from '../../../store/model';

@Component({
    selector: '[criteria]',
    templateUrl: 'tr-criteria.component.html',
    styleUrls: [ '../tr.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrCriteriaComponent {
    @Input() set attribute(attribute: Attribute) {
        this._attribute = attribute;
        this.criteriaForm.controls.name.setValue(attribute.name);
        this.criteriaForm.controls.type.setValue(attribute.type);
        this.criteriaForm.controls.id_criteria_family.setValue(attribute.id_criteria_family);
        this.criteriaForm.controls.search_type.setValue(attribute.search_type);
        this.criteriaForm.controls.operator.setValue(attribute.operator);
        this.criteriaForm.controls.min.setValue(attribute.min);
        this.criteriaForm.controls.max.setValue(attribute.max);
        this.criteriaForm.controls.placeholder_min.setValue(attribute.placeholder_min);
        this.criteriaForm.controls.placeholder_max.setValue(attribute.placeholder_max);
        this.criteriaForm.controls.criteria_display.setValue(attribute.criteria_display);
        const optionsList = attribute.options;
        if (optionsList) {
            this.optionList = [...optionsList];
        }
    }
    @Input() public optionListGenerated: string[];
    @Input() public criteriaFamilyList: CriteriaFamily[];
    @Input() public searchTypeList: SettingsSelectOption[];
    @Input() public operatorList: SettingsSelectOption[];
    @Output() public save: EventEmitter<Attribute> = new EventEmitter();
    @Output() public generateOptionList: EventEmitter<Attribute> = new EventEmitter();

    modalRef: BsModalRef;

    readonly searchTypesWithOperator: string[] = [
        'field', 'select', 'select-multiple', 'datalist', 'radio', 'checkbox', 'date', 'time', 'date-time'
    ];

    _attribute: Attribute;
    optionList: Option[] = [];
    criteriaForm = new FormGroup({
        name: new FormControl({ value: '', disabled: true }),
        type: new FormControl({ value: '', disabled: true }),
        id_criteria_family: new FormControl(),
        search_type: new FormControl(),
        operator: new FormControl(),
        min: new FormControl(),
        max: new FormControl(),
        placeholder_min: new FormControl(),
        placeholder_max: new FormControl(),
        criteria_display: new FormControl()
    });

    constructor(private modalService: BsModalService) { }

    generateOptionListValues(template: TemplateRef<any>): void {
        this.generateOptionList.emit(this._attribute);
        this.modalRef = this.modalService.show(template);
    }

    confirmGeneratedOptionList(): void {
        let newOptionList: Option[] = [];
        for (let i = 0; i < this.optionListGenerated.length; i++) {
            newOptionList.push({
                label: this.optionListGenerated[i],
                value: this.optionListGenerated[i],
                display: ( i + 1) * 10
            });
        }
        this.optionList = newOptionList;
        this.modalRef.hide();
    }
    
    criteriaFamilyOnChange(id: string): void {
        if (id === '') {
            this.criteriaForm.controls.id_criteria_family.setValue(null);
            this.searchTypeOnChange('');
        }
    }
    
    searchTypeOnChange(type: string): void {
        if (type === '') {
            this.criteriaForm.controls.search_type.setValue(null);
            this.operatorOnChange('');
        }
    }

    operatorOnChange(operator: string): void {
        if (operator === '') {
            this.criteriaForm.controls.operator.setValue(null);
            this.criteriaForm.controls.min.setValue(null);
            this.criteriaForm.controls.max.setValue(null);
            this.criteriaForm.controls.placeholder_min.setValue(null);
            this.criteriaForm.controls.placeholder_max.setValue(null);
            this.optionList = [];
        }
    }

    getMinValuePlaceholder(searchType: string): string {
        if (searchType === 'between' || searchType === 'between-date') {
            return 'Default min value (optional)';
        } else {
            return 'Default value (optional)';
        }
    }

    editOption(option: Option): void {
        const index = this.optionList.findIndex(o => o.label === option.label);
        if (index >= 0) {
            this.optionList[index] = option;
        }
        this.optionList = this.optionList.sort(sortDisplay);
        this.criteriaForm.markAsDirty();
    }

    addOption(option: Option): void {
        this.optionList.push(option);
        this.optionList = this.optionList.sort(sortDisplay);
        this.criteriaForm.markAsDirty();
    }

    deleteOption(option: Option): void {
        this.optionList = this.optionList.filter(o => o.label !== option.label);
        this.criteriaForm.markAsDirty();
    }

    emitSave(): void {
        this.save.emit({
            ...this._attribute,
            ...this.criteriaForm.value,
            options: (this.optionList.length > 0) ? this.optionList : null
        })
    }
}
