import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { Attribute } from '../../../store/model';

@Component({
    selector: '[vo]',
    templateUrl: 'tr-vo.component.html',
    styleUrls: [ '../tr.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrVoComponent {
    @Input() set attribute(attribute: Attribute) {
        this._attribute = attribute;
        this.voForm.controls.name.setValue(attribute.name);
        this.voForm.controls.vo_utype.setValue(attribute.vo_utype);
        this.voForm.controls.vo_ucd.setValue(attribute.vo_ucd);
        this.voForm.controls.vo_unit.setValue(attribute.vo_unit);
        this.voForm.controls.vo_description.setValue(attribute.vo_description);
        this.voForm.controls.vo_datatype.setValue(attribute.vo_datatype);
        this.voForm.controls.vo_size.setValue(attribute.vo_size);
    }
    @Output() public save: EventEmitter<Attribute> = new EventEmitter();

    _attribute: Attribute;
    voForm = new FormGroup({
        name: new FormControl({ value: '', disabled: true }),
        vo_utype: new FormControl(),
        vo_ucd: new FormControl(),
        vo_unit: new FormControl(),
        vo_description: new FormControl(),
        vo_datatype: new FormControl(),
        vo_size: new FormControl()
    });

    emitSave(): void {
        this.save.emit({
            ...this._attribute,
            ...this.voForm.value
        });
    }
}
