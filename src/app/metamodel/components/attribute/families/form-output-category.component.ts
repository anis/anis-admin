import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { OutputCategory, OutputFamily } from '../../../store/model';

@Component({
    selector: 'app-form-output-category',
    templateUrl: 'form-output-category.component.html'
})
export class FormOutputCategoryComponent {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: OutputCategory = new OutputCategory();
    @Input() outputFamily: OutputFamily;
    @Output() submitted: EventEmitter<OutputCategory> = new EventEmitter();

    emit(outputCategory: OutputCategory) {
        this.submitted.emit({id: this.model.id, id_output_family: this.outputFamily.id, ...outputCategory});
    }
}
