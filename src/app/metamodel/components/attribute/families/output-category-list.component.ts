import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { OutputCategory, OutputFamily } from '../../../store/model';

@Component({
    selector: 'app-output-category-list',
    templateUrl: 'output-category-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputCategoryListComponent {
    @Input() outputCategoryList: OutputCategory[];
    @Input() outputFamily: OutputFamily;
    @Output() addOutputCategory: EventEmitter<OutputCategory> = new EventEmitter();
    @Output() editOutputCategory: EventEmitter<OutputCategory> = new EventEmitter();
    @Output() deleteOutputCategory: EventEmitter<OutputCategory> = new EventEmitter();

    modalRef: BsModalRef;
    outputCategorySelected: OutputCategory;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>, outputCategory: OutputCategory) {
        this.outputCategorySelected = outputCategory;
        this.modalRef = this.modalService.show(template);
    }

    openModalNew(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    confirmAdd(outputCategory: OutputCategory) {
        this.addOutputCategory.emit(outputCategory);
        this.modalRef.hide();
    }

    confirmEdit(outputCategory: OutputCategory) {
        this.editOutputCategory.emit(outputCategory);
        this.modalRef.hide();
    }

    confirmDel() {
        this.deleteOutputCategory.emit(this.outputCategorySelected);
        this.modalRef.hide();
    }
}
