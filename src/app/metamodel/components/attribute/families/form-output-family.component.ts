import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { OutputFamily } from '../../../store/model';

@Component({
    selector: 'app-form-output-family',
    templateUrl: 'form-output-family.component.html'
})
export class FormOutputFamilyComponent {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: OutputFamily = new OutputFamily();
    @Output() submitted: EventEmitter<OutputFamily> = new EventEmitter();

    emit(outputFamily: OutputFamily) {
        this.submitted.emit({id: this.model.id, ...outputFamily});
    }
}
