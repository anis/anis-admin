import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { CriteriaFamily } from '../../../store/model';

@Component({
    selector: 'app-criteria-family-list',
    templateUrl: 'criteria-family-list.component.html',
    styleUrls: [ 'criteria-family-list.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CriteriaFamilyListComponent {
    @Input() criteriaFamilyList: CriteriaFamily[];
    @Output() addCriteriaFamily: EventEmitter<CriteriaFamily> = new EventEmitter();
    @Output() editCriteriaFamily: EventEmitter<CriteriaFamily> = new EventEmitter();
    @Output() deleteCriteriaFamily: EventEmitter<CriteriaFamily> = new EventEmitter();

    modalRef: BsModalRef;
    criteriaFamilySelected: CriteriaFamily;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>, criteriaFamily: CriteriaFamily) {
        this.criteriaFamilySelected = criteriaFamily;
        this.modalRef = this.modalService.show(template);
    }

    openModalNew(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    confirmAdd(criteriaFamily: CriteriaFamily) {
        this.addCriteriaFamily.emit(criteriaFamily);
        this.modalRef.hide();
    }

    confirmEdit(criteriaFamily: CriteriaFamily) {
        this.editCriteriaFamily.emit(criteriaFamily);
        this.modalRef.hide();
    }

    confirmDel() {
        this.deleteCriteriaFamily.emit(this.criteriaFamilySelected);
        this.modalRef.hide();
    }
}
