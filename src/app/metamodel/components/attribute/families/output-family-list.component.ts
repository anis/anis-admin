import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { OutputFamily, OutputCategory } from '../../../store/model';

@Component({
    selector: 'app-output-family-list',
    templateUrl: 'output-family-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputFamilyListComponent {
    @Input() outputFamilyList: OutputFamily[];
    @Input() outputCategoryList: OutputCategory[];
    @Output() addOutputFamily: EventEmitter<OutputFamily> = new EventEmitter();
    @Output() editOutputFamily: EventEmitter<OutputFamily> = new EventEmitter();
    @Output() deleteOutputFamily: EventEmitter<OutputFamily> = new EventEmitter();
    @Output() addOutputCategory: EventEmitter<OutputCategory> = new EventEmitter();
    @Output() editOutputCategory: EventEmitter<OutputCategory> = new EventEmitter();
    @Output() deleteOutputCategory: EventEmitter<OutputCategory> = new EventEmitter();

    modalRef: BsModalRef;
    outputFamilySelected: OutputFamily;

    constructor(private modalService: BsModalService) { }

    getOutputCategoryListByOutputFamilyId(id: number) {
        return this.outputCategoryList.filter(outputCategory => outputCategory.id_output_family === id);
    }

    openModal(template: TemplateRef<any>, outputFamily: OutputFamily) {
        this.outputFamilySelected = outputFamily;
        this.modalRef = this.modalService.show(template);
    }

    openModalNew(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    confirmAdd(outputFamily: OutputFamily) {
        this.addOutputFamily.emit(outputFamily);
        this.modalRef.hide();
    }

    confirmEdit(outputFamily: OutputFamily) {
        this.editOutputFamily.emit(outputFamily);
        this.modalRef.hide();
    }

    confirmDel() {
        this.deleteOutputFamily.emit(this.outputFamilySelected);
        this.modalRef.hide();
    }
}
