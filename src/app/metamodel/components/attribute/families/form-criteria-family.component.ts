import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { CriteriaFamily } from '../../../store/model';

@Component({
    selector: 'app-form-criteria-family',
    templateUrl: 'form-criteria-family.component.html'
})
export class FormCriteriaFamilyComponent {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: CriteriaFamily = new CriteriaFamily();
    @Output() submitted: EventEmitter<CriteriaFamily> = new EventEmitter();

    emit(criteriaFamily: CriteriaFamily) {
        this.submitted.emit({id: this.model.id, ...criteriaFamily});
    }
}
