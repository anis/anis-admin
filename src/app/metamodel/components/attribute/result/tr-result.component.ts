import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { SettingsSelectOption } from '../../../../settings/store/model';
import { Attribute, RendererConfig } from '../../../store/model';
import { RendererConfigFactory } from '../../../store/model/renderer/renderer-config-factory';

@Component({
    selector: '[result]',
    templateUrl: 'tr-result.component.html',
    styleUrls: [ '../tr.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrResultComponent {
    @Input() set attribute(attribute: Attribute) {
        this._attribute = attribute;
        this.resultForm.controls.name.setValue(attribute.name);
        this.resultForm.controls.renderer.setValue(attribute.renderer);
        if (attribute.renderer) {
            this.setRendererConfigFormGroup(attribute.renderer_config);
        }
        this.resultForm.controls.order_by.setValue(attribute.order_by);
        this.resultForm.controls.order_display.setValue(attribute.order_display);
    }
    @Input() public rendererList: SettingsSelectOption[];
    @Output() public save: EventEmitter<Attribute> = new EventEmitter();

    _attribute: Attribute;
    resultForm = new FormGroup({
        name: new FormControl({ value: '', disabled: true }),
        renderer: new FormControl(),
        renderer_config: new FormGroup({}),
        order_by: new FormControl(),
        order_display: new FormControl()
    });

    getRendererConfigFormGroup() {
        const rendererConfigFormGroup = this.resultForm.controls.renderer_config as FormGroup;
        return rendererConfigFormGroup;
    }

    rendererOnChange(renderer: string) {
        if (renderer === '') {
            this.resultForm.controls.renderer.setValue(null);
            this.resultForm.setControl('renderer_config', new FormGroup({}));
        } else {
            this.setRendererConfigFormGroup(RendererConfigFactory.create(renderer));
        }
    }

    setRendererConfigFormGroup(rendererConfig: RendererConfig) {
        const rendererConfigFormGroup = this.getRendererConfigFormGroup();
        for (const key in rendererConfig) {
            rendererConfigFormGroup.addControl(key, new FormControl(rendererConfig[key]));
        }
    }

    emitSave(): void {
        this.save.emit({
            ...this._attribute,
            ...this.resultForm.value
        });
    }
}
