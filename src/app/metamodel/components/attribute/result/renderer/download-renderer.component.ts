import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-download-renderer',
    templateUrl: 'download-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DownloadRendererComponent {
    @Input() id: number;
    @Input() rendererConfig: FormGroup;
}