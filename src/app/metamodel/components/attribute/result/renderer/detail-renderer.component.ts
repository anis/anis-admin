import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-detail-renderer',
    templateUrl: 'detail-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailRendererComponent {
    @Input() id: number;
    @Input() rendererConfig: FormGroup;
}