import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-link-renderer',
    templateUrl: 'link-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LinkRendererComponent {
    @Input() id: number;
    @Input() rendererConfig: FormGroup;
}