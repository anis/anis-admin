import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-image-renderer',
    templateUrl: 'image-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageRendererComponent {
    @Input() id: number;
    @Input() rendererConfig: FormGroup;
}