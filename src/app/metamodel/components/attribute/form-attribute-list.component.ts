import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { SettingsSelect, SettingsSelectOption } from '../../../settings/store/model';
import { Attribute, Column, CriteriaFamily, Dataset, OutputCategory, OutputFamily } from '../../store/model';

@Component({
    selector: 'app-form-attribute-list',
    templateUrl: 'form-attribute-list.component.html',
    styleUrls: [ 'form-attribute-list.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormAttributeListComponent {
    @Input() dataset: Dataset;
    @Input() attributeList: Attribute[];
    @Input() columnList: Column[];
    @Input() optionListGenerated: string[];
    @Input() criteriaFamilyList: CriteriaFamily[];
    @Input() outputFamilyList: OutputFamily[];
    @Input() outputCategoryList: OutputCategory[];
    @Input() settingsSelectList: SettingsSelect[];
    @Input() tabSelected: string;
    @Input() settingsSelectOptionList: SettingsSelectOption[];
    @Output() addCriteriaFamily: EventEmitter<CriteriaFamily> = new EventEmitter();
    @Output() editCriteriaFamily: EventEmitter<CriteriaFamily> = new EventEmitter();
    @Output() deleteCriteriaFamily: EventEmitter<CriteriaFamily> = new EventEmitter();
    @Output() addOutputFamily: EventEmitter<OutputFamily> = new EventEmitter();
    @Output() editOutputFamily: EventEmitter<OutputFamily> = new EventEmitter();
    @Output() deleteOutputFamily: EventEmitter<OutputFamily> = new EventEmitter();
    @Output() addOutputCategory: EventEmitter<OutputCategory> = new EventEmitter();
    @Output() editOutputCategory: EventEmitter<OutputCategory> = new EventEmitter();
    @Output() deleteOutputCategory: EventEmitter<OutputCategory> = new EventEmitter();
    @Output() addAttribute: EventEmitter<Attribute> = new EventEmitter();
    @Output() editAttribute: EventEmitter<Attribute> = new EventEmitter();
    @Output() deleteAttribute: EventEmitter<Attribute> = new EventEmitter();
    @Output() generateAttributeOptionList: EventEmitter<Attribute> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    getVoEnabled(): boolean {
        return this.dataset.config.download.vo;
    }

    alreadyExists(columnName: string): boolean {
        return this.attributeList.map(a => a.name).includes(columnName);
    }

    addNewAttribute(column: Column) {
        let id = 1;
        if (this.attributeList.length > 0) {
            id = Math.max(...this.attributeList.map(a => a.id)) + 1;
        }
        
        this.addAttribute.emit({
            id,
            name: column.name,
            label: column.name,
            form_label: column.name,
            type: column.type,
            criteria_display: id * 10,
            output_display: id * 10,
            display_detail: id * 10,
            order_display: id * 10,
            selected: true
        })
    }

    getSettingsSelectOptions(settingsSelectName: string): SettingsSelectOption[] {
        const settingsSelect = this.settingsSelectList.find(o => o.name === settingsSelectName);
        if (!settingsSelect) {
            return [];
        }

        return this.settingsSelectOptionList
            .filter(option => option.select_name === settingsSelect.name)
            .sort((a, b) => a.display - b.display);
    }
}
