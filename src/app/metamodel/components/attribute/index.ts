import { FormAttributeListComponent } from './form-attribute-list.component';
import { TableDesignComponent } from './design/table-design.component';
import { TrDesignComponent } from './design/tr-design.component';
import { TableCriteriaComponent } from './criteria/table-criteria.component';
import { TrCriteriaComponent } from './criteria/tr-criteria.component';
import { OptionComponent } from './criteria/option.component';
import { TableOutputComponent } from './output/table-output.component';
import { TrOutputComponent } from './output/tr-output.component';
import { TableResultComponent } from './result/table-result.component';
import { TrResultComponent } from './result/tr-result.component';
import { DetailRendererComponent } from './result/renderer/detail-renderer.component';
import { LinkRendererComponent } from './result/renderer/link-renderer.component';
import { DownloadRendererComponent } from './result/renderer/download-renderer.component';
import { ImageRendererComponent } from './result/renderer/image-renderer.component';
import { TableDetailComponent } from './detail/table-detail.component';
import { TrDetailComponent } from './detail/tr-detail.component';
import { TableVoComponent } from './vo/table-vo.component';
import { TrVoComponent } from './vo/tr-vo.component';
import { CriteriaFamilyListComponent } from './families/criteria-family-list.component';
import { FormCriteriaFamilyComponent } from './families/form-criteria-family.component';
import { OutputFamilyListComponent } from './families/output-family-list.component';
import { FormOutputFamilyComponent } from './families/form-output-family.component';
import { OutputCategoryListComponent } from './families/output-category-list.component';
import { FormOutputCategoryComponent } from './families/form-output-category.component';

export const attributeDummiesComponents = [
    FormAttributeListComponent,
    TableDesignComponent,
    TrDesignComponent,
    TableCriteriaComponent,
    TrCriteriaComponent,
    OptionComponent,
    TableOutputComponent,
    TrOutputComponent,
    TableResultComponent,
    TrResultComponent,
    DetailRendererComponent,
    LinkRendererComponent,
    DownloadRendererComponent,
    ImageRendererComponent,
    TableDetailComponent,
    TrDetailComponent,
    TableVoComponent,
    TrVoComponent,
    CriteriaFamilyListComponent,
    FormCriteriaFamilyComponent,
    OutputFamilyListComponent,
    FormOutputFamilyComponent,
    OutputCategoryListComponent,
    FormOutputCategoryComponent
];
