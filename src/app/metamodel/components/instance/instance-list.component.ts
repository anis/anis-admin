import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Instance } from '../../store/model';

@Component({
    selector: 'app-instance-list',
    templateUrl: 'instance-list.component.html',
    styleUrls: [ 'instance-list.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceListComponent {
    @Input() instanceList: Instance[];
    @Output() deleteInstance: EventEmitter<Instance> = new EventEmitter();

    modalRef: BsModalRef;
    instanceForDel: Instance;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>, instance: Instance) {
        this.instanceForDel = instance;
        this.modalRef = this.modalService.show(template);
    }

    confirmDel() {
        this.deleteInstance.emit(this.instanceForDel);
        this.modalRef.hide();
    }
}
