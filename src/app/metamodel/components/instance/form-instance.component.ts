import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Instance } from '../../store/model';

@Component({
    selector: 'app-form-instance',
    templateUrl: 'form-instance.component.html',
    styleUrls: ['form-instance.component.css']
})
export class FormInstanceComponent {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: Instance = new Instance();
    @Output() submitted: EventEmitter<Instance> = new EventEmitter();


    getConfig(prop: string, key: string): boolean {
        if (this.model.config && this.model.config[prop] && this.model.config[prop][key]) {
            return this.model.config[prop][key];
        }
        return false;
    }

    emit(instance: Instance) {
        let instanceEmitted: Instance;
        (this.model.name) ? instanceEmitted = { name: this.model.name, ...instance } : instanceEmitted = instance;
        instanceEmitted.config = {
            authentication: {
                allowed: this.ngForm.form.value.authentication
            },
            search: {
                allowed: this.ngForm.form.value.search
            },
            search_multiple: {
                allowed: this.ngForm.form.value.search_multiple,
                all_datasets_selected: this.ngForm.form.value.all_datasets_selected
            },
            documentation: {
                allowed: this.ngForm.value.documentation
            }
        };
        this.submitted.emit(instanceEmitted);
    }
}
