import { SurveyListComponent } from './survey/survey-list.component';
import { FormSurveyComponent } from './survey/form-survey.component';
import { DatabaseListComponent } from './database/database-list.component';
import { FormDatabaseComponent } from './database/form-database.component';
import { InstanceListComponent } from './instance/instance-list.component';
import { FormInstanceComponent } from './instance/form-instance.component';
import { DatasetFamilyListComponent } from './dataset-family/dataset-family-list.component';
import { FormDatasetFamilyComponent } from './dataset-family/form-dataset-family.component';
import { DatasetListComponent } from './dataset/dataset-list.component';
import { FormDatasetComponent } from './dataset/form-dataset.component';
import { attributeDummiesComponents } from './attribute';
import { GroupListComponent } from './group/group-list.component';
import { FormGroupComponent } from './group/form-group.component';

export const dummiesComponents = [
    SurveyListComponent,
    FormSurveyComponent,
    DatabaseListComponent,
    FormDatabaseComponent,
    InstanceListComponent,
    FormInstanceComponent,
    DatasetFamilyListComponent,
    FormDatasetFamilyComponent,
    DatasetListComponent,
    FormDatasetComponent,
    attributeDummiesComponents,
    GroupListComponent,
    FormGroupComponent
];
