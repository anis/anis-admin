import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Database } from '../../store/model';

@Component({
    selector: 'app-form-database',
    templateUrl: 'form-database.component.html'
})
export class FormDatabaseComponent {
    @ViewChild(NgForm, {static: true}) ngForm: NgForm;
    @Input() model: Database = new Database();
    @Output() submitted: EventEmitter<Database> = new EventEmitter();

    emit(database: Database) {
        this.submitted.emit({id: this.model.id, ...database});
    }
}
