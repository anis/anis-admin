import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Database, Survey } from '../../store/model';

@Component({
    selector: 'app-database-list',
    templateUrl: 'database-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatabaseListComponent {
    @Input() databaseList: Database[];
    @Input() surveyList: Survey[];
    @Output() deleteDatabase: EventEmitter<Database> = new EventEmitter();

    modalRef: BsModalRef;
    databaseForDel: Database;

    constructor(private modalService: BsModalService) { }

    isNoSurveyAttachedToDatabase(idDatabase: number): boolean {
        return this.getNbSurveyByDatabase(idDatabase) === 0;
    }

    getNbSurveyByDatabase(idDatabase: number): number {
        return this.surveyList.filter(p => p.id_database === idDatabase).length
    }

    openModal(template: TemplateRef<any>, database: Database) {
        this.databaseForDel = database;
        this.modalRef = this.modalService.show(template);
    }

    confirmDel() {
        this.deleteDatabase.emit(this.databaseForDel);
        this.modalRef.hide();
    }
}
