import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SurveyPageComponent } from './containers/survey/survey-page.component';
import { SurveyComponent } from './containers/survey/survey.component';
import { NewSurveyComponent } from './containers/survey/new-survey.component';
import { EditSurveyComponent } from './containers/survey/edit-survey.component';
import { DatabaseComponent } from './containers/database/database.component';
import { NewDatabaseComponent } from './containers/database/new-database.component';
import { EditDatabaseComponent } from './containers/database/edit-database.component';
import { InstanceComponent } from './containers/instance/instance.component';
import { NewInstanceComponent } from './containers/instance/new-instance.component';
import { EditInstanceComponent } from './containers/instance/edit-instance.component';
import { ConfigureInstanceComponent } from './containers/instance/configure-instance.component';
import { NewDatasetComponent } from './containers/dataset/new-dataset.component';
import { EditDatasetComponent } from './containers/dataset/edit-dataset.component';
import { AttributeComponent } from './containers/attribute/attribute.component';
import { GroupComponent } from './containers/group/group.component';
import { NewGroupComponent } from './containers/group/new-group.component';
import { EditGroupComponent } from './containers/group/edit-group.component';
import { AuthGuard } from '../core/auth.guard';
import { environment } from 'src/environments/environment';

const routes: Routes = [
    { path: 'instance-list', component: InstanceComponent },
    { path: 'new-instance', component: NewInstanceComponent },
    { path: 'edit-instance/:iname', component: EditInstanceComponent },
    { path: 'configure-instance/:iname', component: ConfigureInstanceComponent },
    { path: 'configure-instance/:iname/group', component: GroupComponent },
    { path: 'configure-instance/:iname/new-group', component: NewGroupComponent },
    { path: 'configure-instance/:iname/edit-group/:id', component: EditGroupComponent },
    { path: 'configure-instance/:iname/new-dataset', component: NewDatasetComponent },
    { path: 'configure-instance/:iname/edit-dataset/:dname', component: EditDatasetComponent },
    { path: 'configure-instance/:iname/configure-dataset/:dname', component: AttributeComponent },
    {
        path: 'survey', component: SurveyPageComponent, children: [
            { path: '', redirectTo: 'survey-list', pathMatch: 'full' },
            { path: 'survey-list', component: SurveyComponent },
            { path: 'database-list', component: DatabaseComponent }
        ]
    },
    { path: 'new-survey', component: NewSurveyComponent },
    { path: 'edit-survey/:name', component: EditSurveyComponent },
    { path: 'new-database', component: NewDatabaseComponent },
    { path: 'edit-database/:id', component: EditDatabaseComponent }
];

const routesGuarded = routes.map(r=> {
    if (environment.authenticationEnabled) {
        return {
            ...r,
            canActivate: [AuthGuard]
        };
    } else {
        return r;
    }
});

@NgModule({
    imports: [RouterModule.forChild(routesGuarded)],
    exports: [RouterModule]
})
export class MetamodelRoutingModule { }

export const routedComponents = [
    InstanceComponent,
    NewInstanceComponent,
    EditInstanceComponent,
    SurveyPageComponent,
    SurveyComponent,
    NewSurveyComponent,
    EditSurveyComponent,
    DatabaseComponent,
    NewDatabaseComponent,
    EditDatabaseComponent,
    ConfigureInstanceComponent,
    NewDatasetComponent,
    EditDatasetComponent,
    AttributeComponent,
    GroupComponent,
    NewGroupComponent,
    EditGroupComponent
];
