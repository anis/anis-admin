export const environment = {
    production: true,
    apiUrl: '/server',
    servicesUrl: '/services',
    baseHref: '/admin',
    authenticationEnabled: true,
    ssoAuthUrl: 'https://keycloak.lam.fr/auth/',
    ssoRealm: 'anis',
    ssoClientId: 'anis-dev',
    ssoLoginRedirectUri: '/',
    ssoLogoutRedirectUri: '/login'
};
