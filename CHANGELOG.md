# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.6.0]
 - In progress...

## Added
 - #53: Authentication is optional (environment: authenticationEnabled)
 - #54: Administrator can select dataset data_path via a file explorer
 - #55: Add image renderer configuration

### Changed
 - #58: Manage attributes by dataset (add or delete attribute)
 - #57: GUI improvments
 - #52: Update dependencies (Angular v11, ngrx v11, ...)
 - #56: Cone-search configuration improvement

### Fixed
 - #40: Bug cascade deletion

## [3.5.0]
### Added
- #51: Add datasets rights depending on user group
- #49: Add authentication as instance option
- #48: Add Keycloak authentication
- #46: Add dataset public flag option
- #45: Add instance option for search multiple: by default, all datasets selected or none

## [3.4.0]
### Added
- #43: Add dataset result download options
- #42: Add instance options: 'Display search', 'Display search multiple' and 'Display documentation'
- #38: Add new search-type: list
- #41: Add dataset option 'Opened result accordion'
- #33: Add dataset option 'Direct link to the result'

## [3.3.0]
### Added
- #37: Configuration cone search by dataset (form dataset)

## [3.2.0]
### Added
- #15: Automatic generation for the criteria option list values
- #36: Add renderer config form

### Changed
- #27: Add information on the number of dataset families available and datasets available per instance (Instance list)
- #28: Adding tests in continuous integration
- #32: Improvement of the dataset criteria form for the fields type = date
- #34: Upgrade dependencies
- #35: Changing result attribute form (uri_action field deleted and renderer_config field added)

### Deprecated
- For soon-to-be removed features. 

### Removed
- For now removed features.

### Fixed
- #23: Bug in sorting the attribute criteria option list
- #24: Bug in sorting the settings datatables
- #30: Server error when the user adds a new instance (bug in ptoduction only)

### Security
- In case of vulnerabilities. 
